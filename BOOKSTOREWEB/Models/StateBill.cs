﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.Models
{
    public class StateBill
    {
        private int iD;
        private string state;

        public StateBill()
        {

        }

        public StateBill(DataRow row)
        {
            this.ID = (int)row["id"];
            this.State = row["state"].ToString();
        }

        public int ID { get => iD; set => iD = value; }
        public string State { get => state; set => state = value; }
    }
}