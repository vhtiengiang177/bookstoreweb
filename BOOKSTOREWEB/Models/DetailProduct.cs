﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.Models
{
    public class DetailProduct
    {
        string nameProduct;
        Image image;
        int quantity;
        string stateBill;
        float price;

        public int Quantity { get => quantity; set => quantity = value; }
        public string NameProduct { get => nameProduct; set => nameProduct = value; }
        public Image Image { get => image; set => image = value; }
        public string StateBill { get => stateBill; set => stateBill = value; }
        public float Price { get => price; set => price = value; }

        public DetailProduct(DataRow row)
        {
            this.nameProduct = row["name"].ToString();
            this.quantity = (int)row["quantity"];
            this.stateBill = row["state"].ToString();
            this.price = float.Parse(row["prices"].ToString());

        }
    }
}