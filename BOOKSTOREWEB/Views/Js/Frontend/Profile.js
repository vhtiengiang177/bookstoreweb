﻿$(document).ready(function() {
    //getCategory();
    getNameCus(localStorage.getItem("inforCus"));

    $('#info-Account-nav').click(function() {
        $('#list-boughtbill').css('display', 'none');
        $('#list-detailBill').css('display', 'none');
        $('#info-account').css('display', 'block');


        $('#info-Account-nav').addClass("active");
        $('#info-boughtBill-nav').removeClass("active");
        $('#info-detailBill-nav').removeClass("active");

        $('.filter-bill').css('display', 'none');
    });

    $('#info-boughtBill-nav').click(function() {
        $('#list-boughtbill').css('display', 'block');
        $('#list-detailBill').css('display', 'none');
        $('#info-account').css('display', 'none');

        $('#info-Account-nav').removeClass("active");
        $('#info-boughtBill-nav').addClass("active");
        $('#info-detailBill-nav').removeClass("active");

        $('.filter-bill').css('display', 'block');
        $('.filter-bill').first().css('display', 'block');
       getListBoughtBill(localStorage.getItem("inforCus"), "getall");


    });

    if ($('#purchase').children.length > 1) {
      $('.button-log').remove();
    }
    //if ((localStorage.getItem("inforCus") != "null"))
     

    $('#info-Account-nav').click();
});



//Handle filter bill
$('#btnfilter').click(function() {
    var filter = document.querySelectorAll('input[type="radio"]:checked');
    $('#listbill').empty();
    getListBoughtBill(localStorage.getItem("inforCus"), filter[0].value);


});

//Handle find bill

$('#btn-search').click(function() {
    var e = document.getElementById("condition");
    var condition = e.options[e.selectedIndex].value;
    var filter = document.querySelectorAll('input[type="radio"]:checked');
    var keys = $('#keyword').val();
    $('#listbill').empty();
    $('#listbill').append('<h5 style="text-align:center; color: grey; font-size: 18;">Đang tìm kiếm ...</h5>');
    searchBill(localStorage.getItem("inforCus"), filter[0].value, condition, keys);

    return false;
});


function getNameCus(idCus) {
    var URL = "/api/customer/getCustomerInfo/" + idCus;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        dataType: "",
    }).done(function(response) {

        alert(response.Name);
        $('#inputname').val(response.name);
        $('#inputEmail').val(response.Email);
        $('#inputPhone').val(response.Phone);
        $('#inputAddress').val(response.Address);
        var now = new Date(response.BirthDay);
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear() + "-" + (month) + "-" + (day);

        $('#inputBirthdate').val(today);
        if (response.Gender == "Male") $("#radioMale").prop("checked", true);
        if (response.Gender == "Female") $("#radioFemale").prop("checked", true);

    }).fail(function (response) {

        alert("sdj");
    });
}


$('#go-to-cart').click(function() {
    if (localStorage.getItem("inforCus") != "null") {
        window.location.href = "Checkout.html";
    } else
        window.location.href = "../../Login/signIn.html";
})

//Load category in navbar

function loadDetailProduct(listProduct) {
    $.each(listProduct, function (index1, item1) {
        if (index1 % 3 == 0) {


            var strHTML1 = $(`<tr class="row ">
                                    <td class="col-2">
                                        <img class="img-product img-fluid" src="../../../Images/Frontend/Home/product-4.jpg" alt="">
                                    </td>
                                    <td class="col-md-6">
                                        <a class="title" for="title">` + item1.NameProduct + `</a>
                                        <label class="quantity">x` + item1.Quantity + `</label>
                                    </td>
                                    <td class="col-md-2">
                                        <label class="price-product">` + item1.Price + `</label>
                                    </td>
                                    <td class="col-md-2">
                                        <div class="state">
                                            <label class="statebill">` + item1.StateBill + `</label>
                                        </div>

                                    </td>
                                </tr>
                                <tr class="row">
                                    <td class="col-11">
                                        <div class="devider"></div>
                                    </td>
                                </tr>`);
            $('#detailbill-product').append(strHTML1);
            
        }
    });
}

function loadDetailBill(bill) {

    $('#list-detailBill').empty();

    var infoBill = $(`<div class="row">
                        <div class="col-md-5 idbill">
                            <label>Mã đơn hàng: <span>` + bill.ID + `</span></label>

                        </div>
                        <div class="col-md-6">
                            <label>Tình trạng đơn:</label>
                            <label>Đã giao</label>
                        </div>
                    </div>
                    <div class="address-receive">
                        <h5>Địa chỉ nhận hàng</h5>
                        <h6>Nguyễn Hoàng</h6>
                        <h6>` + bill.Phone + `</h6>
                        <h6>` + bill.AddressReceive + `</h6>
                        <label>Thời gian xác nhận :</label>
                        <h6>` + bill.DateComfirm + `</h6>
                        <label>Thời gian giao :</label>
                        <h6>` + bill.DateReceive + `</h6>
                    </div>`);
    $('#list-detailBill').append(infoBill);

    var strHTML = $(`<div class="infor-bill">
                        <div class="product-infor container-fluid">
                            <table>
                                <tbody id="detailbill-product">
                                </tbody>
                            </table>
                            <div class="btn-action row">
                                <div class="col-md-5"></div>
                                <div class="col-md-7">
                                    <h6 class="total-cost">Tổng tiền hàng</h6>
                                    <label style="color: rgb(158, 20, 20);font-size: 18px; font-weight: 500;">` + bill.TotalCost + `</label>
                                    <h6 class="feeship">Phí vận chuyển</h6>
                                    <label>` + bill.FeeShip + `</label>
                                    <h6>Voucher áp dụng</h6>
                                    <label>` + bill.Vouchername + `</label>
                                    <label>-` + bill.Voucher * 100 + `%</label>
                                    <div class="devider"></div>
                                    <h6>Phương thức thanh toán:</h6>
                                    <label>` + bill.Payment + `</label>
                                    <h6>Phương thức vận chuyển:</h6>
                                    <label>` + bill.Delivery + `</label>
                                </div>

                            </div>`);
    $('#list-detailBill').append(strHTML);
    //load list product in a bill

    loadDetailProduct(bill.ListProduct);

}




function getDetailBill(idBill) {
    var URL = "/api/bill/getDetailbill/" + idBill;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        dataType: "",
    }).done(function(response) {

        loadDetailBill(response);

    }).fail(function() {
     //   alert("ko tồn tại bill");
        $('#list-detailBill').append('<h5 style="text-align:center; color: red; font-size: 24;">Không load được sản phẩm </h5>')
    });
}

function showlistBill(bills) {
    $('#listbill').empty();
    if (bills.length == 0)
        $('#listbill').append('<h5 style="text-align:center; color: red; font-size: 24;">Không tìm thấy đơn </h5>');
    $.each(bills, function(index, item) {

        var strHTML = $(`<div class="product-infor">
                            <table>
                               <tbody id="product-bill` + index + `">
                                <tr class="row header container">
                                    <td class="col-4">
                                        <span>Ma don hang :  ID<label class="idBill">` + item.ID + `</label></span>
                                    </td>
                                    <td class="col-4"></td>
                                    <td class="col-4">
                                        <span>Thời gian nhận :<label>` + item.DateReceive + `</label></span>
                                    </td>
                                </tr>
                                                            
                                </tbody>
                            </table>
                            <div class="btn-action">
                                <h5 class="total-cost"><span>Tổng số tiền : </span>` + item.TotalCost + `</h5>
                                <button type="button"  class="btn btn-primary detailProduct">Xem chi tiết đơn hàng</button>
                            </div>                           
                        </div>`);
        $('#listbill').append(strHTML);

        //event click view detail product
        $.each(item.ListProduct, function (index1, item1) {
            if (index1 % 3 == 0) {



                var strHTML1 = $(`<tr class="row ">
                                    <td class="col-2">
                                        <img class="img-product img-fluid" src="../../../Images/Frontend/Home/product-4.jpg" alt="">
                                    </td>
                                    <td class="col-md-6">
                                        <a class="title" for="title">` + item1.NameProduct + `</a>
                                        <label class="quantity">x` + item1.Quantity + `</label>
                                    </td>
                                    <td class="col-md-2">
                                        <label class="price-product">` + item1.Price + `</label>
                                    </td>
                                    <td class="col-md-2">
                                        <div class="state">
                                            <label class="statebill">` + item1.StateBill + `</label>
                                        </div>

                                    </td>
                                </tr>
                                <tr class="row">
                                    <td class="col-11">
                                        <div class="devider"></div>
                                    </td>
                                </tr>`);
                $('#product-bill' + index).append(strHTML1);
            }
        });

        var btndetail = document.getElementsByClassName("detailProduct");
        for (var i = 0; i < btndetail.length; i++) {
            var btn = btndetail[i];
            btn.onclick = function(event) {
                $('.filter-bill').first().css('display', 'none');
                var t = event.target;
                var idbill = t.parentElement.parentElement.getElementsByClassName("idBill")[0].innerText;
                $('#list-boughtbill').css('display', 'none');
                $('#list-detailBill').css('display', 'block');
                $('#info-account').css('display', 'none');
                getDetailBill(idbill);
            }
        }

    });
}

//get list bill of a customer
function getListBoughtBill(idCus, state) {

    $.ajax({
        url: "/api/bill/listBoughtBill/" + idCus + "/" + state,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        dataType: "",
    }).done(function(response) {
        showlistBill(response);

    }).fail(function(response) {
        $('#listbill').empty();
        $('#listbill').append('<h4 style="text-align:center; color: red; font-size: 20;">Chưa có đơn hàng nào</h4>');
    });

}


//Search bill

function searchBill(idCus, state, condition, key) {
    var keywords = String(key.replace(/\s/g, ''));
    var URL = "/api/bill/searchbill/" + idCus + "/" + state + "/" + condition + "/" + key;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },

        dataType: ""
    }).done(function(response) {
        showlistBill(response);

    }).fail(function() {
        $('#listbill').empty();
        $('#listbill').append('<h5 style="text-align:center; color: red; font-size: 20;">Không có đơn hàng phù hợp !</h5>');

    });
}


//Edit persional info

$('#btn-save').click(function editProfile() {
    var customer = {
        Name: $('#inputname').val(),
        BirthDay: $('#inputBirthdate').val(),
        Gender: $("input[type='radio']:checked").val(),
        Address: $('#inputAddress').val(),
        Phone: $('#inputPhone').val(),
        Email: $('#inputEmail').val(),
    }

   // alert(customer.BirthDay);
    var idCus = localStorage.getItem("inforCus");
    $.ajax({
        url: "/api/customer/edit/" + idCus,
        method: "PUT",
        contentType: 'application/json',
        dataType: "json",
        data: JSON.stringify(customer),

    }).done(function (response) {
       // alert("save");
        $('#info-Account-nav').click();
        $('#success-edit').css('display', 'block');
    }).fail(function() {
        $('#success-edit').html("Chỉnh sửa không thành công. Vui lòng thử lại !");
        $('#success-edit').css('color', 'red');
    });
});