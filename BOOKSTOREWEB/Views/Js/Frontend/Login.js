﻿
$(document).ready(function () {
    var loginJS = new LoginJS();
});

class LoginJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        $("#btnSignIn").click(this.getInforUser.bind(this));
        $("#btnSignUp").click(this.goSignUp.bind(this));
    }

    goSignUp() {
        window.location.href = "/Views/Pages/Frontend/Register/SignupPage.html";
    }

    checkLogin() {
        var username = $("#txtUsername").val();
        var password = $("#txtPassword").val();
        this.resultLogin = true;
        if (username != null) {
            if (username == "") {
                $("#error-input-username").html("Không thể để trống tên đăng nhập!");
                this.resultLogin = false;
            }
            else
                $("#error-input-username").html("");
        }
        if (password != null) {
            if (password == "") {
                $("#error-input-password").html("Không thể để trống mật khẩu!");
                this.resultLogin = false;
            }
            else
                $("#error-input-password").html("");
        }
    }

    getInforUser() {
        var self = this;
        //console.log("1");
        this.checkLogin();
        if (!this.resultLogin)
            return;
        var userName = document.getElementById("txtUsername").value;
        //var passWord = $("#txtPassword").val();
        var passWord = document.getElementById("txtPassword").value;
    
        //Kết nối tới dữ liệu backend.
        $.ajax({
            url: "/api/account/login/" + userName + "/" + passWord,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            //data: JSON.stringify(account),//Dữ liệu truyền xuống là account nên chuyển thành file JSON. Truyền xuống thông qua body request.
            dataType: "", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response.length == 0)
                $('#fail-login').html = "Tên đăng nhập hoặc mật khẩu không đúng !";
            else {
                var idPermission = response[0].idPermission;
                if (idPermission < 4) window.location.href = "../../Login/signIn.html";
                else if (idPermission == 4) {
                    $.ajax({
                        url: "/api/account/getIdCus/" + userName,
                        method: "GET",
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json' //Định nghĩa type data trả về.
                        },
                        dataType: "", //Kiểu dữ liệu truyền lên.
                    }).done(function (response) {

                        if (parseInt(response) > 0) {
                            if (localStorage.getItem("inforCus") != null)
                                localStorage.removeItem("inforCus");
                            localStorage.setItem("inforCus", parseInt(response));
                            //alert(localStorage.getItem("inforCus"));
                            window.location.href = "../Home/HomePage.html";

                        }
                        //else alert(parseInt(response));

                    }).fail(function (response) {
                       // alert("Đăng nhập thất bại");
                    });
                }


            }
        }).fail(function (response) {
           // alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
        });
    }   



     /*   $.ajax({
            url: "/api/Account/getIdAccount/" + userName + "/" + passWord,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {

            if (parseInt(response) > 0) {
                if (localStorage.getItem("inforCus") != null)
                    localStorage.removeItem("inforCus");
                localStorage.setItem("inforCus", parseInt(response));
                alert(localStorage.getItem("inforCus"));
                window.location.href = "../Home/HomePage.html";

            }
            else {
                $('#fail-login').html = "Tên đăng nhập hoặc mật khẩu không đúng !";
            }
           

        }).fail(function (response) {
            alert("Đăng nhập thất bại");
        });

    }*/

    getPermission() {
        var self = this;
        this.checkLogin();
        if (!this.resultLogin)
            return;
        //Lấy dữ liệu account từ form login.
        var userName = $("#txtUsername").val();
        var passWord = $("#txtPassword").val();
        //Kết nối tới dữ liệu backend.
        $.ajax({
            url: "/api/account/login/" + userName + "/" + passWord,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            //data: JSON.stringify(account),//Dữ liệu truyền xuống là account nên chuyển thành file JSON. Truyền xuống thông qua body request.
            dataType: "", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response.length == 0)
                alert("Tài khoản hoặc mật khẩu không đúng!");
            else {
                var idPermission = response[0].idPermission;
                switch (idPermission) {
                    case 1: self.setStaffSession(response[0].id);
                        window.location = "/Views/Pages/Dashboard/Home/home.html";
                        break;
                    case 2: self.setStaffSession(response[0].id);
                        window.location = "/Views/Pages/Dashboard/Home/home.html";
                        break;
                    case 3: self.setStaffSession(response[0].id);
                        window.location = "/Views/Pages/Dashboard/Home/transport.html";
                        break;
                    case 4:
                        $.ajax({
                            url: "/api/account/getIdCus/" + userName,
                            method: "GET",
                            headers: {
                                'Accept': 'application/json',
                                'Content-Type': 'application/json' //Định nghĩa type data trả về.
                            },
                            dataType: "", //Kiểu dữ liệu truyền lên.
                        }).done(function (response) {

                            if (parseInt(response) > 0) {
                                if (localStorage.getItem("inforCus") != null)
                                    localStorage.removeItem("inforCus");
                                localStorage.setItem("inforCus", parseInt(response));
                                alert(localStorage.getItem("inforCus"));
                                window.location.href = "../Frontend/Home/HomePage.html";

                            }
                            else alert(parseInt(response));

                        }).fail(function (response) {
                            alert("Đăng nhập thất bại");
                        });
                        break;
                    default: alert("Bạn là đối tượng chưa được xác định");
                }
            }
        }).fail(function (response) {
           // alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
        });
    }

    setStaffSession(id) {
        $.ajax({
            url: "/api/staff/set-staff-session/" + id,
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: "json",
        }).done(function (response) {
            if (!response)
                alert("Kết nối thất bại!");
        }).fail(function (response) {
            //alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
        });
    }

    getIdAcount(username) {
        $.ajax({
            url: "/api/account/getUser/" + userName,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "int", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response > 0)
                localStorage.setItem("inforCus", response);
            else alert("Ko phải khách hàng");

        }).fail(function (response) {
            alert("Đăng nhập thất bại");
        });
    }

    addAccount() {
        var account = {
            UserName: $("#txtUsername").val(),
            PassWord: $("#txtPassword").val(),
        };

        $.ajax({
            url: "/api/account",
            method: "POST",
            data: JSON.stringify(account),//Dữ liệu truyền xuống là account nên chuyển thành file JSON. Truyền xuống thông qua body request.
            contentType: "application/json", //Kiểu dữ liệu trả về.
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response)
                alert("Thêm thành công");
            else
                alert(" Thất bại");
        }).fail(function (response) {
            //alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
        });
    }

}