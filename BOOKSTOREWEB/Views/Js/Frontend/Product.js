﻿var redirUrl = window.location.href;

var myParam = redirUrl.split('type=')[1] ? redirUrl.split('type=')[1] : '0';





$(document).ready(function () {
    getProductWithtype(parseInt(myParam));
    getBestsellerWithType(parseInt(myParam));

    $('#logout').click(function () {
        if (localStorage.getItem("inforCus") != "null")
            localStorage.setItem("inforCus", "null");
        window.location.href = "../../Login/SignIn.html"
    });


    $('#gotocart').click(function () {
        if (localStorage.getItem("inforCus") != "null") {
            window.location.href = "Checkout.html";
        } else {
            window.location.href = "../../Login/signIn.html";
        }
    });

    $('#go-to-cart').click(function () {
        if (localStorage.getItem("inforCus") != "null") {
            window.location.href = "Checkout.html";
        } else {
            window.location.href = "../../Login/signIn.html";
        }
    });
})


var state = {
    'querySet': '',
    'page': 1,
    'rows': 15,
    'window': 4,
}
function pagination(querySet, page, rows) {

    var trimStart = (page - 1) * rows
    var trimEnd = trimStart + rows

    var trimmedData = querySet.slice(trimStart, trimEnd)

    var pages = Math.round(querySet.length / rows);

    return {
        'querySet': trimmedData,
        'pages': pages,
    }
}

function pageButtons(pages) {
    var wrapper = document.getElementById('pagination-wrapper')

    wrapper.innerHTML = ``
    console.log('Pages:', pages)

    var maxLeft = (state.page - Math.floor(state.window / 2))
    var maxRight = (state.page + Math.floor(state.window / 2))

    if (maxLeft < 1) {
        maxLeft = 1
        maxRight = state.window
    }

    if (maxRight > pages) {
        maxLeft = pages - (state.window - 1)

        if (maxLeft < 1) {
            maxLeft = 1
        }
        maxRight = pages
    }



    for (var page = maxLeft; page <= maxRight; page++) {
        wrapper.innerHTML += `<button value=${page} class="page btn btn-sm">${page}</button>`
    }

    if (state.page != 1) {
        wrapper.innerHTML = `<button value=${1} class="page btn btn-sm">&#171; First</button>` + wrapper.innerHTML
    }

    if (state.page != pages) {
        wrapper.innerHTML += `<button value=${pages} class="page btn btn-sm">Last &#187;</button>`
    }

    $('.page').on('click', function (event) {

        $('.page').removeClass('active');
        state.page = Number($(this).val())
        $(this).addClass('active');

        buildTable()
    })

}


function buildTable() {

    var data = pagination(state.querySet, state.page, state.rows)
    var myList = data.querySet;
    $('#main_container_pro').empty();
    $.each(myList, function(index, item) {
        var strHTML = $(`<div class="col-md-2 mt-3 product-men women_two shop-gd">
                                    <div class="product-googles-info googles">
                                        <div class="men-pro-item">
                                            <div class="men-thumb-item">
                                                <img src="data:image/png;base64,`+ item.photo + `" alt="Product Image" class="img-fluid">
                                                <div class="men-cart-pro">
                                                    <div class="inner-men-cart-pro">
                                                        <a class="id-PRO" style="display:block;">` + item.id + `</>
                                                        <a style="color:white;"  class="link-product-add-cart">View</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item-info-product">
                                                <div class="info-product-price">
                                                    <div class="grid_meta">
                                                        <div class="product_price" >
                                                            <h4>
                                                                <a href="#">` + item.name + `</a>
                                                            </h4>
                                                            <div class="grid-price mt-2">
                                                                <span class="money ">` + item.price + `</span>
                                                            </div>
                                                        </div>
                                                        <ul class="stars">
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-star" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="googles single-item hvr-outline-out">
                                                        <form action="#" method="post">
                                                            <label class="id-PRO" type="hidden">` + item.id + `</label>
                                                            <input type="hidden" name="cmd" value="_cart">
                                                            <input type="hidden" name="add" value="1">
                                                            <input type="hidden" name="googles_item" value="Farenheit">
                                                            <input type="hidden" name="amount" value="575.00">
                                                            <button type="submit" class="googles-cart pgoogles-cart">
                                                                <i class="fas fa-cart-plus"></i>
                                                            </button>
                                                        </form>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>`);
        $('#main_container_pro').append(strHTML);
        //load star rate
        for (var i = 0; i < item.Rating - 1; i++) {
            var ratting = document.getElementsByClassName("stars")[index];
            ratting.getElementsByClassName("fa-star")[i].style.color = "yellow";
        }
    });

    var buttonaddtocart = document.getElementsByClassName("googles-cart");
    for (var i = 0; i < buttonaddtocart.length; i++) {
        var add = buttonaddtocart[i];
        add.onclick = function (event) {
            var button = event.target;
            var product = button.parentElement.parentElement;
            var idpro = product.getElementsByClassName("id-PRO")[0].innerHTML;
            var idcus = localStorage.getItem("inforCus");
            var idshop = 1;
            //THÊM VÀO GIỎ HÀNG
            window.productJS.HandleCart(idcus, idpro, idshop);
            return false;
        };
    }

    var viewDetail = document.getElementsByClassName("inner-men-cart-pro");
    for (var i = 0; i < viewDetail.length; i++) {
        var button = viewDetail[i];
        button.onclick = function (event) {
            var t = event.target;
            // Store
            window.location.href = "DetailProductPage.html?ID=" + t.parentElement.getElementsByClassName("id-PRO")[0].innerText;

        };
    }

    pageButtons(data.pages);
}


function loadDataProduct(products) {
    $('#main_container_pro').empty();
    state.querySet = products;
    buildTable();

    var buttonaddtocart = document.getElementsByClassName("googles-cart");
    for (var i = 0; i < buttonaddtocart.length; i++) {
        var add = buttonaddtocart[i];
        add.onclick = function(event) {
            if (localStorage.getItem("inforCus") != null) {
                var button = event.target;
                var product = button.parentElement.parentElement;
                var idpro = product.getElementsByClassName("id-PRO")[0].innerHTML;
                var idcus = localStorage.getItem("inforCus");
                var idshop = 1;
                //THÊM VÀO GIỎ HÀNG
                HandleCart(idcus, idpro, idshop);
                return false;
            } else
                window.location.href = "../../Login/signIn.html";
        };
    }

    var viewDetail = document.getElementsByClassName("inner-men-cart-pro");
    for (var i = 0; i < viewDetail.length; i++) {
        var button = viewDetail[i];
        button.onclick = function(event) {
            var t = event.target;

            window.location.href = "DetailProductPage.html?ID=" + t.parentElement.getElementsByClassName("id-PRO")[0].innerText;
        };
    }

}



function loadBestSellerWithType(products) {
   // alert(products.length);
    $('#best-seller-cato').empty();

    $.each(products, function (index, item) {
        if (index % 3 == 0) {

            var strHTM = $(` <div class="item" >
                                <div class="gd-box-info text-center">
                                    <div class="product-men women_two bot-gd">
                                        <div class="product-googles-info slide-img googles">
                                            <div class="men-pro-item">
                                                <div class="men-thumb-item">
                                                    <img src="data:image/png;base64,`+ item.photo + `" class="img-fluid" alt="">
                                                    <div class="men-cart-pro">
                                                        <div class="inner-men-cart-pro">
                                                            <a href="DetailProductPage.html?ID=`+ item.id + `" class="link-product-add-cart">Quick View</a>
                                                        </div>
                                                    </div>
                                                    <span class="product-new-top">New</span>
                                                </div>
                                                <div class="item-info-product">

                                                    <div class="info-product-price">
                                                        <div class="grid_meta">
                                                            <div class="product_price">
                                                                <h4>
                                                                    <a href="DetailProductPage.html?ID=`+ item.id + `">` + item.name + ` </a>
                                                                </h4>
                                                                <div class="grid-price mt-2">
                                                                    <span class="money ">` + item.price + `đ</span>
                                                                </div>
                                                            </div>
                                                            <ul class="stars">
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="googles single-item hvr-outline-out">
                                                                 <label class="id-PRO" style="display:none;">` + item.id + `</label>
                                                                <button type="button" class="googles-cart pgoogles-cart">
                                                                    <i class="fas fa-cart-plus"></i>
                                                                </button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                `);
            $('#best-seller-cato').append(strHTM);
        }

        
    });

    var buttonaddtocart = document.getElementsByClassName("googles-cart");
    for (var i = 0; i < buttonaddtocart.length; i++) {
        var add = buttonaddtocart[i];
        add.onclick = function (event) {
            var button = event.target;
            var product = button.parentElement.parentElement;
            var idpro = product.getElementsByClassName("id-PRO")[0].innerHTML;
            var idcus = localStorage.getItem("inforCus");
            var idshop = 1;
            //THÊM VÀO GIỎ HÀNG
            window.productJS.HandleCart(idcus, idpro, idshop);
            return false;
        };
    }
}


function getBestsellerWithType(idtype) {
    $.ajax({
        url: "/api/product/listbestsaler/" + idtype,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "",
    }).done(function(products) {
        loadBestSellerWithType(products);
    }).fail(function() {
        //alert("Không load được best seller");
    });
}

function HandleCart(idcus, idproduct, idshop) {

    var cart = {
        IDCustomer: idcus,
        IDproduct: idproduct,
        IDshop: idshop,
        // Quantity: quantity
    };
    $.ajax({
        url: "/api/product/addtocart",
        method: "POST",
        data: JSON.stringify(cart),
        contentType: "application/json",
        dataType: "json",
        traditional: true
    }).done(function(res) {
        if (res == true) {
          //  alert("Sản phẩm đã thêm vào giỏ!");
            // $(location).attr('href', '/Views/Pages/Login/signIn.html');
        } else {
          //  alert("Thêm vào giỏ hàng không thành công!")
        }
    }).fail(function(response) {
      //  alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
    });

}

function getNameType(typeID, products) {
    var URL = "/api/product/getNameType/" + typeID;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "",
    }).done(function(nametype) {
        $('#label-title').html(nametype);
        loadDataProduct(products);
    }).fail(function() {
        //alert("Không load được data");
    });
}

function getProductWithtype(typeID) {
    var URL = "/api/product/getproduct/" + typeID;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json",
    }).done(function(products) {

        getNameType(typeID, products);

    }).fail(function() {

    });
}


function getTypeProductWithCate(idCate, count) {
    var URL = "/api/Category/type/" + idCate;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        dataType: "",
    }).done(function(products) {
        var ul = document.createElement("ul");
        $.each(products, function(index, item) {


            var li = document.createElement("li");
            li.setAttribute("class", "media-mini mt-3");

            var a = document.createElement("a");

            var text = document.createTextNode(item.Name);
            a.setAttribute("href", "ProductPage.html?type=" + item.ID);

            a.appendChild(text);
            li.appendChild(a);
            ul.appendChild(li);


        });
        var container = document.getElementsByClassName("col-md-4 media-list span4 text-left")[count];
        container.appendChild(ul);



    }).fail(function() {

    });
}


function loadCategory(listcategory) {
    self = this;
    $('#nav-category').empty();
    var container = document.getElementById("nav-category");
    $.each(listcategory, function(index, item) {


        var divcontaint = document.createElement("div");
        divcontaint.setAttribute("class", "col-md-4 media-list span4 text-left");


        var h5titleCate = document.createElement("h5");
        h5titleCate.setAttribute("class", "tittle-w3layouts-sub");
        var content = document.createTextNode(item.Name);

        h5titleCate.appendChild(content);

        divcontaint.appendChild(h5titleCate);

        container.appendChild(divcontaint);

        self.getTypeProductWithCate(item.ID, index);
    });

}

//Load category trên thanh narbar
function getCategory() {
    self = this;
    var URL = "";
    $.ajax({
        url: "/api/Category/type",
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json", //Kiểu dữ liệu truyền lên.
    }).done(function(response) {
        self.loadCategory(response);

    }).fail(function(response) {
        //  alert("Lỗi category");
    });
}