﻿$(document).ready(function () {
    if ($('#purchase').children.length > 1) {
        $('.button-log').remove();
    }


    $('#_desktop_user_info').empty();
    if (localStorage.getItem("inforCus") != "null")
        getNameCus(localStorage.getItem("inforCus"));
    else {

        var strHtml = $(` <ul class="notlogin">
                                <li class="item-notlogin"><a href="../Register/LoginPage.html">Đăng nhập</a></li>
                                <li class="item-notlogin"><a href="../Register/SignupPage.html">Đăng kí tài khoản</a></li>
                            </ul>`);
        $('#_desktop_user_info').prepend(strHtml);
    }

    //số lượng một sản phẩm bất kì trong cart 


})



function getNameCus(idCus) {
    var URL = "/api/customer/getCustomerInfo/" + idCus;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {
        $('#_desktop_user_info').empty();
        if (response == null) {
            var strHtml = $(` <ul class="notlogin">
                                <li class="item-notlogin"><a href="../Register/LoginPage.html">Đăng nhập</a></li>
                                <li class="item-notlogin"><a href="../Register/SignupPage.html">Đăng kí tài khoản</a></li>
                            </ul>`);
            $('#_desktop_user_info').prepend(strHtml);
        } else {

            var strHTML = $(` <ul class="login">
                            <li class="item-login"><a href="ProfilePage.html">`+ response.Name + ` <i class="fas fa-user" aria-hidden="true"></i></a></li>
                            <li class="item-login" id="logout"><a >Đăng xuất <i class="fas fa-sign-out-alt" ></i> </a></li>
                        </ul>`);
            $('#_desktop_user_info').prepend(strHTML);
        }
    }).fail(function (response) { });
}

$('#go-to-cart').click(function () {
    if (localStorage.getItem("inforCus") != "null") {
        window.location.href = "Checkout.html";
    } else
        window.location.href = "../Register/LoginPage.html";
});

$('#logout').click(function () {
    if (localStorage.getItem("inforCus") != "null")
        localStorage.setItem("inforCus", "null");
    window.location.href = "../Register/LoginPage.html"
});