﻿

$(document).ready(function () {
   
    if (localStorage.getItem("inforCus") != "null")
        getNameCus(localStorage.getItem("inforCus"));
    if ($('#purchase').children.length > 1) {
        $('.button-log').remove();
    }
    getCategory();


    var detailJS = new DetailProductJS(myParam);
    window.detailJS = detailJS;
    window.resultUpdateOrderState = true;
    window.completionRate = 1;

})

function getNameType(typeID, products) {
    var URL = "/api/product/getNameType/" + typeID;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "",
    }).done(function (nametype) {
        $('#label-title').html(nametype);
        loadDataProduct(products);
    }).fail(function () {
       // alert("Không load được data");
    });
}



function getProductWithtype(typeID) {
    var URL = "/api/product/getproduct/" + typeID;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json",
    }).done(function (products) {
        getNameType(typeID, products);

    }).fail(function () {

    });
}


function getTypeProductWithCate(idCate, count) {
    var URL = "/api/Category/type/" + idCate;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "", //Kiểu dữ liệu truyền lên.
    }).done(function (products) {
        var ul = document.createElement("ul");
        $.each(products, function (index, item) {


            var li = document.createElement("li");
            li.setAttribute("class", "media-mini mt-3");

            var a = document.createElement("a");

            var text = document.createTextNode(item.Name);
            a.setAttribute("href", "ProductPage.html?type=" + item.ID);

            a.appendChild(text);
            li.appendChild(a);
            ul.appendChild(li);
        });
        var container = document.getElementsByClassName("col-md-4 media-list span4 text-left")[count];
        container.appendChild(ul);
    }).fail(function () {

    });
}

function loadCategory(listcategory) {
    $('#nav-category').empty();
    var container = document.getElementById("nav-category");
    $.each(listcategory, function (index, item) {


        var divcontaint = document.createElement("div");
        divcontaint.setAttribute("class", "col-md-4 media-list span4 text-left");


        var h5titleCate = document.createElement("h5");
        h5titleCate.setAttribute("class", "tittle-w3layouts-sub");
        var content = document.createTextNode(item.Name);

        h5titleCate.appendChild(content);

        divcontaint.appendChild(h5titleCate);

        container.appendChild(divcontaint);

        getTypeProductWithCate(item.ID, index);
    });
}
//Load category trên thanh narbar
function getCategory() {
    var URL = "";
    $.ajax({
        url: "/api/Category/type",
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {
        loadCategory(response);

    }).fail(function (response) {
        //alert("Lỗi category");
    });
}



var redirUrl = window.location.href;

var myParam = redirUrl.split('ID=')[1] ? redirUrl.split('ID=')[1] : '0';
//Lấy tài khoản khách hàng

function getNameCus(idCus) {
    var URL = "/api/customer/getCustomerInfo/" + idCus;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {

        if (localStorage.getItem("inforCus") == "null") {
            var strHtml = $(` <li class="button-log">
                            <a class="btn-open" >
                                <span class="fas fa-user" aria-hidden="true"></span> SigIn
                            </a>
                        </li>`);
            $('#purchase').prepend(strHtml);
        }
        else {
            //alert("cart");
            var strHTML = $(`<li class="button-log" style=" color:black;font-size:20px; font-family:'Catfish blues';text-decoration:none;cursor:pointer;">
                            <a  href="ProfilePage.html" class="btn-open" id="name-account"><span class="fas fa-user" aria-hidden="true"></span>`+ response.Name + ` </a>
                        </li>`);
            $('#purchase').prepend(strHTML);
        }
    }).fail(function (response) { });
}

function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return '';
}
function writeCookie(name, value, days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
//Hàm load loại sản phẩm
function getNameType(typeID, products) {
    var URL = "/api/product/getNameType/" + typeID;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "",
    }).done(function (nametype) {
        $('#label-title').html(nametype);
        loadDataProduct(products);
    }).fail(function () {
      //  alert("Không load được data");
    });
}

function getProductWithtype(typeID) {
    var URL = "/api/product/getproduct/" + typeID;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json",
    }).done(function (products) {
        getNameType(typeID, products);

    }).fail(function () {

    });
}


function getTypeProductWithCate(idCate, count) {
    var URL = "/api/Category/type/" + idCate;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "", //Kiểu dữ liệu truyền lên.
    }).done(function (products) {
        var ul = document.createElement("ul");
        $.each(products, function (index, item) {


            var li = document.createElement("li");
            li.setAttribute("class", "media-mini mt-3");

            var a = document.createElement("a");

            var text = document.createTextNode(item.Name);
            a.setAttribute("href", "ProductPage.html?type=" + item.ID);

            a.appendChild(text);
            li.appendChild(a);
            ul.appendChild(li);


        });
        var container = document.getElementsByClassName("col-md-4 media-list span4 text-left")[count];
        container.appendChild(ul);



    }).fail(function () {

    });
}


//Load category trên thanh narbar
function getCategory() {
    var URL = "";
    $.ajax({
        url: "/api/Category/type",
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {
        loadCategory(response);

    }).fail(function (response) {
       // alert("Lỗi category");
    });
}




function loadCategory(listcategory) {
    $('#nav-category').empty();
    var container = document.getElementById("nav-category");
    $.each(listcategory, function (index, item) {


        var divcontaint = document.createElement("div");
        divcontaint.setAttribute("class", "col-md-4 media-list span4 text-left");


        var h5titleCate = document.createElement("h5");
        h5titleCate.setAttribute("class", "tittle-w3layouts-sub");
        var content = document.createTextNode(item.Name);

        h5titleCate.appendChild(content);

        divcontaint.appendChild(h5titleCate);

        container.appendChild(divcontaint);

        getTypeProductWithCate(item.ID, index);
    });
}
//===================================================================//
class DetailProductJS {

    constructor(myParam) {
        this.idProduct = myParam;
        this.idCustomer = localStorage.getItem("inforCus");
        this.idShop = 1;

        // alert(this.idProduct);
        this.initEvents();
    }

    initEvents() {
        self = this;
        window.orderState = 'all';

        this.getTransport(self.idProduct);
        this.getDetailComment(self.idProduct);
        this.loadCountCart(self.idCustomer);
        this.getSuggestBook(self.idProduct);
        //save how many start customer give
        var ratedIndex = -1;
        //span-Number
        //button Minus click
        $('.btn-minus').click(function () {
            let value = parseInt($('#quantityWantBy').val());
            if (value == 1) return;
            value--;
            $('#quantityWantBy').val(value);

        });
        //butto Plus click
        $('.btn-plus').click(function () {
            let value = parseInt($('#quantityWantBy').val());
            if (value == 0) return;
            value++;
            $('#quantityWantBy').val(value);
        });
        $('#user-rating .fa-star').on('click', function () {
            ratedIndex = parseInt($(this).data('index')) + 1;
            localStorage.setItem('ratedIndex', ratedIndex);

            self.resetStarColor();
            self.setStars(ratedIndex);

        })
        $('#btn-submit').click(function () {

            var content = document.getElementById('user-comment').value;
            var rating = ratedIndex + 1;
            if (content != '' && content != null) {
                self.saveComment(self.idCustomer, self.idProduct, self.idShop, content, ratedIndex);
            }
            else {
               // alert('Bình luận không được để trống');
            }

        })
        $('#addToCart').click(function () {
            let numberBuy = parseInt($('#quantityWantBy').val());

            self.createOrder(self.idCustomer, self.idProduct, numberBuy);

            var $addCart = $('#addToCart');
            $addCart.on('click', function (e) {
            e.preventDefault();

            var $link = $(this),
                $img = $('#first-img');

          //   add a copy of the image to the document
            var $ghost = $img.clone().appendTo($link).addClass('ghost');

            var imgCoords = $img.offset(),
                $target = $('#name-account'),
                targetCoords = $target.offset();

            $ghost.animate({
                'left': 100,
                'top': 100,
                'opacity': 0,
                'width': '30px'
            }, 1500, 'easeOutBounce', function () {
                $(this).remove();
                  $target.append('Added 1 ' + $link.find('h4').text() + '.<br>');
            });
            });


        })
        $('#goCheckout').click(function () {
            var idCus = '2';
            writeCookie("sessionID", idCus, 0.3);
            if (localStorage.getItem("infoCus") != null)
                window.location.href = "../../Login/signIn.html"
            else
                window.location.href = "Checkout.html";
        })

    }




    //save to cart
    createOrder(idAcc, idPro, quantity) {

        // alert(idCus + " " + idPro);
        // self = this;
        // var URL = self.getUrlApi(window.orderState);
        var URL = "/api/product/createOrder/" + idAcc + "/" + idPro + "/" + quantity;
        $.ajax({
            url: URL,
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            // alert('Thêm thành công');
            //self.loadCountCart(idCus);
            //Gọi hàm load dữ liệu (ở trên :()
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }
    //get respone from API,fill product which select in this page (main product)
    fillDataTransport(response) {
        self = this;

        window.countTotal = 0;
        window.countDone = 0;
        var totalSum = 0;


        $.each(response, function (index, item) {
            window.countTotal++;
            if (item.state == 'complete')
                window.countDone++;

            $('#nameProduct').text(item.name);
            document.getElementById("nameProduct").innerHTML = item.name;
            //Thảo format data
            let xRating = item.rating;
            for (let i = 1; i <= xRating; i++) {
                let rating = ' <i class="fa fa-star my-start-main"></i>';
                $('#detailproduct-rating').append(rating);
            }

            if (item.idVoucher > 0) {

                $('#priceOrigin').html(String(item.price));
                $('#scale-discount').html(String(item.discount * 100));
                $('#priceProduct').html(String(item.price - item.price * item.discount));
            }
            else {
                document.getElementById("priceOrigin").style.display = "none";
                document.getElementById("scale-discount").style.display = "none";
                $('#priceProduct').html(String(item.price));
            }

            var specification = '<p class="red-deep">Tác giả (biên dịch) :' + item.author + '</p> <p class="red-deep">Nhà sản xuất: ' + item.publisher + '</p>';
            $('#p1-author').append(specification);
            $('#p1-description').html(item.description);

            var pro_manufac = '<span  class="orange-light">' + item.publisher + '</span>';
            var pro_author = '<span  class="orange-light">' + item.author + '</span>';


            $('#pro_manufac').append(pro_manufac);
            $('#pro_author').append(pro_author);

        });
    }

    // API get comment in this product
    fillDataTransportComment(response) {
        self = this;

        window.countTotal = 0;
        window.countDone = 0;

        var totalSum = 0;

        document.getElementById('bootstrap-tab-text-grid').innerHTML = "";
        $.each(response, function (index, item) {
            window.countTotal++;
            if (item.state == 'complete')
                window.countDone++;

            totalSum++;
            //code add comment by js
            var divPreview = document.createElement("div");
            divPreview.setAttribute("class", "reviewer");

            // Name of customer ' comment
            var h4Name = document.createElement("h6");
            var h4NameValue = document.createTextNode(item.name);
            h4Name.appendChild(h4NameValue);
            //Date posting
            divPreview.appendChild(h4Name);

            var span12 = document.createElement("span");
            var span12Text = document.createTextNode(String(item.datePost));
            span12.appendChild(span12Text);
            divPreview.appendChild(span12);
            document.getElementById("bootstrap-tab-text-grid").appendChild(divPreview);
            //Rating
            //var divRating = document.createElement("div");
            //divRating.setAttribute("class", "ratting");


            //var pContent = document.createElement("p");
            //var pValue = document.createTextNode(item.content);
            //pContent.appendChild(pValue);
            //divPreview.appendChild(pContent);
            //  document.getElementById("bootstrap-tab-text-grid").appendChild(divPreview);

            let xRating = item.rating;
            var i_rating = ' <div class="ratting">';
            for (var i = 1; i <= xRating; i++) {
                i_rating += '<i class="far fa-star my-start-rating"></i>';
            }
            $('#bootstrap-tab-text-grid').append(i_rating);
            //   i_rating += '</div> ';
            let pContent = '<p>' + item.content + '</p>';
            $('#bootstrap-tab-text-grid').append(pContent);
            //  divPreview.appendChild(i_rating);

            //divPreview.appendChild(i_rating);


        });
    }
    loadCountCart(idCus) {

        self = this;
        idCus = 1;
        // var URL = self.getUrlApi(window.orderState);
        var URL = "/api/product/quantityOfInCart/" + idCus;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {

            $('#span-Number').text(response);
            //Gọi hàm load dữ liệu (ở trên :()
        }).fail(function (response) {
          //  alert("Hệ thống đang trong thời gian bảo trì, count cart!");
        });
    }

    // Đã chuyển
    getUrlApi(state) {
        switch (state) {
            case 'all': case null: return "/api/product/list-product";
            default: return "/api/cart/list-product/" + state;
        }
    }

    getPhoto(idProduct) {
        //Gọi hàm load dữ liệu (ở trên :()
        $.ajax({
            url: "/api/product/photosDetail/" + idProduct,
            method: "GET",
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json' //định nghĩa type data trả về.
            },
            datatype: "json", //kiểu dữ liệu truyền lên.
        }).done(function (response) {

            if (response.toString().length >= 1) {
                $('#first-img').attr('src', 'data:image/png;base64,' + response[0].photo);
                $('#thum-first').attr('data-thumb', 'data:image/png;base64,' + response[0].photo);
            }

            if (response.toString().length >= 2) {
                $('#second-img').attr('src', 'data:image/png;base64,' + response[1].photo);
                $('#thum-second').attr('data-thumb', 'data:image/png;base64,' + response[1].photo);
            }
            if (response.toString().length >= 3) {
                $('#third-img').attr('src', 'data:image/png;base64,' + response[2].photo);
                $('#thum-third').attr('data-thumb', 'data:image/png;base64,' + response[2].photo);
            }

        }).fail(function (response) {
            //alert("hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });

    }
    getTransport(idProduct) {
        self = this;
        // var URL = self.getUrlApi(window.orderState);
        var URL = "/api/product/detailproduct/" + idProduct;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataTransport(response);
            self.getPhoto(idProduct);

        }).fail(function (response) {
           // alert("Hệ thống đang trong thời gian bảo trì , chi tiết!");
        });
    }
    getDetailComment(idProduct) {
        self = this;

        var URL = "/api/product/comment/" + idProduct;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataTransportComment(response);  //Gọi hàm load dữ liệu (ở trên :(   )
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, comment!");
        });
    }
    //save comment

    //For make rating
    resetStarColor() {

        for (var i = 0; i < 5; i++) {
            $('#user-rating  .fa-star:eq(' + i + ')').css('font-weight', 'normal');
        }
    }
    setStars(max) {
        self.resetStarColor();
        for (var i = 1; i <= max; i++) {

            $('#user-rating  .fa-star:eq(' + (i - 1) + ')').css('font-weight', 900);
        }
    }
    clearComment() {
        document.getElementById('user-comment').value = "";
        self.resetStarColor();
    }
    saveComment(idCus, idPro, idShop, content, rating) {

        self = this;
        var Comment = {
            IDCustomer: idCus,
            IDProduct: idPro,
            IDShop: idShop,
            Content: content,
            Rating: rating
        };
        $.ajax({
            url: "/api/product/createComment",
            method: "POST",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(Comment),
            traditional: true
        }).done(function (response) {
            if (response) {
                //self.getListTransport();

                self.clearComment();
                self.getDetailComment(idPro);
            //    alert("Để lại ý kiến thành công!!! Lưu ý, nếu bạn chưa mua hàng, bạn chỉ có thể để lại bình luận, rating sẽ được xem xét");



            }
            else {
               // alert("Cập nhật không thành công!");
            }

        }).fail(function (response) {
           // alert("Hiện tại ứng dụng đang được bảo trì, vui lòng thử lại sau!");
        });
    }

    //using 
    //Thao- Get list Book of perional customer-- cho xu li
    getlisttransportdetail(idCus) {
        self = this;
        $.ajax({
            url: '/api/product/list-product/' + idCus,
            method: "get",
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json' //định nghĩa type data trả về.
            },
            datatype: "json", //kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.filldatatransportdetail(response);


        }).fail(function (response) {
            //alert("hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }
    getSuggestBook(idProduct) {
        self = this;
        $.ajax({
            url: '/api/product/suggestBook/' + idProduct,
            method: "get",
            headers: {
                'accept': 'application/json',
                'content-type': 'application/json' //định nghĩa type data trả về.
            },
            datatype: "json", //kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.loadSuggestBook(response);


        }).fail(function (response) {
          //  alert("hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    loadSuggestBook(response) {
        self = this;
        //$('#display-suggest').empty();
        if (response.toString().length > 0) {
            $.each(response, function (index, item) {

                var strHTML = $(` <div class="item item-size">
                                <div class="gd-box-info text-center">
                                    <div class="women_two bot-gd">
                                        <div class="product-googles-info slide-img googles">
                                            <div class="men-pro-item">
                                                <div class="men-thumb-item">
                                                    <img src="data:image/png;base64,`+ item.photo + `" class="img-item" alt="">
                                                    <div class="men-cart-pro">
                                                        <div class="inner-men-cart-pro">
                                                            <a href="DetailProductPage.html?ID=`+ item.id + `" class="link-product-add-cart">Quick View</a>
                                                        </div>
                                                    </div>
                                                    <span class="product-new-top">New</span>
                                                </div>
                                                <div class="item-info-product">

                                                    <div class="info-product-price">
                                                        <div class="grid_meta">
                                                            <div class="product_price">
                                                                <h4>
                                                                    <a href="DetailProductPage.html?ID=`+ item.id + `">` + item.name + ` </a>
                                                                </h4>
                                                                <div class="grid-price mt-2">
                                                                    <span class="money ">`+ item.price + `</span>
                                                                </div>
                                                            </div>
                                                            <ul class="stars">
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">
                                                                        <i class="fa fa-star-o" aria-hidden="true"></i>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="googles single-item hvr-outline-out">

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>`);

                $('#display-suggest').append(strHTML);

                for (var i = 0; i < item.rating - 1; i++) {
                    var ratting = document.getElementsByClassName("stars")[index];
                    ratting.getElementsByClassName("fa-star")[i].style.color = "yellow";
                }

            });
        }
    }

}
