﻿//import { add } from "./Home/minicart";

function readCookie(name) {    //OK
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return '';
}

$(document).ready(function () {


    var arrProPay = readCookie("listBuy");
    var paraID = localStorage.getItem("inforCus")
    if (paraID != null) {
        getNameCus(paraID);
        //getCategory();
        var paymentJS = new PaymentJS(paraID, arrProPay);
        window.paymentJS = paymentJS;

        window.resultUpdateOrderState = true;
        window.completionRate = 1;

    }


});
function getNameCus(idCus) {
    var URL = "/api/customer/getCustomerInfo/" + idCus;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {

        if (localStorage.getItem("inforCus") == "null") {
            var strHtml = $(` <li class="button-log">
                            <a class="btn-open" >
                                <span class="fas fa-user" aria-hidden="true"></span> SigIn
                            </a>
                        </li>`);
            $('#purchase').prepend(strHtml);
        }
        else {
           
            var strHTML = $(`<li class="button-log" style=" color:black;font-size:20px; font-family:'Catfish blues';text-decoration:none;cursor:pointer;">
                            <a  href="ProfilePage.html" class="btn-open" id="name-account"><span class="fas fa-user" aria-hidden="true"></span>`+ response.Name + ` </a>
                        </li>`);
            $('#purchase').prepend(strHTML);
        }
    }).fail(function (response) {
    });

}
function loadCategory(listcategory) {
    $('#nav-category').empty();
    var container = document.getElementById("nav-category");
    $.each(listcategory, function (index, item) {


        var divcontaint = document.createElement("div");
        divcontaint.setAttribute("class", "col-md-4 media-list span4 text-left");


        var h5titleCate = document.createElement("h5");
        h5titleCate.setAttribute("class", "tittle-w3layouts-sub");
        var content = document.createTextNode(item.Name);

        h5titleCate.appendChild(content);

        divcontaint.appendChild(h5titleCate);

        container.appendChild(divcontaint);

        getTypeProductWithCate(item.ID, index);
    });
}
//Load category trên thanh narbar
function getCategory() {
    var URL = "";
    $.ajax({
        url: "/api/Category/type",
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {
        loadCategory(response);

    }).fail(function (response) {
      //  alert("Lỗi category");
    });
}

class PaymentJS {

    constructor(paraID, arrProPay) {
        this.feeShipping = 30000;
        this.totalPriceNotShipping = 0;
        this.idCustomer = paraID;
        this.idProduct = 1;
        this.idShop = 1;
        this.idndexDel = 1;
        this.idVoucher = 1; //Default
        this.idPayment = 1; //Default :)
        this.discount = 1;
        this.listVoucher = [];
        this.products = JSON.parse(arrProPay);

        this.initEvents();

    }

    //End function for element GUI
    initEvents() {
        self = this;
        window.orderState = 'all';

        self.getDelivery();

        //self.getVoucher();
        self.getCustomerInfo(self.idCustomer);
        if (self.products.length > 0) {
            self.getTransport(self.idCustomer);
            $('#btnBuying').click(function () {
                self.getValuebill();

            });
        }

    }

    getQuantityInCart(idCus) {          //OK

        self = this;

        var URL = "/api/product/quantityOfInCart/" + idCus;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {

            $('#span-Number').text(response);
            //Gọi hàm load dữ liệu (ở trên :()
        }).fail(function (response) {
           // alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    getTransport(idCus) {
        self = this;

        var listProducts = self.products;
        // var URL = self.getUrlApi(window.orderState);
        var URL = "/api/cart/listSelectPro/" + self.idCustomer;
        $.ajax({
            url: URL,
            method: "POST",

            dataType: 'json', //Kiểu dữ liệu truyền lên.
            contentType: 'application/json',
            data: JSON.stringify(listProducts),
            traditional: true
        }).done(function (response) {
            if (!$.trim(response)) {
                var h3 = `<h3>Your cart is empty! Click <a href="HomePage.html">Here</a> to continue shopping</h3>`;
                $('#tableCart').empty();
                $('#tableCart').append(h3);

            }
            else {
                self.fillProductCheckout(response);  //Gọi hàm load dữ liệu (ở trên :()
            }
            self.getQuantityInCart(self.idCustomer);

        }).fail(function (response) {
         //   alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    getDelivery() {
        self = this;
        // var URL = self.getUrlApi(window.orderState);

        var URL = "/api/bill/delivery";
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDelivery(response);
        }).fail(function (response) {
            //alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    getVoucher() {
        self = this;
        // var URL = self.getUrlApi(window.orderState);

        var URL = "/api/bill/voucher";
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillVoucher(response); 
        }).fail(function (response) {
           // alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    getCustomerInfo(idCus) {
        self = this;

        // var URL = self.getUrlApi(window.orderState);
        var URL = "/api/cart/customerInfo/" + idCus;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {

            self.fillCustomer(response); //Gọi hàm load dữ liệu (ở trên :()

        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    fillProductCheckout(response) {
        self = this;

        window.countTotal = 0;
        window.countDone = 0;
        let count = 1;
        let totalPrice = 0;
        $('#myTable').empty();
        // self.discount = response[0].discount;

        $.each(response, function (index, item) {
            window.countTotal++;
            if (item.state == 'complete')
                window.countDone++;
            let priceFinal;
            if (item.idVoucher > 0) {  // Sản phẩm có áp dụng giảm giá
                priceFinal = item.price * (1 - item.discount);
            }
            else { //Sản phẩm không áp dụng mã giảm giá
                priceFinal = item.price;
            }


            // Lặp qua array item được select, gọi RestFull GET
            var table = document.getElementById("myTable");

            let cell = document.createElement("tr");

            let col = document.createElement("td");
            col.setAttribute("class", "th-row");
            let colValue = document.createTextNode(count);
            col.appendChild(colValue);
            cell.appendChild(col);

            let col1 = document.createElement("td");
            col1.setAttribute("class", "th-row");
            let imageBook = document.createElement("img");
            imageBook.setAttribute("class", "item-image");
            imageBook.setAttribute("src", 'data:image/png;base64,' + item.photo);
            col1.appendChild(imageBook);
            cell.appendChild(col1);

            let col2 = document.createElement("td");
            col2.setAttribute("class", "th-row");
            let colValue2 = document.createTextNode(item.name);
            col2.appendChild(colValue2);
            cell.appendChild(col2);


            let col4 = document.createElement("td");
            col4.setAttribute("class", "th-row");
            let colValue4 = document.createTextNode(priceFinal.toLocaleString('en-US', {
                valute: 'USD',
            }));
            col4.appendChild(colValue4);
            cell.appendChild(col4);


            let col5 = document.createElement("td");
            col5.setAttribute("class", "th-row");
            let colValue5 = document.createTextNode(item.quantity);
            col5.appendChild(colValue5);
            cell.appendChild(col5);

            let col6 = document.createElement("td");
            col6.setAttribute("class", "th-row");

            let colValue6 = document.createTextNode((item.quantity * priceFinal).toLocaleString('en-US', {
                valute: 'USD',
            }));
            col6.appendChild(colValue6);
            cell.appendChild(col6);

            document.getElementById("myTable").appendChild(cell);

             
            totalPrice += priceFinal * item.quantity;
            count++;

        });

        $('#money-subTotal').html(totalPrice.toLocaleString('en-US', {
            valute: 'VND',
        }));
        self.totalPriceNotShipping = totalPrice;
        $('#selectionDelivery').html(self.feeShipping.toLocaleString('en-US', {
            valute: 'USD',
        }));
        //  $('#apply-Voucher').html('-' + (self.discount == 1) ? '0' : (totalPrice * (1 - self.discount)));
        //$('#apply-Voucher').html('-' + (totalPrice * self.discount).toLocaleString('en-US', {
        //    valute: 'USD',
        //}));


        $('#money-mainTotal').html((totalPrice + self.feeShipping).toLocaleString('en-US', {
            valute: 'USD',
        }));

        $('#btn-submit1').on('click', function () {
            // Get value in current
            let cr_phone = $('#phoneCustomer').val();

            let cr_addCus = $('#addressCustomer').val();
            let cr_totalBill = self.totalPriceNotShipping + self.feeShipping;

            //pass to function
            if (cr_phone === "") {
                // alert("your pone is need to contact!!! Fill it");
                $('#pop-contain-header').html("Có gì đó chưa đúng");
                $('#pop-contain-voucher').html("Hình như bạn quên điền số điện thoại đó, kiểm tra lại đi nè!!!");
                document.getElementById("popup").style.display = "block";
            }
            else if (cr_addCus === "") {
                //alert("Where I can delivery for you??");
                $('#pop-contain-header').html("Có gì đó chưa đúng");
                $('#pop-contain-voucher').html("Hình như bạn quên điền địa chỉ nhận hàng đó, làm sao tụi mình chuyển sách cho bạn đây, kiểm tra lại đi nè!!!");
                document.getElementById("popup").style.display = "block";
            }
            else
                self.setNewBill(self.idCustomer, self.idndexDel, self.idPayment, cr_addCus, cr_phone, self.feeShipping, cr_totalBill);
            //alert("Where I can delivery for you??");


        }); 

        $('#btn-close').click(function () {
            document.getElementById("popup").style.display = "none";
        });
    }


    fillCustomer(response) {
        self = this;

        window.countTotal = 0;
        window.countDone = 0;
        var opDelivery = '';
        $.each(response, function (index, item) {
            window.countTotal++;
            if (item.state == 'complete')
                window.countDone++;
            $('#nameCustomer').val(item.name);
            $('#phoneCustomer').val(item.phone);
            $('#addressCustomer').val(item.address);

        });
    }

    fillDelivery(response) {
        self = this;

        window.countTotal = 0;
        window.countDone = 0;
        var opDelivery = '';
        if (!$.trim(response)) {
            self.feeShipping = 1700;
        }
        else   // Has data
        {
            self.feeShipping = response[0].feeShip;

        }

        $('#selectionDelivery').val(self.feeShipping);
        self.updateBillSelectionChange();  //Gọi hàm tính toán lại giá trị của hóa đơn

    }

    fillVoucher(response) {
        self = this;

        window.countTotal = 0;
        window.countDone = 0;
        var opVoucher = '';
        //localStorage.setItem('discount', response[0].discount);
       
      
        $('#selectionVoucher').empty();

        if (!$.trim(response)) {   // No voucher found
            self.discount = 0;
            self.listVoucher = 0;
            opVoucher += '<option class="opVoucher" >'
                + "Hiện không có khuyến mãi nào " + ' (giảm ' + 0 + ' ) %</label></option> ';
        }
        else {
            self.listVoucher = response;
            self.discount = response[0].discount;
            $.each(response, function (index, item) {
                window.countTotal++;

                if (item.state == 'complete')
                    window.countDone++;
                opVoucher += '<option class="opVoucher" >'
                    + item.name + ' (giảm ' + item.discount * 100 + ' ) %</label></option> ';

            });
        }
        $('#selectionVoucher').append(opVoucher);
        $('#selectionVoucher').change(function () {
      
            let value = $('#selectionVoucher :selected').text();
            let myresult = value.substring(
                value.lastIndexOf("(giảm ") + 6,
                value.lastIndexOf(" )"));

            self.idVoucher = $(this)[0].selectedIndex;

            self.discount = parseInt(myresult) / 100;
            self.updateBillSelectionChange();     //Gọi hàm tính toán lại giá trị của hóa đơn
            $('#pop-contain-header').html(self.listVoucher[self.idVoucher].name);
            $('#pop-contain-voucher').html(self.listVoucher[self.idVoucher].detail);
            document.getElementById("popup").style.display = "block";
        });
        //Event for pop up
        $('#btn-close').click(function () {
            document.getElementById("popup").style.display = "none";
        })

    }
    //Update Sau khi đổi đơn vị vận chuyển
    updateBillSelectionChange() {

        //$('#apply-Delivery').html(self.feeShipping.toLocaleString('en-US', {
        //    valute: 'USD',
        //}));
        //$('#apply-Voucher').html('-' + (self.discount * self.totalPriceNotShipping).toLocaleString('en-US', {
        //    valute: 'USD',
        //}));
        //$('#money-mainTotal').html((self.totalPriceNotShipping - self.totalPriceNotShipping * (self.discount) + self.feeShipping).toLocaleString('en-US', {
        //    valute: 'USD',
        //}));

        $('#apply-Delivery').html(self.feeShipping);
        //$('#apply-Voucher').html('-' + (self.discount * self.totalPriceNotShipping).toLocaleString('en-US', {
        //    valute: 'USD',
        //}));
        $('#money-mainTotal').html((self.totalPriceNotShipping + self.feeShipping).toLocaleString('en-US', {
            valute: 'USD',
        }));
    }

    setNewBill(idCus, idDel, idPay, add, phone, feeShip, totalCost) {

        self = this;
        var bill = {
            IDCustomer: idCus,
            IDDelivery: idDel,
            IDPayment: self.idPayment,
            IDVoucher: self.idVoucher,
            addressReceive: add,
            Phone: phone,
            FeeShip: feeShip,
            TotalCost: totalCost
        };

        $.ajax({
            url: "/api/bill/createBill",
            method: "POST",
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(bill),
            traditional: true
        }).done(function (response) {

            if (response) {
            
                var listProducts = self.products;

                $.ajax({
                    url: "/api/bill/updateBillDetail/" + self.idCustomer,
                    method: "POST",
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify(listProducts),
                    traditional: true
                }).done(function (response) {
                    $('#pop-contain-header').html("OPTIMA xin cảm ơn");
                    $('#pop-contain-voucher').html("Đơn hàng đã được ghi nhận, cảm ơn bạn đã mua hàng tại OPTIMA!!! "
                        + "<p> Chúc bạn có một ngày tuyệt vời!!!  </p> <p>Tiếp tục mua sắm nào!!!</p> ");
                    $('#btn-close').click(function () {
                        window.location.href = "/Views/Pages/Frontend/Home/Checkout.html";
                    })
                    document.getElementById("popup").style.display = "block";

                    window.location.href = "/Views/Pages/Frontend/Home/Checkout.html";


                }).fail(function (response) {

                    $('#pop-contain-header').html("Có gì đó chưa đúng");
                    $('#pop-contain-voucher').html("Hình như bạn quên điền số điện thoại đó, kiểm tra lại đi nè!!!");
                    document.getElementById("popup").style.display = "block";
                });

                self.getTransport(self.idCustomer);

            }
            else {
                $('#pop-contain-header').html("Có gì đó chưa đúng");
                $('#pop-contain-voucher').html("Hình như bạn quên điền số điện thoại đó, kiểm tra lại đi nè!!!");
                document.getElementById("popup").style.display = "block";
            }
            window.resultUpdateOrderState = response;
        }).fail(function (response) {
            alert("Hiện tại ứng dụng đang được bảo trì, vui lòng thử lại sau!");
        });


    }
}