﻿function openTab(evt, idelement, content, type) {
    var i, tabcontent, tablinks;
    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tab-change");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.background = "grey";
    }
    var nametype = document.getElementById(idelement).innerHTML;
    window.productJS.getListbookWithnametype(nametype, content);

    document.getElementById(type).style.display = "block";
    document.getElementById(idelement).style.background = "Blue";
}

function getNameCus(idCus) {
    var URL = "/api/customer/getCustomerInfo/" + idCus;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {

        if (response == null) {
            var strHtml = $(` <ul class="notlogin">
                                <li class="item-notlogin"><a href="../Register/LoginPage.html">Đăng nhập</a></li>
                                <li class="item-notlogin"><a href="../Register/SignupPage.html">Đăng kí tài khoản</a></li>
                            </ul>`);
            $('#_desktop_user_info').prepend(strHtml);
        } else {


            var strHTML = $(` <ul class="login">
                            <li class="item-login"><a href="ProfilePage.html">`+ response.Name +` <i class="fas fa-user" aria-hidden="true"></i></a></li>
                            <li class="item-login" id="logout"><a >Đăng xuất <i class="fas fa-sign-out-alt" ></i> </a></li>
                        </ul>`);
            $('#_desktop_user_info').prepend(strHTML);

        }
    }).fail(function (response) { });
}

$('#go-to-cart').click(function () {
    if (localStorage.getItem("inforCus") != "null") {
        window.location.href = "Checkout.html";
    } else
        window.location.href = "../Register/LoginPage.html";
});

$('#logout').click(function () {
  //  alert("logout");
    if (localStorage.getItem("inforCus") != "null")
        localStorage.setItem("inforCus", "null");
    window.location.href = "../Register/LoginPage.html"
});

$(document).ready(function () {
    
    var productJS = new ProductJS();
    window.productJS = productJS;
    //số lượng một sản phẩm bất kì trong cart 


})



//Lây danh sách cuốn sách theo thể loại khi click 


//====================================================================================//

class ProductJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        self = this;
       // self.getCategory();
        self.gettheBestsaler();
        self.getNewestBook();
        self.getListHotDealbook();
        self.getProudctHighestScore();

    }

    checkquantity(idpro, idCus) {
        self = this;
        var URL = "/api/product/checkquantity/" + idCus + "/" + idpro;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {

            this.Amountproductincart = response;

        }).fail(function (response) {
            //alert("khong check quantity được");
        });
    }

    loadListHotdeal(response) {
        self = this;
        $('#display-hotdeal').empty();
        $.each(response, function (index, item) {
           
                var strHTML = $(` <div class="col-lg-3 col-md-4" >
                            <div class="product-item">
                                 <div class="lbdiscount">
                                        <label class="discount my-discount">`
                                         +item.discount*100+       `</label>
                                    </div>
                                <div class="product-title">
                                    <a>` + item.name + `</a>
                                    <div class="ratting">
                                        <i class="fa fa-star" style="color:grey;"></i>
                                        <i class="fa fa-star" style="color:grey;" ></i>
                                        <i class="fa fa-star" style="color:grey;"></i>
                                        <i class="fa fa-star" style="color:grey;"></i>
                                        <i class="fa fa-star" style="color:grey;"></i>
                                    </div>
                                    
                                </div>
                                <div class="product-image">
                                    <a href="product-detail.html">
                                        <img src="data:image/png;base64,`+ item.photo + `" alt="Product Image">
                                    </a>
                                    <div class="product-action">
                                            
                                        <label style="display:none; color:red; " class="id-hotdeal">` + item.id + `</label>
                                        <a class="add-to-cart-hot"><i class="fa fa-cart-plus"></i></a>
                                        <a class="viewdetail-hotdeal" ><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <div class="product-price">
                                    <h3 style="font-size:16px; color:#FFBE01; ">` + item.price + `<span> Đ</span></h3>
                                    <a class="btn" href="">Buy Now</a>
                                </div>
                            </div>
                        </div>`);

                $('#display-hotdeal').append(strHTML);

                for (var i = 0; i < item.rating - 1; i++) {
                    var ratting = document.getElementsByClassName("ratting")[index ];
                    ratting.getElementsByClassName("fa-star")[i].style.color = "yellow";
                }
            
        });

        //Event click add to cart

        var buttonaddtocart = document.getElementsByClassName("add-to-cart-hot");
        for (var i = 0; i < buttonaddtocart.length; i++) {
            var add = buttonaddtocart[i];
            add.onclick = function (event) {
                if (localStorage.getItem("inforCus") != null) {
                    var button = event.target;
                    var product = button.parentElement.parentElement;
                    var idpro = product.getElementsByClassName("id-hotdeal")[0].innerText;
                    var idcus = localStorage.getItem("inforCus");
                    var idshop = 1;
                    //add to cart
                    window.productJS.HandleCart(idcus, idpro, idshop);

                    var $addCart = $('#go-to-cart');
                    //$addCart.on('click', function (e) {
                    //e.preventDefault();

                    var $link = $(this),
                        $img = $('#img-logo');

                    // add a copy of the image to the document
                    var $ghost = $img.clone().appendTo($link).addClass('ghost');

                    var imgCoords = $img.offset(),
                        $target = $('#go-to-cart'),
                        targetCoords = $target.offset();

                    $ghost.animate({
                        'left': targetCoords.left + imgCoords.left,
                        'top': -200,
                        'opacity': 0,
                        'width': '30px'
                    }, 1500, 'easeOutBounce', function () {
                        $(this).remove();
                        //  $target.append('Added 1 ' + $link.find('h4').text() + '.<br>');
                    });

                    return false;
                } else {
                    window.location.href = "../Register/LoginPage.html";
                }


            };
        }

        var viewDetail = document.getElementsByClassName("viewdetail-hotdeal");
        for (var i = 0; i < viewDetail.length; i++) {
            var button = viewDetail[i];
            button.onclick = function (event) {
                var t = event.target;
                // Store
                window.location.href = "DetailProductPage.html?ID=" + t.parentElement.parentElement.getElementsByClassName("id-hotdeal")[0].innerText;
            };
        }

    }

    //get products having discount
    getListHotDealbook() {
        self = this;

        var URL = "/api/product/listallHotdeal";
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.loadListHotdeal(response);


        }).fail(function (response) {
            // alert("Lỗi load hot deal");
        });
    }



    loadNewestBook(response) {
        self = this;
        $('#display-newest').empty();
        $.each(response, function (index, item) {
           
                var strHTML = $(`<div class="col-md-3 product-men women_two">
                            <div class="product-googles-info googles">
                                <div class="men-pro-item">
                                    <div class="men-thumb-item">
                                        <img src="data:image/png;base64,`+ item.photo + `" class="img-fluid" alt=" ">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                 <a class="id-PRO" style="display:none;">` + item.id+ `</>
                                                <a href="DetailProductPage.html?ID=`+item.id+`" class="link-product-add-cart">View</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item-info-product">
                                        <div class="info-product-price">
                                            <div class="grid_meta">
                                                <div class="product_price">
                                                    <h4>
                                                        <a href="single.html">` + item.name + `</a>
                                                    </h4>
                                                    <div class="grid-price mt-2">
                                                        <span class="money">`+item.price+`</span> <span> Đ</span>
                                                    </div>
                                                </div>
                                                <ul class="stars">
                                                    <li>
                                                        <a href="# ">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="# ">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="# ">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="# ">
                                                            <i class="fa fa-star" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="# ">
                                                            <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="googles single-item hvr-outline-out ">
                                                <form action="# " method="post ">
                                                    <label class="id-PRO" style="display:none;">` + item.id + `</label>
                                                    <input type="hidden" name="cmd " value="_cart">
                                                    <input type="hidden" name="add " value="1">
                                                    <input type="hidden" name="googles_item" value="Farenheit ">
                                                    <input type="hidden" name="amount " value="`+ item.price+ `">
                                                    <button type="submit" class="googles-cart pgoogles-cart ">
                                                        <i class="fas fa-cart-plus"></i>
                                                    </button>
                                                </form>

                                            </div>
                                        </div>
                                        <div class="clearfix "></div>
                                    </div>
                                </div>
                            </div>
                        </div>`);

                $('#display-newest').append(strHTML);

                for (var i = 0; i < item.Rating - 1; i++) {
                    var ratting = document.getElementsByClassName("stars")[index];
                    ratting.getElementsByClassName("fa-star")[i].style.color = "yellow";
                }
           
        });

        //su kien them vao gio hang

        var buttonaddtocart = document.getElementsByClassName("googles-cart");
        for (var i = 0; i < buttonaddtocart.length; i++) {
            var add = buttonaddtocart[i];
            add.onclick = function (event) {
                var button = event.target;
                var product = button.parentElement.parentElement;
                var idpro = product.getElementsByClassName("id-PRO")[0].innerHTML;
                var idcus = localStorage.getItem("inforCus");
                var idshop = 1;
                //THÊM VÀO GIỎ HÀNG
                window.productJS.HandleCart(idcus, idpro, idshop);

                return false;
            };
        }

        var viewDetail = document.getElementsByClassName("inner-men-cart-pro");
        for (var i = 0; i < viewDetail.length; i++) {
            var button = viewDetail[i];
            button.onclick = function (event) {
                var t = event.target;
                // Store
                window.location.href = "DetailProductPage.html?ID=" + t.parentElement.getElementsByClassName("id-PRO")[0].innerText;

            };
        }


    }

    getNewestBook() {
        self = this;
        $.ajax({
            url: "/api/product/listNewestproduct",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.loadNewestBook(response)
        }).fail(function () {
            //alert("Lỗi load sản phẩm mới nhất")
        });

    }

    //Lấy danh sách sản phẩm bán chạy nhất
    loadtheBestsaler(response) {
        window.count = 0;
        $('#best-saler-product').empty();
        $.each(response, function (index, item) {
                
                window.count++;
                var strHTML = $(` <div class="col-md-3 product-men women_two">
                        <div class="product-googles-info googles">
                            <div class="men-pro-item">
                                <div class="men-thumb-item"> 
                                    <img src="data:image/png;base64,`+ item.photo + `" class="img-fluid" alt="">
                                    <div class="men-cart-pro">
                                        <div class="inner-men-cart-pro">
                                              <a class="id-PRO" style="display:none;">` + item.id + `</>
                                            <a style="color:white;" class="link-product-add-cart">Quick View</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-info-product">
                                    <div class="info-product-price">
                                        <div class="grid_meta">
                                            <div class="product_price">
                                                <h4>
                                                    <a style="font-size:14px ;">` + item.name + `</a>
                                                </h4>
                                                <div class="grid-price mt-2">
                                                    <span class="money ">` + item.price + `</span>
                                                </div>
                                            </div>
                                            <ul class="stars">                     
                                               <li>
                                                    <a >
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a >
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a>
                                                        <i class="fa fa-star" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                        <div class="googles single-item hvr-outline-out">
                                                 <form action="#" method="post">
                                                    <label class="id-PRO" style="display:none;">` + item.id + `</label>
                                                    <input type="hidden" name="cmd" value="_cart">
                                                    <input type="hidden" name="add" value="1">
                                                    <input type="hidden" name="googles_item" value="Farenheit">
                                                    <input type="hidden" name="amount" value="575.00">
                                                    <button type="button" class="googles-cart pgoogles-cart">
                                                        <i class="fas fa-cart-plus"></i>
                                                    </button>
                                                 </form>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>`);

                $('#best-saler-product').append(strHTML); //chen duoi

                for (var i = 0; i < item.rating - 1; i++) {
                    var ratting = document.getElementsByClassName("stars")[index];
                    ratting.getElementsByClassName("fa-star")[i].style.color = "yellow";
                }

            
        });

        //tạo sự kiện cho button add to cart

        var buttonaddtocart = document.getElementsByClassName("googles-cart");
        for (var i = 0; i < buttonaddtocart.length; i++) {
            var add = buttonaddtocart[i];
            add.onclick = function (event) {
                var button = event.target;
                var product = button.parentElement.parentElement;
                var idpro = product.getElementsByClassName("id-PRO")[0].innerHTML;
                var idcus = localStorage.getItem("inforCus");
                var idshop = 1;
                //THÊM VÀO GIỎ HÀNG
                window.productJS.HandleCart(idcus, idpro, idshop);
                return false;
            };
        }

        var viewDetail = document.getElementsByClassName("inner-men-cart-pro");
        for (var i = 0; i < viewDetail.length; i++) {
            var button = viewDetail[i];
            button.onclick = function (event) {
                var t = event.target;
                // Store
                window.location.href = "DetailProductPage.html?ID=" + t.parentElement.getElementsByClassName("id-PRO")[0].innerText;

            };
        }
    }

    gettheBestsaler() {
        self = this;
        var URL = "/api/product/listbestsaler";
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.loadtheBestsaler(response);
        }).fail(function (response) {
            console.log(response);
        });
    }


    //Lấy danh sách loại sách
    loadListTypebook(response) {
        $("#Typebook-right").empty();
        $("#container-left").empty();
        var count = 0;
        $.each(response, function (index, item) {
            count++;
            var strHTML1 = $(`<div class="tab-change" id="type` + item.id + `">
                                <section class="slider">
                                    <ul id="autoWidth` + item.id + `" class="cs-hidden">
                                    </ul>
                                </section>
                              </div>`);
            $('#container-left').append(strHTML1);

            if (index == 0)
                var strHTML2 = $(`<button class="tablinks" id="btn_` + item.id + `" onclick="openTab(event,'btn_` + item.id + `','autoWidth` + item.id + `','type` + item.id + `')" >` + item.name + `</button>`)
            else
                var strHTML2 = $(`<button class="tablinks" id="btn_` + item.id + `" onclick="openTab(event,'btn_` + item.id + `','autoWidth` + item.id + `','type` + item.id + `')" >` + item.name + `</button>`)
            $('#Typebook-right').append(strHTML2);

        });
        document.getElementById("btn_1").onclick();

    }

    //Lấy danh sách thể loại sách
    getListTypebook() {
        self = this;
        var URL = "/api/product/listalltypeproduct";
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.loadListTypebook(response);
            //alert("Load xong thẻ loại");


        }).fail(function (response) {
            //alert("Lỗi load thể loại");
        });

    }


    //Lấy danh sách sách theo thể loại
    /*getListbookWithnametype(typename,autoWidthid) {
        self = this;
        var URL = "/api/product/listalltypeproduct/" + typename;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.loadListHotbook(response,autoWidthid);
        }).fail(function (response) {
            console.log(response);
            alert("Lỗi load sản phẩm");
        });

    }
    */

    getTypeProductWithCate(idCate, count) {
        var URL = "/api/Category/type/" + idCate;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "", //Kiểu dữ liệu truyền lên.
        }).done(function (products) {
            var ul = document.createElement("ul");
            $.each(products, function (index, item) {

                var li = document.createElement("li");
                li.setAttribute("class", "media-mini mt-3");

                var a = document.createElement("a");

                var text = document.createTextNode(item.Name);
                a.setAttribute("href", "ProductPage.html?type=" + item.ID);
                a.appendChild(text);
                li.appendChild(a);
                ul.appendChild(li);


            });
            var container = document.getElementsByClassName("col-md-4 media-list span4 text-left")[count];
            container.appendChild(ul);

        }).fail(function () {

        });
    }


    //SHOW THỂ LOẠI
    loadCategory(listcategory) {
        self = this;
        $('#nav-category').empty();
        var container = document.getElementById("nav-category");
        $.each(listcategory, function (index, item) {


            var divcontaint = document.createElement("div");
            divcontaint.setAttribute("class", "col-md-4 media-list span4 text-left");


            var h5titleCate = document.createElement("h5");
            h5titleCate.setAttribute("class", "tittle-w3layouts-sub");
            var content = document.createTextNode(item.Name);

            h5titleCate.appendChild(content);

            divcontaint.appendChild(h5titleCate);

            container.appendChild(divcontaint);

            self.getTypeProductWithCate(item.ID, index);
        });

    }

    //Load category trên thanh narbar
    getCategory() {
        self = this;
        var URL = "";
        $.ajax({
            url: "/api/Category/type",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.loadCategory(response);
            // self.gettheBestsaler();
        }).fail(function (response) {
            //  alert("Lỗi category");
        });
    }




    //Thêm sản phẩm vào giỏ
    HandleCart(idcus, idproduct, idshop) {
        var cart = {
            IDCustomer: idcus,
            IDproduct: idproduct,
            IDshop: idshop,
            Quantity: 1

            // Quantity: quantity
        };
     //   console.log(cart);

        $.ajax({
            url: "/api/product/addtocart",
            method: "POST",
            data: JSON.stringify(cart),
            contentType: "application/json",
            dataType: "json",
            traditional: true
        }).done(function (res) {
            if (res == true) {
                //alert("Sản phẩm đã thêm vào giỏ!");

                var $addCart = $('#go-to-cart');
                //$addCart.on('click', function (e) {
                //e.preventDefault();

                var $link = $(this),
                    $img = $('#img-logo');

                // add a copy of the image to the document
                var $ghost = $img.clone().appendTo($link).addClass('ghost');

                var imgCoords = $img.offset(),
                    $target = $('#go-to-cart'),
                    targetCoords = $target.offset();

                $ghost.animate({
                    'left': targetCoords.left + imgCoords.left,
                    'top': -200,
                    'opacity': 0,
                    'width': '30px'
                }, 1500, 'easeOutBounce', function () {
                    $(this).remove();
                    //  $target.append('Added 1 ' + $link.find('h4').text() + '.<br>');
                });
                // $(location).attr('href', '/Views/Pages/Login/signIn.html');
            } else {
              //  alert("Thêm vào giỏ hàng không thành công!")
            }
        }).fail(function (response) {
          //  alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
        });

    }

    //Dardboard


    loadDardboardUI(product) {
        //$('#carousel-inner').empty();
        $.each(product, function (index, item) {
         
                var HTML = $(`<div class="carousel-item">
                                <div class="row testimonials_grid text-center">
                                    <div class="col-md-3 img">
                                        <img src="data:image/png;base64,`+ item.photo + `" class="img-fluid"/>
                                    </div>
                                    <div class="col-md-9">
                                         <input type="hidden" class="id-Pro1" value=` + item.id + `>
                                        <h4 style="color:firebrick; display:none; font-size: 25px;font-weight: 700; font-family:Arial;">
                                            ` + (index) + `
                                        </h4>
                                        <h3 class="name-product">
                                            ` + item.name + `
                                        </h3>
                                        <label>` + item.author + `</label>
                                        <p>` + item.description + `</p>
                                    </div>
                                </div>
                            </div>`);
                $('#carousel-inner').append(HTML);
            
        });

        var carousel_inner = document.getElementById("carousel-inner");
        carousel_inner.getElementsByClassName("carousel-item")[0].classList.add("active");
        var linktoproduct = document.getElementsByClassName("name-product");
        for (var i = 0; i < linktoproduct.length; i++) {

            linktoproduct[i].onclick = function (event) {
                var name = event.target;
               // alert(name.parentElement.getElementsByClassName("id-Pro1")[0].value);
                window.location.href = "DetailProductPage.html?ID=" + name.parentElement.getElementsByClassName("id-Pro1")[0].value;

            }
        }
    }
    getProudctHighestScore() {
        self = this;
        var URL = "/api/product/highestproductscore";
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (products) {

            self.loadDardboardUI(products);

        }).fail(function () {
            // alert("fail darboard");
        });
    }



    commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        }
        val += ' VNĐ';
        return val;
    }
    BuyNow() {
        //Find id
        //Add to card
        //Go Bill
    }
}