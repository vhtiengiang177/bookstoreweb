﻿


$(document).ready(function () {
    getCategory();

});


function getTypeProductWithCate(idCate, count) {
    var URL = "/api/Category/type/" + idCate;
    $.ajax({
        url: URL,
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "", //Kiểu dữ liệu truyền lên.
    }).done(function (products) {
        var ol = document.createElement("ol");
        ol.setAttribute("class", "sub-sub-menu");
        $.each(products, function (index, item) {

            var li = document.createElement("li");
            li.setAttribute("class", "menu-item");

            var a = document.createElement("a");

            var text = document.createTextNode(item.Name);
            a.setAttribute("href", "ProductPage.html?type=" + item.ID);

            a.appendChild(text);
            li.appendChild(a);
            ol.appendChild(li);

        });
        var container = document.getElementsByClassName("menu-item category-item")[count];
        container.appendChild(ol);



    }).fail(function () {
        alert("lỗi load type");
    });
   
}


function loadCategory(listcategory) {
    $('#sub-menu').empty();
    var container = document.getElementById("sub-menu");
    $.each(listcategory, function (index, item) {


        var licontaint = document.createElement("li");
        licontaint.setAttribute("class", "menu-item category-item");

        var namecategory = document.createElement("a");
        var content = document.createTextNode(item.Name);

        namecategory.appendChild(content);
        licontaint.appendChild(namecategory);
        container.appendChild(licontaint);

        getTypeProductWithCate(item.ID, index);
    });

}

function getCategory() {
    var URL = "";
    $.ajax({
        url: "/api/Category/type",
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json", //Kiểu dữ liệu truyền lên.
    }).done(function (response) {
        loadCategory(response);

    }).fail(function (response) {
        alert("Lỗi category");
    });
}
