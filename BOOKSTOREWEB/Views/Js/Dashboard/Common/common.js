$(document).ready(function(){
    var common = new Common();
    window.timeHide = 400;
});

class Common{
    constructor(){
        this.initEvents();
    }

    initEvents()
    {
        $("#btnOrderForm").click(this.goOrder.bind(this));
        $("#btnTransport").click(this.goTransport.bind(this));
        $("#btnReport").click(this.goReport.bind(this));
        $("#btnAccount").click(this.showAccountForm.bind(this));
        $("#btnAccountProfile").click(this.goAccount.bind(this));
        $("#btnAccountManagerment").click(this.goAccountManagerment.bind(this));
        $("#btnProduct").click(this.showProduct.bind(this));
        $("#btnProducts").click(this.goProduct.bind(this));
        $("#btnAddProduct").click(this.goAddProduct.bind(this));
        $("#btnVoucher").click(this.showVoucher.bind(this));
        $("#btnVouchers").click(this.goVoucher.bind(this));
        $("#btnAddVoucher").click(this.goAddVoucher.bind(this));
        $("#btnProductsVoucher").click(this.goProductsVoucher.bind(this));
        $("#btnCustomer").click(this.goCustomer.bind(this));
        $("#btnOverview").click(this.goOverview.bind(this));
        $(".logo").click(this.goHomePage.bind(this));
    }

    goHomePage() {
        window.location.href = "/Views/Pages/Frontend/Home/HomePage.html";
    }

    goOverview() {
        window.location.href = "/Views/Pages/Dashboard/Home/home.html";
    }

    goCustomer() {
        window.location.href = "/Views/Pages/Dashboard/Home/customer.html";
    }

    goVoucher() {
        window.location.href = "/Views/Pages/Dashboard/Home/voucher.html";
    }

    goAddVoucher() {
        window.location.href = "/Views/Pages/Dashboard/Home/addvoucher.html";
    }

    goProductsVoucher() {
        window.location.href = "/Views/Pages/Dashboard/Home/productsAddvoucher.html";
    }

    goProduct() {
        window.location.href = "/Views/Pages/Dashboard/Home/product.html";
    }

    goAddProduct() {
        window.location.href = "/Views/Pages/Dashboard/Home/addproduct.html";
    }

    goTransport() {
        window.location.href = "/Views/Pages/Dashboard/Home/transport.html";
    }

    goReport() {
        window.location.href = "/Views/Pages/Dashboard/Home/report.html";
    }

    goOrder() {
        window.location.href = "/Views/Pages/Dashboard/Home/order.html";
    }

    goAccount() {
        window.location.href = "/Views/Pages/Dashboard/Home/account.html";
    }

    goAccountManagerment() {
        window.location.href = "/Views/Pages/Dashboard/Home/accountManagerment.html";
    }

    showVoucher() {
        this.hideOrderForm();
        this.hidePromote();
        this.hideAccountForm();
        this.hideProduct();
        $("#function-detail-voucher").slideToggle(window.timeHide);  
    }

    hideVoucher() {
        $("#function-detail-voucher").slideUp(window.timeHide);
    }

    hideOrderForm()
    {
        $("#function-detail-order-form").slideUp(window.timeHide);
    }

    showAccountForm() {
        this.hideOrderForm();
        this.hidePromote();
        this.hideProduct();
        this.hideVoucher();
        $("#function-detail-account").slideToggle(window.timeHide);   
    }


    hideAccountForm() {
        $("#function-detail-account").slideUp(window.timeHide);   
    }

    hideProduct()
    {
        $("#function-detail-product").slideUp(window.timeHide);
    }

    showProduct()
    {
        this.hideOrderForm();
        this.hidePromote();
        this.hideAccountForm();
        this.hideVoucher();
        $("#function-detail-product").slideToggle(window.timeHide);
    }

    hidePromote()
    {
        $("#function-detail-promote").slideUp(window.timeHide);
    }

    showPromote()
    {
        this.hideOrderForm();
        this.hideAccountForm();
        this.hideProduct();
        this.hideVoucher();
        $("#function-detail-promote").slideToggle(window.timeHide);
    }
}