﻿$(document).ready(function () {
    var deliveryJS = new DeliveryJS();
    window.deliveryJS = deliveryJS;
    
    
   

});

function deleteDelivery(self) {
    var id = self.id.replace('select-delete-delivery', '');
    $('#txtiddelete').replaceWith(` <input type="text" value="`+id+`"  id="txtiddelete" style="display:none" >`);
    window.deliveryJS.deleteDelivery();
}

function editDelivery(self) {
    var id = self.id.replace('select-edit-delivery', '');
    $('#btnEditDelivery').show();
    $('#btnAddnewDelivery').hide();
    $('.div-front-add-delivery').show(); 
    $('#check').replaceWith(` <input type="text" value="edit" id="check" style="display:none" >`);
    window.deliveryJS.getInfoDelivery(id);
   

}



//===================================================================//
class DeliveryJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        self = this;
        var temp = 0;
        this.getDataDelivery();
        $('#btnSearchDelivery').click(this.searchDeliveryByNameOrFee.bind(this));
        $('#btnAddDelivery').click(function () {
            $('#btnEditDelivery').hide();
            $('#check').replaceWith(` <input type="text" value="add" id="check" style="display:none" >`);
        });
        $('#btnAddDelivery').click(this.showAddDelivery.bind(this));
       
        $('#btnAddnewDelivery').click(this.checkAdd.bind(self));

        $('#icon-close-add-delivery').click(function () {
            $('#txtiddelivery').replaceWith(` <input type="text"  id="txtiddelivery" style="display:none" >`);
            $('#txtNameDelivery').replaceWith(` <input type="text" id="txtNameDelivery"   required />`);
            $('#txtfeedelivery').replaceWith(` <input type="number"   id="txtfeedelivery"     step="1000" required />`);
            $('.div-front-add-delivery').hide();
            location.reload();
        });

        $('#btnEditDelivery').click(this.checkAdd.bind(self));
    }

    checkAdd() {
        this.inputValidate();
        if (this.resultAdd == false)
            return;
        this.isExistNameDelivery();
    }

     // Kiểm tra đầu vào có hợp lệ hay không
    inputValidate() {
        var name = $('#txtNameDelivery').val();    
        var fee = $('#txtfeedelivery').val();
       
        this.resultAdd = true;
        if (name != null) {
            if (name == "") {
                $('#error-input-Namedelivery').html("Không được để trống tên đơn vị!");
                this.resultAdd = false;
            }
            else
                $('#error-input-Namedelivery').html("");
        }
        
        if (fee != null) {
            if (fee == "") {
                $("#error-input-Pricedelivery").html("Không được để trống phí vận chuyển!");
                this.resultAdd = false;
            }
            else
                $("#error-input-Pricedelivery").html("");
        }
       
    }


    //Lấy thông tin chi tiết KM
    getInfoDelivery(id) {
        self = this;
       
        $.ajax({
            url: '/api/delivery/list-info-delivery/' + id,
            method: 'GET',
            contentType: 'application/json',
            dataType: 'json'
        }).done(function (data) {
          
                $.each(data, function (index, item) {
                    $('#txtiddelivery').replaceWith(` <input type="text" value=` + item.id + ` id="txtiddelivery" style="display:none" >`);
                    $('#txtNameDelivery').replaceWith(` <input type="text" value="` + item.name + `" id="txtNameDelivery"   required />`);
                    $('#txtfeedelivery').replaceWith(` <input type="number" value=` + item.feeShip + `  id="txtfeedelivery"     step="1000" required />`);
                    
                });
            
           

        }).fail(function (response) {
            alert("Hiện tại ứng dụng đang được bảo trì, vui lòng thử lại sau!");
        });
    }

    //Kiểm tra tên đơn vị đã tồn tại
    isExistNameDelivery() {
        self = this;
        var delivery = {
            Name: $("#txtNameDelivery").val()
        }
        $.ajax({
            url: "/api/delivery/isexistnamedelivery",
            method: "POST",
            data: JSON.stringify(delivery),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                if ( $('#check').val() == "add")
                    self.addDelivery();
                else self.updateDelivery();
            }
            else {
                if ($('#check').val() == "edit")
                    self.updateDelivery();
                else
                    $("#error-input-Namedelivery").html("Đơn vị vận chuyển đã tồn tại!");
            }
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau!");
        });
    }


    updateDelivery() {
        self = this;
        var iddelivery = $("#txtiddelivery").val();
       
        var delivery = {
            ID: iddelivery,
            Name: $("#txtNameDelivery").val(),
            FeeShip: $("#txtfeedelivery").val(),
            
        };
        $.ajax({
            url: "/api/delivery/updateDelivery",
            method: "PUT",
            data: JSON.stringify(delivery),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response) {
                alert("Sửa thành công");
                location.reload();
            }

            else
                alert(" Thất bại");
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau!");
        });
    }


    deleteDelivery() {
        self = this;
        var iddelivery = $("#txtiddelete").val();

        var delivery = {
            ID: iddelivery
           

        };
        $.ajax({
            url: "/api/delivery/deleteDelivery",
            method: "PUT",
            data: JSON.stringify(delivery),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response) {
                alert("Xóa thành công");
                location.reload();
            }

            else
                alert(" Thất bại");
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau!");
        });
    }

    // Thêm đơn vị
    addDelivery() {
        var delivery = {
            Name: $("#txtNameDelivery").val(),
            FeeShip: $('#txtfeedelivery').val()
                     
        };

        $.ajax({
            url: "/api/delivery/addDelivery",
            method: "POST",
            data: JSON.stringify(delivery), // Truyền xuống thông qua body request.
            contentType: "application/json", //Kiểu dữ liệu trả về.
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response) {
                alert("Thêm thành công");
                location.reload();
            }
               
            else
                alert(" Thất bại");
        });
    }


    showAddDelivery() {
        $('#btnAddnewDelivery').show();
        $('.div-front-add-delivery').show();


    }

    // Lấy danh sách đơn vị vận chuyển
    getDataDelivery() {
        var self = this;

        $.ajax({
            url: '/api/delivery/list-delivery',
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataDelivery(response);

        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }


    // hiểm thị toàn bộ đơn vị vận chuyển
    fillDataDelivery(response) {
        console.log(response);
        self = this;

        $('#gird-delivery tbody').empty();
        $.each(response, function (index, item) {
            var idDeleteDelivery = 'select-delete-delivery' + item.id;
            var idEditDelivery = 'select-edit-delivery' + item.id;
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.name + `</td>
            <td>` + item.feeShip + `</td>
            //<td><a id=`+ idEditDelivery + ` onclick="editDelivery(this)" style="color:blue;" >Sửa</a>  <a id=` + idDeleteDelivery+` onclick="deleteDelivery(this)" style="color:red;">Xóa</a></td>
           
            </tr> `);
            $('#gird-delivery tbody').append(trHTML);

        });
    }

    //Search
    searchDeliveryByNameOrFee() {
        self = this;
        var search = $("#txtSearchDelivery").val();
        $.ajax({
            url: '/api/delivery/searchdelivery/' + search,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataDelivery(response);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }
}
