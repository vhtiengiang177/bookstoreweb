﻿$(document).ready(function () {
    var product = new ProductJS();
    window.product = product;
});

function deletePro(self) {
    var idPro = self.id.replace('delete-', '');
    window.product.deleteProduct(idPro);
}

function editPro(self) {
    var idPro = self.id.replace('edit-', '');
    $('.div-front').show();
    window.product.getListType();
    window.product.getInfoOneProduct(idPro);
}

// Xuất file excel
function exportToExcel() {
    var table = document.getElementById("table-product");
    var htmls = "";
    var uri = 'data:application/vnd.ms-excel;base64,';
    var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    var base64 = function (s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };

    var format = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    };

    htmls = table.outerHTML;

    var ctx = {
        worksheet: 'Product_Sheet',
        table: htmls
    }

    var date2 = new Date().toISOString().substr(0, 19).replace('T', ' ');
    var link = document.createElement("a");
    link.download = "ExportListProduct" + date2 + ".xls";
    link.href = uri + base64(format(template, ctx));
    link.click();
}

class ProductJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        var self = this;
        //$('#btnSearch').click(this.searchProductByNameOrTypes.bind(this));
        $('#icon-close').click(function () {
            $('.div-front').hide();
        });
        self.getListSearch();
        //self.getListProduct();
        $('#btnUpdateProduct1').click(this.checkUpdate.bind(self));
        $('#txtSearch').keyup(this.searchProductByNameOrTypes.bind(this));
        $('#type-product-dropdownlist-search').change(this.getListProduct.bind(self));
    }

    // Lấy danh sách sp
    getListProduct() {
        var self = this;
        var select = $("#type-product-dropdownlist-search").val();
        $.ajax({
            url: "/api/product/listtableproduct/" + select,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataTable(response);
        });
    }

    // Đổ dữ liệu vào table
    fillDataTable(response) {
        self = this;
        $('#grid-product tbody').empty();
        $.each(response, function (index, item) {
            var idDelete = 'delete-' + item.id;
            var idEdit = 'edit-' + item.id;
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.name + `</td>
            <td>` + item.price + `</td>
            <td>` + item.quantity + `</td>
            <td>` + item.quantitySold + `</td>
            <td>`+ item.score + `</td>
            <td>`+ item.name_types + `</td>
            <td><span style="color:dodgerblue" id="` + idEdit + `" onclick="editPro(this)"> Sửa </span></td>
            <td><span style="color:darkred" id="` + idDelete + `" onclick="deletePro(this)">Xóa</span></td>
            </tr> `);
            $('#grid-product tbody').append(trHTML);//chen duoi
        });
    }

    deleteProduct(idPro) {
        self = this;
        $.ajax({
            url: '/api/product/deleteproduct/' + idPro,
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response) {
                self.getListProduct();
            }
            else
                alert("Thất bại");
        });
    }

    // Lấy thông tin sản phẩm đã chọn
    getInfoOneProduct(idPro) {
        self = this;
        $.ajax({
            url: '/api/product/getinfooneproduct/' + idPro,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataTableOneProduct(response);
        });
    }

    // Đổ dữ liệu sản phẩm đã chọn vào modal sửa
    fillDataTableOneProduct(response) {
        self = this;
        $.each(response, function (index, item) {
            $('#txtName').val(item.name);
            $('#type-product-dropdownlist').val(item.idType);
            $('#txtAuthor').val(item.author);
            $('#txtPrice').val(item.price);
            $('#txtQuantity').val(item.quantity);
            $('#txtPublisher').val(item.publisher);
            $('#txtDescription').val(item.description);
            $('#txtID').val(item.id);
        });
    }

    // Lấy danh sách thể loại
    getListType() {
        self = this;
        $.ajax({
            url: "/api/product/listTypeProduct",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataListType(response);
        });
    }

    // Đổ dữ liệu vào dropdown list
    fillDataListType(response) {
        self = this;
        $('#type-product-dropdownlist').empty();
        $.each(response, function (index, item) {
            var strHTML_type = $(`<option value=` + item.id + `>` + item.name + `</option>`);
            $('#type-product-dropdownlist').append(strHTML_type);//chen duoi
        });
        self.getListProduct();
    }

    // Lấy danh sách thể loại cho search
    getListSearch() {
        self = this;
        $.ajax({
            url: "/api/product/listTypeProduct",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataListTypeSearch(response);
        });
    }

    // Đổ dữ liệu vào dropdown list
    fillDataListTypeSearch(response) {
        self = this;
        $('#type-product-dropdownlist-search').empty();
        var searchType = $(`<option value="0">--Tất cả--</option>`);
        $('#type-product-dropdownlist-search').append(searchType);
        $.each(response, function (index, item) {
            var strHTML_type = $(`<option value=` + item.id + `>` + item.name + `</option>`);
            $('#type-product-dropdownlist-search').append(strHTML_type);
        });
        $('#type-product-dropdownlist-search').val(0);
        self.getListProduct();
    }

    //Search
    searchProductByNameOrTypes() {
        self = this;
        var search = $("#txtSearch").val();
        var select = $("#type-product-dropdownlist-search").val();
        if (search != "") {
            $.ajax({
                url: "/api/product/searchproduct/" + search + "/" + select,
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json' //Định nghĩa type data trả về.
                },
                dataType: "json", //Kiểu dữ liệu truyền lên.
            }).done(function (response) {
                self.fillDataTable(response);
            });
        }
        else {
            self.getListProduct();
        }
    }

    checkUpdate () {
        this.inputValidate();
        if (this.resultAdd == false)
            return;
        this.isExistNameProduct();
    }

    // Kiểm tra đầu vào có hợp lệ hay không
    inputValidate() {
        var name = $('#txtName').val();
        var author = $('#txtAuthor').val();
        var price = $('#txtPrice').val();
        var quantity = $('#txtQuantity').val();
        var publisher = $('#txtPublisher').val();
        this.resultAdd = true;
        if (name != null) {
            if (name == "") {
                $('#error-input-name').html("Không được để trống tên sản phẩm!");
                this.resultAdd = false;
            }
            else
                $('#error-input-name').html("");
        }
        if (author != null) {
            if (author == "") {
                $("#error-input-author").html("Không được để trống tên tác giả!");
                this.resultAdd = false;
            }
            else
                $("#error-input-author").html("");
        }
        if (price != null) {
            if (price == "") {
                $("#error-input-price").html("Không được để trống giá sản phẩm!");
                this.resultAdd = false;
            }
            else if (price < 0) {
                $("#error-input-price").html("Giá sản phẩm luôn dương!");
                this.resultAdd = false;
            }
            else
                $("#error-input-price").html("");
        }
        if (quantity != null) {
            if (quantity == "") {
                $("#error-input-quantity").html("Không được để trống số lượng!");
                this.resultAdd = false;
            }
            else if (quantity < 0) {
                $("#error-input-quantity").html("Số lượng nhập luôn dương!");
                this.resultAdd = false;
            }
            else
                $("#error-input-quantity").html("");
        }
        if (publisher != null) {
            if (publisher == "") {
                $("#error-input-publisher").html("Không được để trống nhà xuất bản!");
                this.resultAdd = false;
            }
            else
                $("#error-input-publisher").html("");
        }
    }

    //Kiểm tra tên sản phẩm đã tồn tại
    isExistNameProduct() {
        self = this;
        var product = {
            Name: $("#txtName").val(),
            ID: $('#txtID').val()
        }
        $.ajax({
            url: "/api/Product/isexistnameproductwhenedit",
            method: "POST",
            data: JSON.stringify(product),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.editProduct();
            }
            else {
                $("#error-input-name").html("Tên sản phẩm đã tồn tại!");
            }
        });
    }

    // Sửa sản phẩm
    editProduct() {
        var product = {
            ID: $('#txtID').val(),
            Name: $("#txtName").val(),
            Author: $("#txtAuthor").val(),
            Price: $("#txtPrice").val(),
            Quantity: $("#txtQuantity").val(),
            Description: $("#txtDescription").val(),
            Publisher: $("#txtPublisher").val(),
            IDType: $("#type-product-dropdownlist").val()
        };

        $.ajax({
            url: "/api/product/editProduct",
            method: "PUT",
            data: JSON.stringify(product), // Truyền xuống thông qua body request.
            contentType: "application/json", //Kiểu dữ liệu trả về.
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response) {
                $('.div-front').hide();
                self.getListProduct();
            }
            else
                alert("Sửa thất bại");
        });
    }
    
}