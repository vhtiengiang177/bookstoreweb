﻿window.check = isCheck;
function getPermissionSession() {
    $.ajax({
        url: "/api/account/get-permission-session",
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json' //Định nghĩa type data trả về.
        },
        dataType: "json"
    }).done(function (permissionSession) {
        if (permissionSession === null || permissionSession.Permission !== "admin") {
            window.check = false;
        }
        else {
            window.check = true;
        }

    }).fail(function (response) {
        return null;
    });
}


function checkPermission() {
    if (!isCheck) {
        getPermissionSession();
    }   
}

checkPermission();
setTimeout(function () {
    isCheck = window.check;
    console.log("isCheck admin: " + isCheck);
}, delayTime);

