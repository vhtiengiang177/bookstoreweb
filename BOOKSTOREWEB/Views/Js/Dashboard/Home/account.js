﻿
$(document).ready(function () {
    var accountJS = new AccountJS();
});

var STAFF = {};

class AccountJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        //Quyen

        var self = this;
        $("#img-avatar-account").on('click', function () {
            $("#file-image-avatar").click();
        });

        $("#btn-select-image").on('click', function () {
            $("#file-image-avatar").click();
        });

        $("#file-image-avatar").on('change', function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-avatar-account').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        });

        $("#btnUpdate").on('click', function () {
            var dataURL = $('#img-avatar-account').attr('src');
            dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            console.log(dataURL);
            self.updateStaffSession();
        });

        self.getStaffSession();
    }

    getStaffSession() {
        var self = this;
        $.ajax({
            url: '/api/staff/get-staff-session',
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillStaffSession(response[0]);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    fillStaffSession(staff) {
        STAFF = staff;
        $('#img-avatar-account').attr('src', 'data:image/png;base64,' + staff.avatar);
        $("#txtFullName").val(staff.name);
        $("#selectGender").val(staff.gender);
        $("#dtpkBirthday").val(staff.birthDay.slice(0, 10));
        $("#txtEmail").val(staff.email);
        $("#txtPhone").val(staff.phone);
        $("#txtAddress").val(staff.address);
        $("#txtPermission").val(staff.permission);
    }

    updateStaffSession() {
        var self = this;
        var avatarBase64 = $('#img-avatar-account').attr('src').replace(/^data:image\/(png|jpg);base64,/, "");
        var accountAdmin = {
            staff: {
                id: STAFF.id,
                avatar: avatarBase64,
                name: $("#txtFullName").val(),
                gender: $("#selectGender").val(),
                birthDay: $("#dtpkBirthday").val(),
                email: $("#txtEmail").val(),
                phone: $("#txtPhone").val(),
                address: $("#txtAddress").val()
            },
            
            permission: {
              permission: $("#txtPermission").val()
            },
            avatarStr: avatarBase64
        }

        STAFF = accountAdmin.staff;

        $.ajax({
            url: '/api/staff/update-staff',
            method: "PUT",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            cache: false,
            data: JSON.stringify(accountAdmin),
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response)
                alert("Cập nhật thành công!");
            else
                alert("Cập nhật thất bại!");
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

}