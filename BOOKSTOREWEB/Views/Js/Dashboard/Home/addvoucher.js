﻿$(document).ready(function () {
        var voucher = new AddVoucherJS();
});

class AddVoucherJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        var self = this;
        
        $('#btnAddVoucherForm').click(this.checkAdd.bind(self));
        //$('#btnAddProduct1').on('click', function () {
        //    self.checkAdd();
        //});
    }

    checkAdd() {
        this.inputValidate();
        if (this.resultAdd == false)
            return;
        this.isExistNameVoucher();
    }

    // Kiểm tra đầu vào có hợp lệ hay không
    inputValidate() {
        var name = $('#txtNameVoucher').val();    
        var discount = $('#txtPriceVoucher').val();
       
        var detail = $('#txtDescriptionVoucher').val();
        var today = new Date();
        var startDt = document.getElementById("starttimeVoucher").value;
        var endDt = document.getElementById("endtimeVoucher").value;
        this.resultAdd = true;
        if (name != null) {
            if (name == "") {
                $('#error-input-namevoucher').html("Không được để trống mã khuyến mãi!");
                this.resultAdd = false;
            }
            else
                $('#error-input-namevoucher').html("");
        }
        
        if (discount != null) {
            if (discount == "") {
                $("#error-input-pricevoucher").html("Không được để trống giá trị khuyến mãi!");
                this.resultAdd = false;
            }
            else
                $("#error-input-pricevoucher").html("");
        }
        if (startDt != null) {
            if (startDt == "") {
                $("#error-input-start").html("Không được để trống ngày bắt đầu!");
                this.resultAdd = false;
            }

            else
                $("#error-input-start").html("");
        }
        if (endDt != null) {
            if (endDt == "") {
                $("#error-input-end").html("Không được để trống ngày kết thúc!");
                this.resultAdd = false;
            }
            
            else
                $("#error-input-end").html("");
        }
        if (detail != null) {
            if (detail == "") {
                $('#error-input-detail').html("Không được để trống mô tả khuyến mãi!");
                this.resultAdd = false;
            }
            else
                $('#error-input-detail').html("");
        }

        if ((new Date(endDt).getTime() < new Date(startDt).getTime())) {
            $("#error-input-end").html("Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu!");
            this.resultAdd = false;
        }

        if ((new Date(startDt).getTime() < new Date(today).getTime())) {
            $("#error-input-start").html("Ngày bắt đầu phải lớn hơn ngày hiện tại!");
            this.resultAdd = false;
        }
    }

    //Kiểm tra mã khuyến mãi đã tồn tại
    isExistNameVoucher() {
        self = this;
        var voucher = {
            Name: $("#txtNameVoucher").val()
        }
        $.ajax({
            url: "/api/voucher/isexistnamevoucher",
            method: "POST",
            data: JSON.stringify(voucher),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.addVoucher();
            }
            else {
                $("#error-input-namevoucher").html("Mã đã tồn tại!");
            }
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau!");
        });
    }

    // Thêm km
    addVoucher() {
        var voucher = {
            Name: $("#txtNameVoucher").val(),
            StartDate: $("#starttimeVoucher").val(),
            EndDate: $("#endtimeVoucher").val(),
            Detail: $("#txtDescriptionVoucher").val(),
            Discount: $("#txtPriceVoucher").val()   
                    
        };

        $.ajax({
            url: "/api/voucher/addVoucher",
            method: "POST",
            data: JSON.stringify(voucher), // Truyền xuống thông qua body request.
            contentType: "application/json", //Kiểu dữ liệu trả về.
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response) {
                alert("Thêm thành công");
                location.reload();
            }
               
            else
                alert(" Thất bại");
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau!");
        });
    }


   
}