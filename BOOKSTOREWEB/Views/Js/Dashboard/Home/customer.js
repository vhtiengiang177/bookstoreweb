﻿$(document).ready(function () {
    var customer = new CustomerJS();
    window.customer = customer;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    document.getElementById("datefield").setAttribute("max", today);
});

// Xuất file excel
function exportToExcel() {
    var table = document.getElementById("table-cus");
    var htmls = "";
    var uri = 'data:application/vnd.ms-excel;base64,';
    var template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
    var base64 = function (s) {
        return window.btoa(unescape(encodeURIComponent(s)))
    };

    var format = function (s, c) {
        return s.replace(/{(\w+)}/g, function (m, p) {
            return c[p];
        })
    };

    htmls = table.outerHTML;

    var ctx = {
        worksheet: 'Customer_Sheet',
        table: htmls
    }

    var date2 = new Date().toISOString().substr(0, 19).replace('T', ' ');
    var link = document.createElement("a");
    link.download = "ExportListCustomer" + date2 + ".xls";
    link.href = uri + base64(format(template, ctx));
    link.click();
}

function showDetailCus(self) {
    var id = self.id.replace('select-detail-', '');
    $('.div-front').show();
    $('#id-cus').val(id);
    window.customer.getDetailCus(this);
}

function showBillDetail(self) {
    var id = self.id.replace('select-detail-', '');
    $('.div-front').hide();
    $('.div-front-1').show();
    $('#id-bill').val(id);
    $('#title-billdetail').append(" #" + id);
    window.customer.getBillDetail(this);
}

class CustomerJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        var self = this;
        this.getListCustomer();
        $('#txtSearch').keyup(this.searchCustomer.bind(this));
        $('#datefield').change(this.getDetailCusByDate.bind(this));
        $('#icon-close').click(function () {
            $('#datefield').hide();
            $('#title-bill').text("DANH SÁCH ĐƠN HÀNG ĐÃ MUA");
            $('.div-front').hide();
        });
        $('#select-date').click(function () {
            $('#datefield').show();
            if ($('#datefield').val() != null) {
                window.customer.getDetailCusByDate();
            }
        })
        $('#all-bill').click(function () {
            $('#datefield').hide();
            window.customer.getDetailCus();
        });
        $('#icon-close-bill-detail').click(this.closeBillDetail.bind(self));
    }

    closeBillDetail() {
        $('.div-front').show();
        $('.div-front-1').hide();
        $('#title-billdetail').text("CHI TIẾT ĐƠN");
    }

    // Lấy danh sách sp
    getListCustomer() {
        var self = this;
        $.ajax({
            url: "/api/customer/getListCustomer",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataTable(response);
        });
    }

    // Đổ dữ liệu vào table
    fillDataTable(response) {
        self = this;
        $('#grid-cus tbody').empty();
        $.each(response, function (index, item) {
            var idDetail = 'select-detail-' + item.id;
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.name + `</td>
            <td>` + item.username + `</td>
            <td>` + item.email + `</td>
            <td>` + item.phone + `</td>
            <td>`+ item.address + `</td>
            <td style="text-align: center;">` + `<i class="fas fa-info-circle" id=` + idDetail + ` onclick="showDetailCus(this)"></i>` + `</td>
            </tr> `);
            $('#grid-cus tbody').append(trHTML);//chen duoi
        });
    }

    //Search
    searchCustomer() {
        self = this;
        var search = $("#txtSearch").val();
        if (search != "") {
            $.ajax({
                url: '/api/customer/searchcustomer/' + search,
                method: "GET",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json' //Định nghĩa type data trả về.
                },
                dataType: "json", //Kiểu dữ liệu truyền lên.
            }).done(function (response) {
                self.fillDataTable(response);
            });
        }
        else {
            self.getListCustomer();
        }
    }

    getDetailCusByDate() {
        self = this;
        var idCus = $('#id-cus').val();
        var date = $('#datefield').val().toString();
        $.ajax({
            url: '/api/customer/getbillcusbydate/' + idCus + '/' + date,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            $('#title-bill').text("DANH SÁCH ĐƠN HÀNG ĐÃ MUA THEO NGÀY " + date);
            self.fillDataTableBillOfCus(response);
        });
    }

    getDetailCus() {
        self = this;
        var idCus = $('#id-cus').val();
        $.ajax({
            url: '/api/customer/getbillcus/' + idCus,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            $('#title-bill').text("DANH SÁCH ĐƠN HÀNG ĐÃ MUA");
            self.fillDataTableBillOfCus(response);
        });
    }

    fillDataTableBillOfCus(response) {
        self = this;
        $('#gird-customer-detail tbody').empty();
        var i = 1;
        $.each(response, function (index, item) {
            var idDetail = item.id;
            var trHTML = $(`<tr>
            <td>` + i + `</td>
            <td>` + item.id + `</td>
            <td>` + item.dateConfirm.replace('T', ' ') + `</td>
            <td>` + item.dateReceive.replace('T', ' ') + `</td>
            <td>` + item.totalQuantity + `</td>
            <td>` + item.totalCost + `</td>
            <td style="text-align: center;">` + `<i class="fas fa-info-circle" id=` + idDetail + ` onclick="showBillDetail(this)"></i>` + `</td>
            </tr> `);
            i++;
            $('#gird-customer-detail tbody').append(trHTML);//chen duoi
        });
    }
    
    getBillDetail() {
        self = this;
        var id = $('#id-bill').val();
        $.ajax({
            url: '/api/customer/getbilldetail/' + id,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.getInfoBillByID(id);
            self.fillDataTableBillDetail(response);
        });
    }

    getInfoBillByID(id) {
        self = this;
        $.ajax({
            url: '/api/customer/getinfobillbyidbill/' + id,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataInfoBill(response);
        });
    }

    fillDataInfoBill(response) {
        $('#transport-fee').empty();
        $('#total-cost').empty();
        $.each(response, function (index, item) {
            var trHTML = $(`
               <p>` + item.feeShip + `</p>
             `);
            $('#transport-fee').append(trHTML);
            var trHTML = $(`
               <p>` + item.totalCost + `</p>
             `);
            $('#total-cost').append(trHTML);
        });
    }

    fillDataTableBillDetail(response) {
        self = this;
        $('#gird-bill-detail tbody').empty();
        var i = 1;
        $.each(response, function (index, item) {
            var state;
            if (item.statebill == 'unapproved') {
                state = "Chưa duyệt";
            }
            else if (item.statebill == 'approved') {
                state = "Đã duyệt";
            }
            else if (item.statebill == 'cancelled') {
                state = "Đã hủy";
            }
            else if (item.statebill == 'shipping') {
                state = "Đang giao";
            }
            else if (item.statebill == 'complete') {
                state = "Đã giao";
            }
            else {
                state = "Trả hàng";
            }
            var trHTML = $(`<tr>
            <td>` + i + `</td>
            <td>` + item.id + `</td>
            <td>` + item.nameproduct + `</td>
            <td>` + state + `</td>
            <td>` + item.quantity + `</td>
            <td>` + item.prices + `</td>
            <td>` + item.quantity * item.prices + `</td>
            </tr> `);
            i++;
            $('#gird-bill-detail tbody').append(trHTML);//chen duoi
        });
    }
}