﻿$(document).ready(function () {
    $(document).off('.datepicker.data-api');
    var reportJS = new ReportJS();
});

var labelsRevenue = [];
var dataRevenue = [];
var labelsProduct = [];
var dataProduct = [];
var labelsCustomer = [];
var dataCustomer = [];

function chartRevenue(labels_input, data_input) {
    var config = {
        type: 'line',
        data: {
            labels: labels_input,
            datasets: [{
                label: 'Doanh thu',
                backgroundColor: ['rgba(54, 162, 235, 0.2)'],
                borderColor: ['rgba(54, 162, 235, 1)'],
                fill: true,
                data: data_input,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Thống kê doanh thu'
            },
            scales: {
                xAxes: [{
                    display: true,
                }],
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById('myChartRevenue').getContext('2d');
    window.myLineRevenue = new Chart(ctx, config);
}

function chartProduct(labels_input, data_input) {
    var backgroundColor_input = ['rgba(54, 162, 235, 0.2)'];
    var borderColor_input = ['rgba(54, 162, 235, 1)'];
    var config = {
        type: 'bar',
        data: {
            labels: labels_input, //Mảng labels
            datasets: [{
                label: 'Số lượng sách bán: ',
                data: data_input, //Mảng data
                backgroundColor: backgroundColor_input,
                borderColor: borderColor_input,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById('myChartProduct').getContext('2d');
    window.myBarProduct = new Chart(ctx, config);
   
}

function chartCustomer(labels_input, data_input) {
    var backgroundColor_input = ['rgba(54, 162, 235, 0.2)'];
    var borderColor_input = ['rgba(54, 162, 235, 1)'];
    var config = {
        type: 'bar',
        data: {
            labels: labels_input, //Mảng labels
            datasets: [{
                label: 'Danh sách khách hàng: ',
                data: data_input, //Mảng data
                backgroundColor: backgroundColor_input,
                borderColor: borderColor_input,
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    };
    var ctx = document.getElementById('myChartCustomer').getContext('2d');
    window.myBarCustomer = new Chart(ctx, config);

}

class ReportJS {
    constructor() {
        this.initEvents();   
    }

    initEvents() {
        var self = this;
        this.setDate();
        window.typeDateReveune = $("#select-date").val();
        window.typeDateProduct = $("#select-date-product").val();
        window.typeDateCustomer = $("#select-date-customer").val();

        this.getRevenueReport($("#txtDateRevenue").val());
        $("#txtDateRevenue").on('change', function () {
            self.getRevenueReport($("#txtDateRevenue").val());
            window.myLineRevenue.update();
        });
        $("#select-date").on('change', function () {
            window.typeDateReveune = $("#select-date").val();
            self.getRevenueReport($("#txtDateRevenue").val());
            window.myLineRevenue.update();
        });

        this.getProductReport($("#txtDateProduct").val());
        $("#txtDateProduct").on('change', function () {
            self.getProductReport($("#txtDateProduct").val());
            window.myBarProduct.update();
        });

        $("#select-date-product").on('change', function(){
            window.typeDateProduct = $("#select-date-product").val();
            self.getProductReport($("#txtDateProduct").val());
            window.myBarProduct.update();
        });

        this.getCustomerReport($("#txtDateCustomer").val());
        $("#txtDateCustomer").on('change', function () {
            self.getCustomerReport($("#txtDateCustomer").val());
            window.myBarCustomer.update();
        });

        $("#select-date-customer").on('change', function () {
            window.typeDateCustomer = $("#select-date-customer").val();
            self.getCustomerReport($("#txtDateCustomer").val());
            window.myBarCustomer.update();
        });
    }

    getURLRevenue(typeDate, date) {
        if (typeDate == 'week')
            return '/api/bill/revenue-report/' + date;
        return '/api/bill/revenue-report-month/' + date;
    }

    getRevenueReport(date) {
        date = date.toString();
        var self = this;
        var URL = self.getURLRevenue($("#select-date").val(), date);
        $.ajax({
            url: URL,//'/api/bill/revenue-report/' + date,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillRevenueReport(response);
            chartRevenue(labelsRevenue, dataRevenue);
            self.getDetailRevenue(date);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    fillRevenueReport(response) {
        var self = this;
        labelsRevenue = [];
        dataRevenue = [];
        $.each(response, function (index, item) {
            if (window.typeDateReveune == 'week')
                labelsRevenue.push(self.getDayOfWeek(new Date(item.date).getDay()));
            else {
                var index = item.date.indexOf('T');
                var date = item.date[index - 2] + item.date[index - 1];
                labelsRevenue.push(date);
            }
            dataRevenue.push(item.revenue);
        });
    }

    getURLDetailRevenue(typeDate, date) {
        if (typeDate == 'week')
            return '/api/bill/revenue-detail/' + date;
        return '/api/bill/revenue-detail-month/' + date;
    }

    getDetailRevenue(date) {
        date = date.toString();
        var self = this;
        var URL = self.getURLDetailRevenue($("#select-date").val(), date);
        $.ajax({
            url: URL,//'/api/bill/revenue-detail/' + date,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.setDetailRevenue(response);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    setDetailRevenue(response) {
        $("#customer-number").text(response[0]);
        $("#order-number").text(response[1]);
        $("#complete-number").text(response[2]);
        $("#return-number").text(response[3]);
        $("#cancelled-number").text(response[4]);
        $("#total-revenue").text(this.commaSeparateNumber(parseInt(response[5])));
    }

    getURLProductReport(typeDate, date) {
        if (typeDate == 'week')
            return '/api/bill/products-report/' + date;
        return '/api/bill/products-report-month/' + date;
    }

    getProductReport(date) {
        var self = this;
        date = date.toString();
        var URL = self.getURLProductReport($("#select-date-product").val(), date);
        $.ajax({
            url: URL,//'/api/bill/products-report/' + date,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillProductReport(response);
            chartProduct(labelsProduct, dataProduct);
            self.fillProductReportDetail(response);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    fillProductReport(response) {
        var self = this;
        labelsProduct = [];
        dataProduct = [];
        $.each(response, function (index, item) {
            labelsProduct.push("Mã sách: " + item.idProduct);
            dataProduct.push(item.totalQuantity);
        });
    }

    fillProductReportDetail(response) {
        self = this;
        $('#gird-product-detail tbody').empty();
        $.each(response, function (index, item) {
            var trHTML = $(`<tr>
            <td>` + item.idProduct + `</td>
            <td>` + item.name + `</td>
            <td>` + item.category + `</td>
            <td>` + item.totalQuantity + `</td>
            </tr> `);
            $('#gird-product-detail tbody').append(trHTML);//chen duoi
            //$('.grid tbody').prepend(trHTML);// chen tren
        });
    }

    getURLCustomerReport(typeDate, date) {
        if (typeDate == 'week')
            return '/api/bill/customers-report/' + date;
        return '/api/bill/customers-report-month/' + date;
    }

    getCustomerReport(date) {
        var self = this;
        date = date.toString();
        var URL = self.getURLCustomerReport($("#select-date-customer").val(), date);
        $.ajax({
            url: URL,//'/api/bill/products-report/' + date,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillCustomerReport(response);
            chartCustomer(labelsCustomer, dataCustomer);
            self.fillCustomerReportDetail(response);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    fillCustomerReport(response) {
        var self = this;
        labelsCustomer = [];
        dataCustomer = [];
        $.each(response, function (index, item) {
            labelsCustomer.push("Mã KH: " + item.id);
            dataCustomer.push(item.spending);
        });
    }

    fillCustomerReportDetail(response) {
        self = this;
        $('#gird-customer-detail tbody').empty();
        $.each(response, function (index, item) {
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.name + `</td>
            <td>` + item.orderNumber + `</td>
            <td>` + item.spending + `</td>
            </tr> `);
            $('#gird-customer-detail tbody').append(trHTML);//chen duoi
            //$('.grid tbody').prepend(trHTML);// chen tren
        });
    }


    commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        }
        val += ' VNĐ';
        return val;
    }

    getDayOfWeek(index) {
        switch (index)
            {
            case 0: return "Chủ nhật";
            case 1: return "Thứ hai";
            case 2: return "Thứ ba";
            case 3: return "Thứ tư";
            case 4: return "Thứ năm";
            case 5: return "Thứ sáu";
            case 6: return "Thứ bảy";
        }
    }

    setDate() {
        var today = new Date();
        var month = (today.getMonth() + 1) < 10 ? '0' + (today.getMonth() + 1).toString() : (today.getMonth() + 1).toString();
        var date = (today.getDate()) < 10 ? '0' + today.getDate().toString() : today.getDate().toString();
        var strDate = today.getFullYear().toString() + '-' + month + '-' + date;
        $("#txtDateRevenue").val(strDate);
        $("#txtDateProduct").val(strDate);
        $("#txtDateCustomer").val(strDate);
    }


}
