﻿$(document).ready(function () {
    var voucherJS = new VoucherJS();
    
    window.voucherJS = voucherJS;


    

    $("#type-product-2-dropdownlist").change(function () {
        window.voucherJS.getListProductByType();
        window.voucherJS.getListProductByTypeByVoucher();
    });



    $("#type-voucher-dropdownlist").change(function () {
        window.voucherJS.getListProductByTypeByVoucher();
        
    });
    
});


function showDetailVoucher(self) {
    var id = self.id.replace('select-detail-', '');
    $('.div-front').show();
    window.voucherJS.getDetailVoucher(id);
}

function changeIDVoucher(self) {
    var id = self.id.replace('select-id-', '');
    var idvoucher = $("#type-voucher-dropdownlist").val();
    
    window.voucherJS.updateIDVoucher(id,idvoucher);
}

function deleteIDVoucher(self) {
    var id = self.id.replace('select-id-type-', '');
    window.voucherJS.deleteIDVoucher(id);
}

function deleteVoucher(self) {
    var id = self.id.replace('select-delete-', '');
    window.voucherJS.deleteVoucher(id);
}

function editVoucher(self) {
    var id = self.id.replace('select-edit-', '');
    $('.div-front-edit').show();
    window.voucherJS.getInfoVoucher(id);
   
   
}



//===================================================================//
class VoucherJS {
    constructor() {
        this.initEvents();
    }

    initEvents() {
        self = this;
        this.getDataVoucher();
        this.getListType();
        this.getListVoucher();
        this.getListProductByTypeFirst();
       
        $('#icon-close').click(function () {
            $('.div-front').hide();
        });


        $('#icon-close-edit').click(function () {
            $('.div-front-edit').hide();
        });

        $('#btnSearchVoucher').click(this.searchVoucherByNameOrDiscount.bind(this));
        $('#btnAddVoucherForm').click(this.goAddVoucher.bind(this));
        $('#btnEditVoucher').click(this.checkSave.bind(self));
    }


    goAddVoucher() {
        window.location.href = "/Views/Pages/Dashboard/Home/addvoucher.html";
    }


    checkSave() {
        this.inputValidate();
        if (this.resultAdd == false)
            return;
        else {
            this. updateVoucher();
            location.reload();
        }
       
       
    }

   
   updateVoucher() {
        self = this;
       var idvoucher = $("#txtid").val();
       var discount = $("#txtdiscount").val()
       var voucher = {
           ID:idvoucher,
           Name: $("#txtnamevoucher").val(),
           StartDate: $("#starttimevoucher").val(),
           EndDate: $("#endtimevoucher").val(),
           Detail: $("#txtdescriptionvoucher").val(),
           Discount: discount
       };
        $.ajax({
            url: "/api/voucher/updateVoucher" ,
            method: "PUT",
            data: JSON.stringify(voucher),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response) {
                alert("Sửa thành công");
                //location.reload();
            }

            else
                alert(" Thất bại");
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau!");
        });
    }
    // Kiểm tra đầu vào có hợp lệ hay không
    inputValidate() {
        var name = $('#txtnamevoucher').val();
        var discount = $('#txtdiscount').val();
        var detail = $('#txtdescriptionvoucher').val();
        var today = new Date();
        var startDt = document.getElementById("starttimevoucher").value;
        var endDt = document.getElementById("endtimevoucher").value;
        this.resultAdd = true;
        if (name != null) {
            if (name == "") {
                $('#error-input-namevoucher').html("Không được để trống mã khuyến mãi!");
                this.resultAdd = false;
            }
            else
                $('#error-input-namevoucher').html("");
        }

        if (discount != null) {
            if (discount == "") {
                $("#error-input-pricevoucher").html("Không được để trống giá trị khuyến mãi!");
                this.resultAdd = false;
            }
            else
                $("#error-input-pricevoucher").html("");
        }
        
       
        if (detail != null) {
            if (detail == "") {
                $('#error-input-detail').html("Không được để trống mô tả khuyến mãi!");
                this.resultAdd = false;
            }
            else
                $('#error-input-detail').html("");
        }


       
        if ((new Date(endDt).getTime() < new Date(startDt).getTime())) {
            $("#error-input-end").html("Ngày kết thúc phải lớn hơn hoặc bằng ngày bắt đầu!");
            this.resultAdd = false;
        }

        if ((new Date(endDt).getTime() < new Date(today).getTime())) {
            $("#error-input-end").html("Ngày kết thúc phải lớn hơn ngày hiện tại!");
            this.resultAdd = false;
        }

    }
    //Thêm mã khyến mãi cho sản phẩm
    updateIDVoucher(id, idvoucher) {
        self = this;
        $.ajax({
            url: '/api/voucher/updateIdVoucher/' + id + '/' + idvoucher,
            method: 'PUT',
            contentType: 'application/json',
            dataType: 'json'
        }).done(function (response) {
            if (response) {
                self.getListProductByTypeByVoucher();
                self.getListProductByType();
                
            }
            else {
                alert("Cập nhật không thành công!");
            }
           
        }).fail(function (response) {
            alert("Hiện tại ứng dụng đang được bảo trì, vui lòng thử lại sau!");
        });
    }


    deleteIDVoucher2(id) {
        self = this;
        $.ajax({
            url: '/api/voucher/deleteIdVoucher2/' + id,
            method: 'PUT',
            contentType: 'application/json',
            dataType: 'json'
        }).done(function (response) {
            if (response) {
                self.getListProductByType();
                self.getListProductByTypeByVoucher();
                

            }
            

        }).fail(function (response) {
            alert("Lỗi, vui lòng thử lại sau!");
        });
    }

    deleteIDVoucher(id) {
        self = this;
        $.ajax({
            url: '/api/voucher/deleteIdVoucher/' + id,
            method: 'PUT',
            contentType: 'application/json',
            dataType: 'json'
        }).done(function (response) {
            if (response) {
                self.getListProductByTypeByVoucher();
                self.getListProductByType();

            }
            else {
                alert("Cập nhật không thành công!");
            }

        }).fail(function (response) {
            alert("Hiện tại ứng dụng đang được bảo trì, vui lòng thử lại sau!");
        });
    }


    //xóa km
    deleteVoucher(id) {
        self = this;
        $.ajax({
            url: '/api/voucher/deleteVoucher/' + id,
            method: 'DELETE',
            contentType: 'application/json',
            dataType: 'json'
        }).done(function (response) {
            if (response) {
                self.getDataVoucher();
                alert("xóa  thành công!");

            }
            else {
                alert("xóa không thành công!");
            }

        }).fail(function (response) {
            alert("Hiện tại ứng dụng đang được bảo trì, vui lòng thử lại sau!");
        });
    }

    //Lấy thông tin chi tiết KM
    getInfoVoucher(id) {
        self = this;
        var str ="compareDate"+id
        var start = document.getElementById(str).innerHTML;
        var today = new Date();
        $.ajax({
            url: '/api/voucher/list-info-voucher/' + id,
            method: 'GET',
            contentType: 'application/json',
            dataType: 'json'
        }).done(function (data) {   
            if ((new Date(start).getTime() < new Date(today).getTime())) {
                $.each(data, function (index, item) {
                    $('#txtid').replaceWith(` <input type="text" value=` + item.id + ` id="txtid" style="display:none" >`);
                    $('#txtnamevoucher').replaceWith(` <input type="text" value="` + item.name + `" id="txtnamevoucher"  readonly placeholder="Mã áp dụng"  required />`);
                    $('#txtdiscount').replaceWith(` <input type="text" value=` + item.discount + `  id="txtdiscount" readonly placeholder="Nhập giá trị giảm"   max="1" min="0.1" step="0.01" required />`);
                    $('#starttimevoucher').replaceWith(` <input type="text" value=` + item.startDate.split("T")[0] + ` id="starttimevoucher" readonly  name="starttime">`);
                    $('#endtimevoucher').replaceWith(` <input type="date" value=` + item.endDate.split("T")[0] + ` id="endtimevoucher"  name="endtime">`);
                    $('#txtdescriptionvoucher').replaceWith(`<input placeholder="Nhập mô tả"  value="` + item.detail + `"  id="txtdescriptionvoucher" />`);
                });
            }
            else {
                $.each(data, function (index, item) {
                    $('#txtid').replaceWith(` <input type="text" value=` + item.id + ` id="txtid" style="display:none" >`);
                    $('#txtnamevoucher').replaceWith(` <input type="text" value="` + item.name + `" id="txtnamevoucher"  readonly placeholder="Mã áp dụng"  required />`);
                    $('#txtdiscount').replaceWith(` <input type="number" value=` + item.discount + `  id="txtdiscount"  placeholder="Nhập giá trị giảm"   max="1" min="0.1" step="0.01" required />`);
                    $('#starttimevoucher').replaceWith(` <input type="date" value=` + item.startDate.split("T")[0] + ` id="starttimevoucher"   name="starttime">`);
                    $('#endtimevoucher').replaceWith(` <input type="date" value=` + item.endDate.split("T")[0] + ` id="endtimevoucher"  name="endtime">`);
                    $('#txtdescriptionvoucher').replaceWith(`<input placeholder="Nhập mô tả"  value="` + item.detail + `"  id="txtdescriptionvoucher" />`);
                });
            }
             

        }).fail(function (response) {
            alert("Hiện tại ứng dụng đang được bảo trì, vui lòng thử lại sau!");
        });
    }

    

    //Lấy danh sách sản phẩm có khuyến mãi theo loại sách
    getListProductByTypeByVoucher() {
        self = this;
        var idtype = $("#type-product-2-dropdownlist").val();
        var idvoucher = $("#type-voucher-dropdownlist").val();
        $.ajax({
            url: "/api/voucher/get-list-product-by-type-by-voucher/" + idtype + "/" + idvoucher,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (data) {
            $('#gird-voucher-type tbody').empty();
            $.each(data, function (index, item) {
                var idProduct = 'select-id-type-' + item.id;
                var trHTML = $(`<tr>        
                    <td>` + item.name + `</td>         
                    <td> <a id=`+ idProduct + ` onclick='deleteIDVoucher(this)'  style="font-weight: normal; color:red;	" > Xóa</a ></td >        
                    </tr> `);
                $('#gird-voucher-type tbody').append(trHTML);//chen duoi

            });
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }

    //Lấy danh sách sản phẩm không khuyến mãi theo loại sách
    getListProductByType() {
        self = this;
        var idtype = $("#type-product-2-dropdownlist").val();
        $.ajax({
            url: "/api/voucher/get-list-product-by-type/" + idtype,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (data) {
            $('#gird-product tbody').empty();
            $.each(data, function (index, item) {
               var idProduct = 'select-id-' + item.id;
                var trHTML = $(`<tr>        
            <td>` + item.name + `</td>         
            <td> <a id=`+ idProduct +` onclick='changeIDVoucher(this)'>Thêm</a></td>        
            </tr> `);
                $('#gird-product tbody').append(trHTML);//chen duoi

            });
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }
    //lấy danh sách voucher
    getDataVoucher() {
        var self = this;

        $.ajax({
            url: '/api/voucher/list-voucher',
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataVoucher(response);

        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    // hiểm thị toàn bộ km
    fillDataVoucher(response) {
        console.log(response);
        self = this;
        $('#gird-voucher tbody').empty();
        $.each(response, function (index, item) {
            var idDetail = 'select-detail-' + item.id;
            var idDelete = 'select-delete-' + item.id;
            var idEdit = 'select-edit-' + item.id;
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.name + `</td>
            <td id="compareDate`+ item.id + `" value=''>` + item.startDate.split("T")[0] + `</td>
            <td>` + item.endDate.split("T")[0] + `</td>
            <td>` + item.discount + `</td>
            <td><a id=`+ idEdit + ` onclick="editVoucher(this)" style="font-weight: normal; color:blue;	">  Sửa |</a>  <a id=` + idDelete +` onclick="deleteVoucher(this)" style="font-weight: normal; color:red;	">Xóa</a></td>
            <td style="text-align: center;">` + `<i class="fas fa-info-circle" id=` + idDetail + ` onclick="showDetailVoucher(this)"></i>` + `</td>
            </tr> `);
            $('#gird-voucher tbody').append(trHTML);
            
        });
    }

  
    //lấy mô tả chi tiết của voucher
    getDetailVoucher(id) {
        self = this;
        $.ajax({
            url: '/api/voucher/list-detail-voucher/' + id,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDetailVoucher(response);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    fillDetailVoucher(response) {
        self = this;
        $('#gird-detail-voucher tbody').empty();
        $.each(response, function (index, item) {
            var trHTML = $(`<tr>           
            <td>` + item.name + `</td>
            <td>` + item.detail + `</td>          
            </tr> `);
            $('#gird-detail-voucher tbody').prepend(trHTML);
        });
    }

    //Search
    searchVoucherByNameOrDiscount() {
        self = this;
        var search = $("#txtSearchVoucher").val();
        $.ajax({
            url: '/api/voucher/searchvoucher/' + search,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataVoucher(response);
        }).fail(function (response) {
            alert("Hệ thống đang trong thời gian bảo trì, vui lòng thử lại sau!");
        });
    }

    // Lấy danh sách thể loại sp
    getListType() {
        self = this;
        $.ajax({
            url: "/api/product/listTypeProduct",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataListType(response);
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }

    // Đổ dữ liệu vào dropdown list
    fillDataListType(response) {
        self = this;
        window.countTotal = 0;
        $('#type-product-2-dropdownlist').empty();
       
       
        $.each(response, function (index, item) {
            window.countTotal++;
            var strHTML_type = $(`<option value=` + item.id + `>` + item.name + `</option>`);
            $('#type-product-2-dropdownlist').append(strHTML_type);
            
        });
        
    }




    // Lấy danh sách khuyến mãi
    getListVoucher() {
        self = this;
        $.ajax({
            url: "/api/voucher/list-voucher",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataListVoucher(response);
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }

    // Đổ dữ liệu vào dropdown list
    fillDataListVoucher(response) {
        self = this;
        window.countTotal = 0;
        var today = new Date();
        
        var check = false;
        $('#type-voucher-dropdownlist').empty();
        
        $.each(response, function (index, item) {
            window.countTotal++;
            if ((new Date(item.endDate).getTime() > new Date(today).getTime())) {
               
                if (check == false) {
                    self.getListProductByTypeByVoucherFirst(item.id);
                    check = true;
                }
                    
               
                var strHTML_type = $(`<option value=` + item.id + `>` + item.name + `</option>`);
            }
            else {
                self.deleteIDVoucher2(item.id);
            }
            
            $('#type-voucher-dropdownlist').append(strHTML_type);//chen duoi
            //$('.grid tbody').prepend(trHTML);// chen tren
        });
        //self.changePercentApproved();
    }


    
    

    fillDataListProductByType(response) {
        self = this;
        $('#gird-product tbody').empty();
        $.each(response, function (index, item) {          
            var trHTML = $(`<tr>        
            <td>` + item.name + `</td>         
            <td> <a>Thêm</a></td>        
            </tr> `);
            $('#gird-product tbody').append(trHTML);//chen duoi
            //$('.grid tbody').prepend(trHTML);// chen tren
        });
    }



    //load bảng đầu
    getListProductByTypeFirst() {
        self = this;
        var idtype = 1;
        $.ajax({
            url: "/api/voucher/get-list-product-by-type/" + idtype,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (data) {
            $('#gird-product tbody').empty();
            $.each(data, function (index, item) {
                var idProduct = 'select-id-' + item.id;
                var trHTML = $(`<tr>        
            <td>` + item.name + `</td>         
            <td> <a id=`+ idProduct + ` onclick="changeIDVoucher(this)">Thêm</a></td>        
            </tr> `);
                $('#gird-product tbody').append(trHTML);//chen duoi

            });
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }

    getListProductByTypeByVoucherFirst(idvoucher) {
        self = this;
        var idtype = 1;
        
        $.ajax({
            url: "/api/voucher/get-list-product-by-type-by-voucher/" + idtype + "/" + idvoucher,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (data) {
            $('#gird-voucher-type tbody').empty();
            $.each(data, function (index, item) {
                var idProduct = 'select-id-type-' + item.id;
                var trHTML = $(`<tr>        
                    <td>` + item.name + `</td>         
                    <td> <a id=`+ idProduct + ` onclick="deleteIDVoucher(this)"  style="font-weight: normal; color:red;	"> Xóa</a ></td >        
                    </tr> `);
                $('#gird-voucher-type tbody').append(trHTML);//chen duoi

            });
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }
}

