﻿$(document).ready(function () {
    var accountManagermentJS = new AccountManagermentJS();
});

function showAvatar(src) {
    $("#front-div").addClass('flex');
    $("#img-avatar-managerment").attr('src', src);
}

class AccountManagermentJS {
    constructor(){
        this.initEvents();
    }

    initEvents() {
        var self = this;
        this.getAccountManagermentOfStaff();
        $("#icon-close").on('click', function () {
            $("#front-div").removeClass('flex');
        });
        $("#icon-close-add-account").on('click', function () {
            $("#front-div-add-account").removeClass('flex');
        });
        $("#btnSearchStaff").on('click', function () {
            var keyword = $("#txtSearchStaff").val();
            if (keyword === "")
                self.getAccountManagermentOfStaff();
            else
                self.getAccountManagermentOfStaffByKeyword(keyword);
        });

        $("#btnAddAccount").on('click', function () {
            $("#front-div-add-account").addClass('flex');
        });

        $("#img-avatar-account").on('click', function () {
            $("#file-image-avatar").click();
        });

        $("#btn-select-image").on('click', function () {
            $("#file-image-avatar").click();
        });

        $("#file-image-avatar").on('change', function () {
            var input = this;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img-avatar-account').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        });

        $("#btnSubmitAddAccount").on('click', function () {
            self.checkAddStaff();
        });
    }

    getAccountManagermentOfStaff() {
        var self = this;
        var URL = "/api/account/getaccountmanagermentofstaff";
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillAccountManagerment(response);
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }

    getAccountManagermentOfStaffByKeyword(keyword) {
        var self = this;
        var URL = "/api/account/get-account-managerment-of-staff/" + keyword;
        $.ajax({
            url: URL,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillAccountManagerment(response);
        }).fail(function (response) {
            alert("Hệ thống đang bảo trì, vui lòng thử lại sau!");
        });
    }

    fillAccountManagerment(response) {
        self = this;
        $('#gird-account-managerment tbody').empty();
        $.each(response, function (index, item) {
            var src = "data:image/png;base64," + item.avatar;
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.userName + `</td>
            <td>` + item.name + `</td>
            <td class="center"><img class="avatar-in-table" src="` + src + `" onclick="showAvatar('` + src + `')"></td>
            <td>` + item.birthDay.replace('T', ' ').slice(0, 10) + `</td>
            <td>` + item.gender + `</td>
            <td>` + item.address + `</td>
            <td>` + item.phone + `</td>
            <td>` + item.email + `</td>
            <td>` + item.permission + `</td>
            </tr> `);
            $('#gird-account-managerment tbody').append(trHTML);//chen duoi
            //$('.grid tbody').prepend(trHTML);// chen tren
        });
    }



    //Add account
    getPermission() {
        var permission = $("#txtPermission").val();
        if (permission == "admin")
            return 1;
        if (permission == "staff")
            return 2;
        return 3;
    }

    addStaff() {
        var self = this;
        var dataURL = $('#img-avatar-account').attr('src');
        dataURL = dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        var user = {
            staff: {
                name: $("#txtName").val(),
                birthday: $("#txtBirthDay").val(),
                phone: $("#txtPhone").val(),
                gender: $("#genderMale").val() == 'on' ? "Male" : "Female",
                email: $("#txtEmail").val(),
                address: $("#txtAddress").val(),
            },
            account: {
                username: $("#txtUserName").val(),
                password: $("#txtPassWord").val(),
                idPermission: this.getPermission()
            },
            avatarStr: dataURL
        };
        $.ajax({
            url: "/api/staff/add-staff",
            method: "POST",
            data: JSON.stringify(user),
            contentType: "application/json",
            dataType: "json",
            traditional: true
        }).done(function (res) {
            if (res == true) {
                $("#front-div-add-account").removeClass('flex');
                self.getAccountManagermentOfStaff();
            }
            else {
                alert("Thêm tài khoản không thành công!")
            }
        }).fail(function (response) {
            alert("Thành công!");
        });

    }

    checkAddStaff() {
        var result = true;
        if (this.checkSignUp() == false)
            return;
        this.isExistEmail();
        //this.isExistUserName();
    }

    isExistEmail() {
        self = this;
        var customer = {
            email: $("#txtEmail").val()
        }
        $.ajax({
            url: "/api/customer/isexistemail",
            method: "POST",
            data: JSON.stringify(customer),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.isExistUserName();
            }
            else {
                $("#error-input-email").html("Email đã tồn tại!");
            }
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
        });
    }


    isExistUserName() {
        self = this;
        var username = $("#txtUserName").val();
        $.ajax({
            url: "/api/account/isexistusername/" + username,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: ""
        }).done(function (response) {
            if (response == false) {
                $("#error-input-username").html("");
                self.addStaff();
            }
            else {
                $("#error-input-username").html("Tên đăng nhập đã tồn tại!");
            }
        }).fail(function (response) {
            alert("Hiện tại hệ thống đang gặp sự cố, vui lòng thử lại sau! Cảm ơn quý khách đã tin dùng sản phẩm của chúng tôi!");
        });
    }

    checkSignUp() {
        var result = true;
        var name = $("#txtName").val();
        var birthday = $("#txtBirthDay").val();
        var phone = $("#txtPhone").val();
        var gender = $("#genderMale").val() == 'on' ? "Male" : "Female";
        var email = $("#txtEmail").val();
        var address = $("#txtAddress").val();
        var username = $("#txtUserName").val();
        var password = $("#txtPassWord").val();
        var rePassword = $("#txtRePassWord").val();
        if (name == "") {
            $("#error-input-name").html("Vui lòng nhập tên!");
            result = false;
        }
        else {
            $("#error-input-name").html("");
        }

        var today = new Date();
        var fullDate = today.getFullYear() + "-" + today.getMonth() + "-" + today.getDate();
        if (birthday >= fullDate) {
            $("#error-input-birthday").html("Ngày sinh không hợp lệ!");
            result = false;
        }
        else {
            $("#error-input-birthday").html("");
        }

        if (phone == "") {
            $("#error-input-phone").html("Vui lòng nhập số điện thoại!");
            result = false;
        }
        else {
            $("#error-input-phone").html("");
        }

        if (email == "") {
            $("#error-input-email").html("Vui lòng nhập địa chỉ email!");
            result = false;
        }
        else {
            if (this.isEmail(email)) {
                $("#error-input-email").html("");
            }
            else {
                $("#error-input-email").html("Email không hợp lệ!");
            }

        }

        if (username == "") {
            $("#error-input-username").html("Vui lòng nhập tài khoản!");
            result = false;
        }
        else {
            $("#error-input-username").html("");
        }

        if (password == "") {
            $("#error-input-password").html("Vui lòng nhập mật khẩu!");
            result = false;
        }
        else {
            $("#error-input-password").html("");
        }

        if (rePassword == "") {
            $("#error-input-repassword").html("Vui lòng nhập lại mật khẩu!");
            result = false;
        }
        else {
            $("#error-input-repassword").html("");
        }

        if (password == rePassword) {
            $("#error-input-repassword").html("");
        }
        else {
            $("#error-input-repassword").html("Mật khẩu không trùng khớp!");
            result = false;
        }

        return result;
    }

    isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    } 
};