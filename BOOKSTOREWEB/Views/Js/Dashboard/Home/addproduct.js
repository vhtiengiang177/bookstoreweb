﻿var imageSrcs = [];

$(document).ready(function ()
{
    $('#fileInput').on('change', function () {
        var files = $(this)[0].files;
        uploadFile(files, 0);
        setTimeout(function () {
            console.log(imageSrcs);
        }, 100);
    });
    function uploadFile(files, index) {
        var length = files.length
        if (index == length) {
            return;
        }

        var file = files[index];
        var fileReader = new FileReader();
        fileReader.onload = function () {
            var str = '<div>' + '<img class="img-addproduct js-file-image style="">' + '<div>';
            $('.js-file-list').append(str);

            var imageSrc = event.target.result;
            var fileName = file.name;

            $('.js-file-name').last().text(fileName);
            $('.js-file-image').last().attr('src', imageSrc);
            imageSrcs.push(imageSrc);
            uploadFile(files, index + 1);
        };
        fileReader.readAsDataURL(file);
    }

    var addproduct = new AddProductJS();
    window.addproduct = addproduct;
});

function editType(self) {
    var idType = self.id.replace('edit-', '');
    $('#divAddType').hide();
    $('#divEditType').show();
    window.addproduct.getInfoOneType(idType);
}

function deleteType(self) {
    var idType = self.id.replace('delete-', '');
    window.addproduct.deleteTypes(idType);
}

function editCate(self) {
    var idCategory = self.id.replace('edit-', '');
    $('#divAddCategory').hide();
    $('#divEditCategory').show();
    window.addproduct.getInfoOneCategory(idCategory);
}

function deleteCate(self) {
    var idCate= self.id.replace('delete-', '');
    window.addproduct.deleteCategory(idCate);
}

class AddProductJS
{
    constructor() {
        this.initEvents();
    }

    initEvents() {
        var self = this;
        this.getListType();
        $('#btnAddProduct1').click(this.checkAdd.bind(self));
        $('#btnOpenAddType').click(this.openAddType.bind(self));
        $('#icon-close-addtype').click(this.closeAddType.bind(self));
        $('#btnAddType1').click(this.checkAddType.bind(self));
        $('#btnCancel').click(function () {
            $('#divAddType').show();
            $('#divEditType').hide();
            $('#txtTypeName').val("");
            $('#category-product-dropdownlist').val(1);
        });
        $('#btnEditType1').click(this.checkEditType.bind(self));
        $('#btnAddCategory').click(function () {
            $('.div-front').hide();
        });
        $('#btnOpenAddCategory').click(this.openAddCategory.bind(self));
        $('#icon-close-category').click(this.closeAddCategory.bind(self));
        $('#btnAddCategory').click(this.checkAddCategory.bind(self));
        $('#btnEditCategory1').click(this.checkEditCategory.bind(self));
        $('#btnCancelCate').click(function () {
            $('#divAddCategory').show();
            $('#divEditCategory').hide();
            $('#txtCategoryName').val("");
        })
    }

    // Mở modal thêm thể loại
    openAddType() {
        this.getListCategory();
        $('.div-front').show();
        $('#category-product-dropdownlist').val(1);
        $('#txtTypeName').val("");
        $('#error-input-typename').html("");
    }

    // Đóng modal thêm thể loại types
    closeAddType() {
        $('.div-front').hide();
        $('#category-product-dropdownlist').val(1);
        $('#txtTypeName').val("");
        $('#error-input-typename').html("");
        this.getListType();
    }

    // Mở modal thêm loại
    openAddCategory() {
        this.getListCategory();
        $('.div-front-1').show();
        this.closeAddType();
    }

    // Đóng modal thêm loại category
    closeAddCategory() {
        $('.div-front-1').hide();
        this.getListCategory();
        $('#txtCategoryName').val("");
        $('#error-input-categoryname').html("");
        this.openAddType();
    }

    // Kiểm tra việc thêm sản phẩm
    checkAdd(){
        this.inputValidate();
        if (this.resultAdd == false)
            return;
        this.isExistNameProduct();
    }

    // Kiểm tra đầu vào có hợp lệ hay không
    inputValidate()
    {
        var name = $('#txtName').val();
        var author = $('#txtAuthor').val();
        var price = $('#txtPrice').val();
        var quantity = $('#txtQuantity').val();
        var publisher = $('#txtPublisher').val();
        this.resultAdd = true;
        if (name != null) {
            if (name == "") {
                $('#error-input-name').html("Không được để trống tên sản phẩm!");
                this.resultAdd = false;
            }
            else
                $('#error-input-name').html("");
        }
        if (author != null) {
            if (author == "") {
                $("#error-input-author").html("Không được để trống tên tác giả!");
                this.resultAdd = false;
            }
            else
                $("#error-input-author").html("");
        }
        if (price != null) {
            if (price == "") {
                $("#error-input-price").html("Không được để trống giá sản phẩm!");
                this.resultAdd = false;
            }
            else if (price < 1000) {
                $("#error-input-price").html("Giá sản phẩm phải trên 1000 VND!");
                this.resultAdd = false;
            }
            else
                $("#error-input-price").html("");
        }
        if (quantity != null) {
            if (quantity == "") {
                $("#error-input-quantity").html("Không được để trống số lượng!");
                this.resultAdd = false;
            }
            else if (quantity < 0) {
                $("#error-input-quantity").html("Số lượng nhập luôn dương!");
                this.resultAdd = false;
            }
            else
                $("#error-input-quantity").html("");
        }
        if (publisher != null) {
            if (publisher == "") {
                $("#error-input-publisher").html("Không được để trống nhà xuất bản!");
                this.resultAdd = false;
            }
            else
                $("#error-input-publisher").html("");
        }
    }

    //Kiểm tra tên sản phẩm đã tồn tại
    isExistNameProduct() {
        self = this;
        var product = {
            Name: $("#txtName").val()
        }
        $.ajax({
            url: "/api/Product/isexistnameproduct",
            method: "POST",
            data: JSON.stringify(product),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.addProduct();
            }
            else {
                $("#error-input-name").html("Tên sản phẩm đã tồn tại!");
            }
        });
    }

    convertSrcToBase64(src) {
        return src.replace(/^data:image\/(png|jpg);base64,/, "");
    }

    // Thêm sản phẩm
    addProduct() {
        var productFull = {
            product: {
                Name: $("#txtName").val(),
                Author: $("#txtAuthor").val(),
                Price: $("#txtPrice").val(),
                Quantity: $("#txtQuantity").val(),
                Description: $("#txtDescription").val(),
                Publisher: $("#txtPublisher").val(),
                IDType: $("#type-product-dropdownlist").val()
            },
            imageSrcs: imageSrcs.map(this.convertSrcToBase64)
        }

        $.ajax({
            url: "/api/product/addProduct",
            method: "POST",
            data: JSON.stringify(productFull), // Truyền xuống thông qua body request.
            contentType: "application/json", //Kiểu dữ liệu trả về.
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response) {
                alert("Thêm thành công");
                window.location.reload(true);
            }
            else
                alert("Thất bại");
        });
    }

    // Lấy danh sách thể loại sp
    getListType() {
        self = this;
        $.ajax({
            url: "/api/product/listTypeProduct",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataListType(response);
            self.fillDataTableType(response);
        });
    }

    // Đổ dữ liệu Type vào dropdown list
    fillDataListType(response) {
        self = this;
        $('#type-product-dropdownlist').empty();
        $.each(response, function (index, item) {
            var strHTML_type = $(`<option value=` + item.id + `>` + item.name + `</option>`);
            $('#type-product-dropdownlist').append(strHTML_type);//chen duoi
            //$('.grid tbody').prepend(trHTML);// chen tren
        });
    }

    // Đổ dữ liệu vào datatable
    fillDataTableType(response) {
        self = this;
        $('#grid-types tbody').empty();
        $.each(response, function (index, item) {
            var idDelete = 'delete-' + item.id;
            var idEdit = 'edit-' + item.id;
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.name + `</td>
            <td>` + item.name_cate + `</td>
            <td><span style="color:dodgerblue" id="` + idEdit + `" onclick="editType(this)"> Sửa </span></td>
            <td><span style="color:darkred" id="` + idDelete + `" onclick="deleteType(this)">Xóa</span></td>
            </tr> `);
            $('#grid-types tbody').append(trHTML);//chen duoi
        });
    }

    // Lấy danh sách loại sp (Category)
    getListCategory() {
        self = this;
        $.ajax({
            url: "/api/product/listCategory",
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json" //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            self.fillDataListCategory(response);
            self.fillDataTableCategory(response);
        });
    }

    // Đổ dữ liệu Category vào dropdown list
    fillDataListCategory(response) {
        self = this;
        $('#category-product-dropdownlist').empty();
        $.each(response, function (index, item) {
            var strHTML_cate = $(`<option value=` + item.id + `>` + item.name + `</option>`);
            $('#category-product-dropdownlist').append(strHTML_cate);//chen duoi
            //$('.grid tbody').prepend(trHTML);// chen tren
        });
    }

    fillDataTableCategory(response) {
        self = this;
        $('#grid-category tbody').empty();
        $.each(response, function (index, item) {
            var idDelete = 'delete-' + item.id;
            var idEdit = 'edit-' + item.id;
            var trHTML = $(`<tr>
            <td>` + item.id + `</td>
            <td>` + item.name + `</td>
            <td><span style="color:dodgerblue" id="` + idEdit + `" onclick="editCate(this)"> Sửa </span></td>
            <td><span style="color:darkred" id="` + idDelete + `" onclick="deleteCate(this)">Xóa</span></td>
            </tr> `);
            $('#grid-category tbody').append(trHTML);//chen duoi
        });
    }


    checkAddType() {
        this.inputValidateTypes();
        if (this.resultAddType == false)
            return;
        this.isExistNameTypes();
    }

    // Kiểm tra thể loại
    inputValidateTypes() {
        var name = $('#txtTypeName').val();
        this.resultAddType = true;
        if (name != null) {
            if (name == "") {
                $('#error-input-typename').html("Không được để trống tên thể loại!");
                this.resultAddType = false;
            }
            else
                $('#error-input-typename').html("");
        }
    }

    //Kiểm tra tên thể loại đã tồn tại
    isExistNameTypes() {
        self = this;
        var types = {
            Name: $("#txtTypeName").val()
        }
        $.ajax({
            url: "/api/product/isexistnametype",
            method: "POST",
            data: JSON.stringify(types),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.addTypes();
            }
            else {
                $("#error-input-typename").html("Tên thể loại đã tồn tại!");
            }
        });
    }

    addTypes() {
        var types = {
            IDCategory: $('#category-product-dropdownlist').val(),
            Name: $('#txtTypeName').val()
        }
        $.ajax({
            url: "/api/product/addType",
            method: "POST",
            data: JSON.stringify(types),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == true) {
                $('#txtTypeName').val("");
                $('#category-product-dropdownlist').val(1);
                self.getListType();
            }
            else {
                alert("Thêm thể loại thất bại");
            }
        });
    }

    getInfoOneType(idType) {
        self = this;
        $.ajax({
            url: '/api/product/getinfoonetype/' + idType,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            $.each(response, function (index, item) {
                $('#txtTypeName').val(item.name);
                $('#category-product-dropdownlist').val(item.idCategory);
                $('#txtIDTypeEdit').val(item.id);
            });
        });
    }

    checkEditType() {
        this.inputValidateTypes();
        if (this.resultAdd == false)
            return;
        this.isExistNameTypesWhenEdit();
    }

    isExistNameTypesWhenEdit() {
        self = this;
        var types = {
            Name: $("#txtTypeName").val(),
            ID: $('#txtIDTypeEdit').val()
        }
        $.ajax({
            url: "/api/product/isexistnametypewhenedit",
            method: "POST",
            data: JSON.stringify(types),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.editTypes();
            }
            else {
                $("#error-input-typename").html("Tên thể loại đã tồn tại!");
            }
        });
    }

    editTypes() {
        var types = {
            IDCategory: $('#category-product-dropdownlist').val(),
            Name: $('#txtTypeName').val(),
            ID: $('#txtIDTypeEdit').val()
        }
        $.ajax({
            url: "/api/product/editType",
            method: "POST",
            data: JSON.stringify(types),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == true) {
                self.getListType();
                $('#divAddType').show();
                $('#divEditType').hide();
                $('#txtTypeName').val("");
                $('#category-product-dropdownlist').val(1);
            }
            else {
                $('#error-input-edittypes').html("Sửa thể loại thất bại");
            }
        });
    }

    deleteTypes(idType) {
        self = this;
        $.ajax({
            url: '/api/product/deletetypes/' + idType,
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response) {
                self.getListType();
            }
            else
                alert("Không thể xóa vì tồn tại sản phẩm thuộc thể loại này");
        });
    }

    checkAddCategory() {
        this.inputValidateCategory();
        if (this.resultAddCategory == false)
            return;
        this.isExistNameCategory();
    }

    // Kiểm tra loại
    inputValidateCategory() {
        var name = $('#txtCategoryName').val();
        this.resultAddCategory = true;
        if (name != null) {
            if (name == "") {
                $('#error-input-categoryname').html("Không được để trống tên loại!");
                this.resultAddCategory = false;
            }
            else
                $('#error-input-categoryname').html("");
        }
    }

    //Kiểm tra tên loại đã tồn tại
    isExistNameCategory() {
        self = this;
        var types = {
            Name: $("#txtCategoryName").val()
        }
        $.ajax({
            url: "/api/product/isexistnamecategory",
            method: "POST",
            data: JSON.stringify(types),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.addCategory();
            }
            else {
                $("#error-input-categoryname").html("Tên loại đã tồn tại!");
            }
        });
    }

    // Thêm loại
    addCategory() {
        var types = {
            Name: $('#txtCategoryName').val()
        }
        $.ajax({
            url: "/api/product/addCategory",
            method: "POST",
            data: JSON.stringify(types),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == true) {
                $('#txtCategoryName').val("");
                self.getListCategory();
            }
            else {
                alert("Thêm loại thất bại");
            }
        });
    }

    getInfoOneCategory(idCategory) {
        self = this;
        $.ajax({
            url: '/api/product/getinfoonecategory/' + idCategory,
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            $.each(response, function (index, item) {
                $('#txtCategoryName').val(item.name);
                $('#txtIDCategoryEdit').val(item.id);
            });
        });
    }

    checkEditCategory() {
        this.inputValidateCategory();
        if (this.resultAddCategory == false)
            return;
        this.isExistNameCategoryWhenEdit();
    }

    isExistNameCategoryWhenEdit() {
        self = this;
        var category = {
            Name: $("#txtCategoryName").val(),
            ID: $('#txtIDCategoryEdit').val()
        }
        $.ajax({
            url: "/api/product/isexistnamecategorywhenedit",
            method: "POST",
            data: JSON.stringify(category),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == false) {
                self.editCategory();
            }
            else {
                $("#error-input-categoryname").html("Tên loại đã tồn tại!");
            }
        });
    }

    editCategory() {
        var category = {
            Name: $('#txtCategoryName').val(),
            ID: $('#txtIDCategoryEdit').val()
        }
        $.ajax({
            url: "/api/product/editCategory",
            method: "POST",
            data: JSON.stringify(category),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            contentType: "application/json",
            dataType: "json"
        }).done(function (response) {
            if (response == true) {
                self.getListCategory();
                $('#divAddCategory').show();
                $('#divEditCategory').hide();
                $('#txtCategoryName').val("");
            }
            else {
                $('#error-input-editcate').html("Sửa loại thất bại");
            }
        });
    }

    deleteCategory(idCategory) {
        self = this;
        $.ajax({
            url: '/api/product/deletecategory/' + idCategory,
            method: "DELETE",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json' //Định nghĩa type data trả về.
            },
            dataType: "json", //Kiểu dữ liệu truyền lên.
        }).done(function (response) {
            if (response) {
                self.getListCategory();
            }
            else
                alert("Không thể xóa vì tồn tại thể loại thuộc loại này");
        });
    }
}