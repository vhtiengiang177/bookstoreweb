﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOOKSTOREWEB.Models;
using BOOKSTOREWEB.DAO;
using System.Data;

namespace BOOKSTOREWEB.Controllers
{
    [RoutePrefix("api/voucher")]
    public class VoucherController : ApiController
    {

        // GET: Voucher
        [HttpGet]
        [Route("list-voucher")]
        public DataTable GetListVoucher()
        {
            return VoucherDAO.Instance.GetListVoucher();
        }

        //lấy chi tiết KM
        [HttpGet]
        [Route("list-detail-voucher/{id}")]
        public DataTable GetDetailVoucherByID(int id)
        {
            return VoucherDAO.Instance.GetDetailVoucherByID(id);
        }


        //thông tin KM theo id
        [HttpGet]
        [Route("list-info-voucher/{id}")]
        public DataTable GetInfoVoucherByID(int id)
        {
            return VoucherDAO.Instance.GetInfoVoucherByID(id);
        }


        [HttpPost] // insert
        [Route("addVoucher")]
        public bool AddVoucher([FromBody] Voucher v)
        {
           
            return VoucherDAO.Instance.AddVoucher(v);
        }


        [HttpPut] //update
        [Route("updateIdVoucher/{id}/{idvoucher}")]
        public bool UpdateProductVoucher(int id,int idvoucher)
        {

            return VoucherDAO.Instance.UpdateProductVoucher(id,idvoucher);
        }


        [HttpPut] //xóa khuyến mãi của sản phẩm
        [Route("deleteIdVoucher/{id}")]
        public bool DeleteProductVoucher(int id)
        {

            return VoucherDAO.Instance.DeleteProductVoucher(id);
        }

        [HttpPut] //xóa khuyến mãi của sản phẩm
        [Route("deleteIdVoucher2/{id}")]
        public bool DeleteProductVoucher2(int id)
        {

            return VoucherDAO.Instance.DeleteProductVoucher2(id);
        }


        //Kiểm tra trùng tên
        [HttpPost]
        [Route("isexistnamevoucher")]
        public bool IsExistNameVoucher([FromBody] Voucher v)
        {
            if (VoucherDAO.Instance.IsExistNameVoucher(v.Name))
            {
                return true;
            }
            return false;
        }

        //tìm KM

        [HttpGet]
        [Route("searchvoucher/{search}")]
        public DataTable SearchVoucher(string search)
        {
            return VoucherDAO.Instance.SearchVoucher(search);
        }

        //lấy ds sản phẩm theo type và không có km
        [HttpGet]
        [Route("get-list-product-by-type/{idtype}")]
        public DataTable getListProByType(int idtype)
        {
            return VoucherDAO.Instance.getListProByType(idtype);
        }

        //lấy ds sản phẩm theo type và có km
        [HttpGet]
        [Route("get-list-product-by-type-by-voucher/{idtype}/{idvoucher}")]
        public DataTable getListProByTypeByVoucher(int idtype,int idvoucher)
        {
            return VoucherDAO.Instance.getListProByTypeByVoucher(idtype,idvoucher);
        }

        //xóa KM khỏi danh sách KM
        [HttpDelete]
        [Route("deleteVoucher/{id}")]
        public bool DeleteVoucher(int id)
        {
            return VoucherDAO.Instance.DeleteVoucher(id);
        }


       


        [HttpPut] //cập nhật khuyến mãi của sản phẩm
        [Route("updateVoucher")]
        public bool UpdateVoucher(Voucher v)
        {

            return VoucherDAO.Instance.UpdateVoucher(v);
        }


        // GET: api/Voucher/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Voucher
        public void Post([FromBody]string value)
        {

        }

        // PUT: api/Voucher/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Voucher/5
        public void Delete(int id)
        {
        }
    }
}
