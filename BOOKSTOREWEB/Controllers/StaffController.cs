﻿using BOOKSTOREWEB.DAO;
using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace BOOKSTOREWEB.Controllers
{
    [RoutePrefix("api/staff")]
    public class StaffController : ApiController
    {
        [HttpGet]
        [Route("staff-session")]
        public Staff GetStaffSession()
        {
            return VarDAO.Instance.staff;
        }

        // GET: api/Staff
        [HttpGet]
        [Route("get-staff/{id}")]
        public Staff GetStaffByID(int id)
        {
            return StaffDAO.Instance.GetStaffByID(id);
        }

        [HttpGet]
        [Route("get-staff-session")]
        public DataTable StaffGetStaffSession()
        {
            return StaffDAO.Instance.GetFullInfoStaffByID(VarDAO.Instance.staff.ID);
        }

        [HttpGet]
        [Route("get-avatar-staff-session")]
        public string StaffGetAvatarStaffSession()
        {
            return StaffDAO.Instance.ConvertImageToBase64String(VarDAO.Instance.staff.Avatar);
        }
        // POST: api/Staff
        [HttpPost]
        [Route("set-staff-session/{id}")]
        public bool Post(int id)
        {
            if (id != 0)
                VarDAO.Instance.staff = StaffDAO.Instance.GetStaffByID(id);
            else
                VarDAO.Instance.staff = null;
            return true;
        }

        public class AccountAdmin
        {
            public Staff staff;
            public Permissions permission;
            public string avatarStr;
        }

        public class StaffInfo
        {
            public Staff staff;
            public Account account;
            public string avatarStr;
        }

        [HttpPost]
        [Route("add-staff")]
        public bool PostAddCustomer([FromBody] StaffInfo user)
        {
            Staff staff = user.staff;
            Account account = user.account;
            string avatarStr = user.avatarStr;
            Image avatar = StaffDAO.Instance.ConvertFromBase64StringToImage(avatarStr);
            staff.Avatar = avatar;
            return StaffDAO.Instance.AddStaff(staff, account);
        }


        // PUT: api/Staff/5
        [HttpPut]
        [Route("update-staff")]
         public bool Put([FromBody] AccountAdmin accountAdmin)
        {
            Staff staff = accountAdmin.staff;
            Permissions permission = accountAdmin.permission;
            string avatarStr = accountAdmin.avatarStr;
            int id = staff.ID;
            string name = staff.Name;
            Image avatar = StaffDAO.Instance.ConvertFromBase64StringToImage(avatarStr);
            DateTime birthDay = staff.BirthDay;
            string gender = staff.Gender;
            string address = staff.Address;
            string phone = staff.Phone;
            string email = staff.Email;
            string per = permission.Permission;
            bool isUpdated = StaffDAO.Instance.UpdateInfoStaffById(id, name, avatar, birthDay, gender, address, phone, email, per);
            return isUpdated;
        }
       
        // DELETE: api/Staff/5
        public void Delete(int id)
        {
                
        }
    }
}
