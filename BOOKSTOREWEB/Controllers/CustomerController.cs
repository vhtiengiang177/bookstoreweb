﻿using BOOKSTOREWEB.DAO;
using BOOKSTOREWEB.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.Results;
using System.Web.UI.WebControls;

namespace BOOKSTOREWEB.Controllers
{
    [RoutePrefix("api/customer")]
    public class CustomerController : ApiController
    {
        // GET: api/Customer
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Customer/5
        public string Get(int id)
        {
            return "value";
        }

        public class CustomerInfo
        {
            public Customer customer { get; set; }
            public Account account { get; set; }
        }

        [HttpPost]
        [Route("addcustomer")]
        public bool PostAddCustomer([FromBody] CustomerInfo user)
        {
            Customer customer = user.customer;
            Account account = user.account;
            return CustomerDAO.Instance.AddCustomer(customer, account);
        }

        [HttpPost]
        [Route("addnewcustomer")]
        public bool PostAddNewCustomer([FromBody] CustomerInfo user)
        {
            Customer customer = user.customer;
            Account account = user.account;
            return CustomerDAO.Instance.AddNewCustomer(customer, account);
        }

        // POST: api/Customer
        [HttpPost]
        [Route("isexistemail")] 
        public bool PostIsExistEmail([FromBody]Customer customer)
        {
            if( CustomerDAO.Instance.IsExistEmail(customer.Email))
            {
                var tmp = customer.Email;
                  VarDAO.Instance.customer.Email = customer.Email;
              //  VarDAO.Instance.customer.Email = "thaole301000@gmail.com";

             
                return true;
            }
            return false;
        }

        // PUT: api/Customer/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Customer/5
        public void Delete(int id)
        {
        }

        [HttpGet]
        [Route("getCustomerInfo/{idCus}")]
        public Customer GetCustomerInfo (int idCus)
        {
            return CustomerDAO.Instance.GetCustomer(idCus);
        }

        [HttpPut]
        [Route("edit/{idCus}")]
        public Customer editCustomer(int idCus ,[FromBody] Customer customer)
        {
            return CustomerDAO.Instance.UpdateCustomer(idCus, customer);
        }
     
        [HttpGet]
        [Route("getListCustomer")]
        public DataTable GetListCustomer()
        {
            return CustomerDAO.Instance.GetListCustomer();
        }

        [HttpGet]
        [Route("searchcustomer/{search}")]
        public DataTable SearchCustomer(string search)
        {
            return CustomerDAO.Instance.SearchCus(search);
        }

        [HttpGet]
        [Route("getbillcusbydate/{idCus}/{date}")]
        public DataTable GetInfoBillOfCusByDate(int idCus, string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            return CustomerDAO.Instance.GetInfoBillOfCusByDate(idCus, dateTime );
        }

        [HttpGet]
        [Route("getbillcus/{idCus}")]
        public DataTable GetInfoBillOfCus(int idCus)
        {
            return CustomerDAO.Instance.GetInfoBillOfCus(idCus);
        }

        [HttpGet]
        [Route("getbilldetail/{id}")]
        public DataTable GetBillDetail(int id)
        {
            return CustomerDAO.Instance.GetBillDetail(id);
        }

        [HttpGet]
        [Route("getinfobillbyidbill/{id}")]
        public DataTable GetInfoBillByIDBill(int id)
        {
            return BillDAO.Instance.GetInfoBillByIDBill(id);
        }
    }
}
