﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOOKSTOREWEB.Models;
using BOOKSTOREWEB.DAO;
using System.Data;

namespace BOOKSTOREWEB.Controllers
{
    [RoutePrefix("api/delivery")]
    public class DeliveryController : ApiController
    {

        // GET:Delivery
        [HttpGet]
        [Route("list-delivery")]
        public DataTable GetListDelivery()
        {
            return DeliveryDAO.Instance.GetListDelivery();
        }

        
        //Tìm đơn vị vận chuyển
        [HttpGet]
        [Route("searchdelivery/{search}")]
        public DataTable SearchVoucher(string search)
        {
            return DeliveryDAO.Instance.SearchDelivery(search);
        }

        //Kiểm tra trùng tên
        [HttpPost]
        [Route("isexistnamedelivery")]
        public bool IsExistNameDelivery([FromBody] Delivery d)
        {
            if (DeliveryDAO.Instance.IsExistNameDelivery(d.Name))
            {
                return true;
            }
            return false;
        }

        [HttpPost] // insert
        [Route("addDelivery")]
        public bool AddDelivery([FromBody] Delivery d)
        {

            return DeliveryDAO.Instance.AddDelivery(d);
        }


        [HttpPut] //cập nhật đơn vị vị vận chuyển
        [Route("updateDelivery")]
        public bool UpdateDelivery(Delivery d)
        {

            return DeliveryDAO.Instance.UpdateDelivery(d);
        }


        [HttpPut] //cập nhật đơn vị vị vận chuyển
        [Route("deleteDelivery")]
        public bool DeleteDelivery(Delivery d)
        {

            return DeliveryDAO.Instance.DeleteDelivery(d);
        }

        //thông tin đơn vị theo id
        [HttpGet]
        [Route("list-info-delivery/{id}")]
        public DataTable GetInfoDeliveryByID(int id)
        {
            return DeliveryDAO.Instance.GetInfoDeliveryByID(id);
        }

    }
}
