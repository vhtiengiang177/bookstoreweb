﻿using BOOKSTOREWEB.DAO;
using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BOOKSTOREWEB.Models;

namespace BOOKSTOREWEB.Controllers
{
    [RoutePrefix("api/bill")]
    public class BillController : ApiController
    {
        [HttpGet]
        [Route("list-transport")]
        public DataTable Get()
        {
            return BillDAO.Instance.GetListTransport();
        }

        [HttpGet]
        [Route("list-transport/{state}")]
        public DataTable GetListTransportByState(string state)
        {
            return BillDAO.Instance.GetListTransportByState(state);
        }

        [HttpGet]
        [Route("list-transport-detail/{idBill}")]
        public DataTable GetTransportDetailByIDBill(int idBill)
        {
            return BillDAO.Instance.GetTransportDetailByIDBill(idBill);
        }

        [HttpGet]
        [Route("list-transport/{idBill}/{state}")]
        public DataTable GetTransportByIDBillAndState(int idBill, string state)
        {
            return BillDAO.Instance.GetTransportByIDBillAndState(idBill, state);
        }

        [HttpGet]
        [Route("list-order")]
        public DataTable GetOrderFullState()
        {
            return BillDAO.Instance.GetOrderFullState();
        }


        [HttpGet]
        [Route("list-order/{idBill}")]
        public DataTable GetOrderByIDBill(int idBill)
        {
            return BillDAO.Instance.GetOrderByIDBill(idBill);
        }

        [HttpGet]
        [Route("info-customer/{idBill}")]
        public DataTable GetInfoCustomerByIDBill(int idBill)
        {
            return BillDAO.Instance.GetCustomerByIDBill(idBill);
        }

        [HttpGet]
        [Route("info-bill/{idBill}")]
        public DataTable GetInfoBillByIDBill(int idBill)
        {
            return BillDAO.Instance.GetInfoBillByIDBill(idBill);
        }

        [HttpGet]
        [Route("revenue-report/{date}")]
        public DataTable GetRevenueReportOfWeek(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            return BillDAO.Instance.GetRevenueReportOfWeek(dateTime);
        }

        [HttpGet]
        [Route("revenue-report-month/{date}")]
        public DataTable GetRevenueReportOfMonth(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            return BillDAO.Instance.GetRevenueReportOfMonth(dateTime);
        }

        [HttpGet]
        [Route("revenue-detail/{date}")]
        public List<int> GetOrderNumberOfWeek(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            List<int> Numbers = new List<int>();
            int OrderNumber = BillDAO.Instance.GetOrderNumberOfWeek(dateTime);
            int CustomerNumber = BillDAO.Instance.GetCustomerNumberOfWeek(dateTime);
            int CompleteNumber = BillDAO.Instance.GetCompleteNumberOfWeek(dateTime);
            int ReturnNumber = BillDAO.Instance.GetReturnNumberOfWeek(dateTime);
            int CancelledNumber = BillDAO.Instance.GetCancelledNumberOfWeek(dateTime);
            int TotalRevenue = BillDAO.Instance.GetTotalRevenueReportOfWeek(dateTime);
            Numbers.Add(OrderNumber);
            Numbers.Add(CustomerNumber);
            Numbers.Add(CompleteNumber);
            Numbers.Add(ReturnNumber);
            Numbers.Add(CancelledNumber);
            Numbers.Add(TotalRevenue);
            return Numbers;
        }

        [HttpGet]
        [Route("revenue-detail-month/{date}")]
        public List<int> GetOrderNumberOfMonth(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            List<int> Numbers = new List<int>();
            int OrderNumber = BillDAO.Instance.GetOrderNumberOfMonth(dateTime);
            int CustomerNumber = BillDAO.Instance.GetCustomerNumberOfMonth(dateTime);
            int CompleteNumber = BillDAO.Instance.GetCompleteNumberOfMonth(dateTime);
            int ReturnNumber = BillDAO.Instance.GetReturnNumberOfMonth(dateTime);
            int CancelledNumber = BillDAO.Instance.GetCancelledNumberOfMonth(dateTime);
            int TotalRevenue = BillDAO.Instance.GetTotalRevenueReportOfMonth(dateTime);
            Numbers.Add(OrderNumber);
            Numbers.Add(CustomerNumber);
            Numbers.Add(CompleteNumber);
            Numbers.Add(ReturnNumber);
            Numbers.Add(CancelledNumber);
            Numbers.Add(TotalRevenue);
            return Numbers;
        }


        [HttpGet]
        [Route("products-report/{date}")]
        public DataTable GetProductsReportOfWeek(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            return BillDAO.Instance.GetSellingProductsOfWeek(dateTime);
        }

        [HttpGet]
        [Route("products-report-month/{date}")]
        public DataTable GetProductsReportOfMonth(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            return BillDAO.Instance.GetSellingProductsOfMonth(dateTime);
        }

        [HttpGet]
        [Route("customers-report/{date}")]
        public DataTable GetCustomersReportOfWeek(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            return BillDAO.Instance.GetSpendingCustomersOfWeek(dateTime);
        }

        [HttpGet]
        [Route("customers-report-month/{date}")]
        public DataTable GetCustomersReportOfMonth(string date)
        {
            DateTime dateTime = BillDAO.Instance.ConvertStringToDateTime(date);
            return BillDAO.Instance.GetSpendingCustomersOfMonth(dateTime);
        }
        // POST: api/Bill
        public void Post([FromBody]string value)
        {
        }
        //GUI_Thao: Tạo một đơn hàng mới
        [HttpPost]
        [Route("createBill")]
        public bool CreateNewBill([FromBody] Bill bi)
        {
            return BillDAO.Instance.CreateNewBill(bi.IDCustomer, bi.IDDelivery, bi.IDPayment, bi.IDVoucher, bi.AddressReceive, bi.Phone, bi.FeeShip, bi.TotalCost);
        }
        [HttpPost]
        [Route("updateBillDetail/{idCustomer}")]
        public bool UpdateInBillDetail(int idCustomer, [FromBody] string[] listProducts)
        {
            try
            {
                string[] a = listProducts;
                for (int i = 0; i < a.Length; i++)
                {
                    int idPro = Convert.ToInt32(a[i]);
                    BillDAO.Instance.MatchInBillDetail(idCustomer, idPro);
                }
                return true;
            }
            catch
            {
                return false;
            }
                
        }
        //GUI_Thao: Lấy đơn vị vận chuyển.
        [HttpGet]
        [Route("delivery")]
        public DataTable GetOpDelivery()
        {
            return BillDAO.Instance.GetDelivery();
        }
        //GUI_Thao: Lấy ds khuyến mãi. 
        [HttpGet]
        [Route("voucher")]
        public DataTable GetOpVoucher()
        {
            return BillDAO.Instance.GetPromotion();
        }

        // PUT: api/Bill/5
        [HttpPut]
        [Route("update-transport/{idBill}/{state}")]
        public bool Put(int idBill, string state)
        {
            return BillDAO.Instance.UpdateOrderStateByIdBill(idBill, state);
        }

        // DELETE: api/Bill/5
        public void Delete(int id)
        {
        }


        // Hoàng thêm
       

        [HttpGet]
        [Route("listBoughtBill/{idCus}/{state}")]
        public List<Bill> GetListBillofAcusState(int idCus, string state)
        {
            List<Bill> data = BillDAO.Instance.GetListBillofCus(idCus,state);
            return data;
        }

        [HttpGet]
        [Route("searchbill/{idCus}/{state}/{condition}/{key}")]
        public List<Bill> searchBill( int idCus, string state,  int condition, string key)
        {
            List<Bill> data = BillDAO.Instance.searchBill( idCus, state, condition, key);
            return data;
        }

        [HttpGet]
        [Route("getDetailbill/{idbill}")]
        public Bill getDetailBill(int idbill)
        {
            Bill data = BillDAO.Instance.getDetailBill(idbill);
            return data;
        }


    }
}
