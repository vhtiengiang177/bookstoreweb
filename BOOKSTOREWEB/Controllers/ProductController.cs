﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using BOOKSTOREWEB.DAO;
using System.Net.Http;
using System.Web.Http;
using BOOKSTOREWEB.Models;
using System.Drawing;
using System.IO;

namespace BOOKSTOREWEB.Controllers
{
    [RoutePrefix("api/product")]
    public class ProductController : ApiController
    {
        // GET: Product
        [HttpGet]
        [System.Web.Http.Route("listallproduct")]
        public DataTable GetAll()
        {
            return ProductDAO.Instance.GetProducts();    
        }


        [HttpGet]
        [System.Web.Http.Route("listalltypeproduct")]
        public DataTable GetListType()
        {
           return TypesDAO.Instance.GetTableTypes();
        }

        [HttpGet]
        [Route("listbestsaler")]
        //Lấy những sách bán chạy nhất trong tháng
        public DataTable GetTheBestsaler ()
        {
            ProductDAO a = new ProductDAO();
            return a.GetThebestSaler(10);
        }

        [HttpGet]
        [Route("listbestsaler/{idCat}")]
        //Lấy những sách bán chạy nhất trong tháng theo loại sách
        public List<Product> GetTheBestsalerWithCatogery(int idCat)
        {
            ProductDAO a = new ProductDAO();
            return a.GetThebestSalerWithCato(6,idCat);
        }



        [HttpGet]
        [Route("listalltypeproduct/{typename}")]
        //Lấy sách theo tên loại sách
        public DataTable GetProductbyTypename(string typename)
        {
            return ProductDAO.Instance.GetProductWithType(typename);
        }

        [HttpGet]
        [Route("getNameType/{typeID}")]
        //Lấy tên của một thể loại truyền vào ID
        public string GetNameType (int typeID)
        {
            return TypesDAO.Instance.getNameType(typeID);
        }

        [HttpGet]
        [Route("getproduct/{typeID}")]
        //Lấy tất cả sách của một thể loại
        public DataTable GetProductbyTypeID(int typeID)
        {
            return ProductDAO.Instance.GetProductWithType(typeID);
        }

        [HttpGet]
        [Route("listallHotdeal")]
        //Lấy sản phảm giảm giá
        public DataTable GetProductbyHotdeal()
        {
            return ProductDAO.Instance.GetListHotdealProduct();
        }


        [HttpGet]
        [Route("listNewestproduct")]
        public DataTable GetNewestProduct ()
        {
            return ProductDAO.Instance.GetNewestProduct(8);
        }


        [HttpPost] 
        [Route("addtocart")] 
        // POST: api/product
        public bool HandleCart([FromBody] Cart cart) //[From Body] là dùng để truyền dữ liệu thông qua Request Body.
        {
            int quantity = Checkquantity(cart.IDCustomer, cart.IDProduct);
            //nếu chưa có loại hàng này thì số lượng =1;
            if (quantity == 0)
            {
                cart.Quantity = quantity + 1;
                return CartDAO.Instance.AddCart(cart);
            }

            else
            //Trường hợp đã có hàng sẵn thì cập nhật lại số lượng
            {
                return CartDAO.Instance.UpdateCart(cart.IDCustomer, cart.IDProduct, quantity+1);
            }
                   
        }

        //Kiểm tra trong giỏ hàng đã có sản phẩm này chưa. Nếu có thì lấy số lương trả về, chưa thì trả về 0
        
        public int Checkquantity(int idCus, int idPro)
        {

            return CartDAO.Instance.Checkquantity(idPro, idCus);
        }

        [HttpPut]
        [Route("update-cart/{idCus}/{idPro}")]
        public bool UpdateCart (int idCus, int idPro, int quantity)
        {
            return CartDAO.Instance.UpdateCart(idCus, idPro, quantity);
        }


        //Thao get all Product
        [HttpGet]
        [Route("listProduct")]
        public DataTable Get()
        {
            return ProductDAO.Instance.GetListProduct();
        }
        //Get listproduct of Customer
        [HttpGet]
        [Route("listProduct/{idCus}")]
        public DataTable GetListBookByIDCus(int idCus)
        {
            return ProductDAO.Instance.GetProductByIdCus(idCus);
        }
        //Lấy thông sản phẩm theo id
        [HttpGet]
        [Route("detailProduct/{idPro}")]
        public DataTable GetDetailProduct(int idPro)
        {
            return ProductDAO.Instance.GetDetailProduct(idPro);
        }
        //Lấy các bình luận của một sản phẩm cụ thể
        [HttpGet]
        [Route("comment/{idPro}")]
        public DataTable GetCommentOfProduct(int idPro)
        {
            return ProductDAO.Instance.GetCommentOfProduct(idPro);
        }
        //Lấy số lượng sản phẩm trong giỏ hàng
        [HttpGet]
        [Route("quantityOfInCart/{idCus}")]
        public int GetQuantityInCArt(int idCus)
        {
            return ProductDAO.Instance.GetQuantityInCart(idCus);
        }
        //Lấy hình ảnh để hiển thị trong Detail Page
        [HttpGet]
        [Route("photosDetail/{idProduct}")]
        public DataTable GetPhotosDetail(int idProduct)
        {
            DataTable a = ProductDAO.Instance.GetPhotoInDetail(idProduct);
            return a;
        } 

        //Lưu vào giỏ hàng của khách hàng
        [HttpPost]
        [Route("createOrder/{idCus}/{idPro}/{quantity}")]
        public bool CreateOrder(int idCus, int idPro, int quantity)
        {
            return ProductDAO.Instance.SaveInCart(idCus, idPro, quantity);
        }
        // POST: api/Product
        //Thao-POST:ap/product/createComment
        [HttpPost]
        [Route("createComment")]
        public bool SaveComment([FromBody] Comment com)
        {
            return ProductDAO.Instance.SaveComment(com);
        }


        // End Thao API


        #region GIANG BACKEND
        //Edit by An -- add image
        public class ProductFull
        {
            public Product product;
            public string[] imageSrcs;
        }
        [HttpPost] // insert
        [Route("addProduct")]
        public bool AddProduct([FromBody]ProductFull productFull)
        {
            Product p = productFull.product;
            string[] imageSrcs = productFull.imageSrcs;
            p.IDShop = 1;
            if (ProductDAO.Instance.AddProduct(p))
            {
                int idProduct = ProductDAO.Instance.GetMaxIDProduct();
                for (int i = 0; i < imageSrcs.Length; i++)
                {
                    Image image = StaffDAO.Instance.ConvertFromBase64StringToImage(imageSrcs[i]);
                    PhotoProduct photoProduct = new PhotoProduct(idProduct, image);
                    if (!PhotoProductDAO.Instance.AddPhotoProduct(photoProduct))
                    {
                        ProductDAO.Instance.DeleteProductError(idProduct);
                        return false;
                    }
                }
                return true;
            }
            return false;
        }

        /// <summary>
        /// Lấy thể loại sách đổ vào dropdownlist
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("listTypeProduct")]
        public DataTable GetListTypeProduct()
        {
            return ProductDAO.Instance.GetListTypePro();
        }

        /// <summary>
        /// Kiểm tra tên sản phẩm có trùng không
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("isexistnameproduct")]
        public bool IsExistNameProduct([FromBody]Product p)
        {
            if (ProductDAO.Instance.IsExistNameProduct(p.Name))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Lấy thông tin toàn bộ sp
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("listtableproduct/{select}")]
        public DataTable GetDataTablePro(int select)
        {
            if(select != 0)
            {
                return ProductDAO.Instance.GetProductForDataTableFilter(select);
            }
            return ProductDAO.Instance.GetProductForDataTable();
        }

        // Tìm kiếm sp
        [HttpGet]
        [Route("searchproduct/{search}/{select}")]
        public DataTable SearchProduct(string search, int select)
        {
            if(select != 0)
            {
                return ProductDAO.Instance.SearchProductFilter(search, select);
            }
            return ProductDAO.Instance.SearchProduct(search);
        }

        // Xóa sp
        [HttpDelete]
        [Route("deleteproduct/{id}")]
        public bool Delete(int id)
        {
            if (ProductDAO.Instance.DeleteProduct(id))
            {
                return true;
            }
            else return false;
        }

        // Lấy thông tin 1 sp
        [HttpGet]
        [Route("getinfooneproduct/{id}")]
        public DataTable GetInfoOneProduct(int id)
        {
            return ProductDAO.Instance.GetInfoOneProduct(id);
        }

        // Chỉnh sửa thông tin sp
        [HttpPut]
        [Route("editProduct")]
        public bool Edit([FromBody]Product p)
        {
            p.IDShop = 1;
            return ProductDAO.Instance.EditProduct(p);
        }

        // Kiếm tra tên sp khi chỉnh sửa 
        [HttpPost]
        [Route("isexistnameproductwhenedit")]
        public bool IsExistNameProductWhenEdit([FromBody]Product p)
        {
            if (ProductDAO.Instance.IsExistNameProductWhenEdit(p.Name, p.ID))
            {
                return true;
            }
            return false;
        }

        // Lấy danh sách loại đổ vào drop list
        [HttpGet]
        [Route("listCategory")]
        public DataTable GetListCategory()
        {
            return ProductDAO.Instance.GetListCategory();
        }

        // Kiểm tra tên thể loại Type có trùng không
        [HttpPost]
        [Route("isexistnametype")]
        public bool IsExistNameType([FromBody]Types t)
        {
            if (TypesDAO.Instance.IsExistNameType(t.Name))
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [Route("isexistnamecategory")]
        public bool IsExistNameCategory([FromBody]Category c)
        {
            if (CategoryDAO.Instance.IsExistNameCategory(c.Name))
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [Route("addType")]
        public bool AddTypes([FromBody]Types t)
        {
            if (TypesDAO.Instance.AddTypes(t))
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [Route("addCategory")]
        public bool AddCategory([FromBody]Category c)
        {
            if (CategoryDAO.Instance.AddCategory(c))
            {
                return true;
            }
            return false;
        }

        // Lấy thông tin 1 thể loại type
        [HttpGet]
        [Route("getinfoonetype/{id}")]
        public DataTable GetInfoOneType(int id)
        {
            return TypesDAO.Instance.GetInfoOneType(id);
        }

        // Lấy thông tin 1 loại category
        [HttpGet]
        [Route("getinfoonecategory/{id}")]
        public DataTable GetInfoOneCategory(int id)
        {
            return CategoryDAO.Instance.GetInfoOneCategory(id);
        }

        // Kiếm tra tên thể loại type khi chỉnh sửa 
        [HttpPost]
        [Route("isexistnametypewhenedit")]
        public bool IsExistNameTypesWhenEdit([FromBody]Types t)
        {
            if (TypesDAO.Instance.IsExistNameTypesWhenEdit(t.Name, t.ID))
            {
                return true;
            }
            return false;
        }

        // Kiếm tra tên loại category khi chỉnh sửa 
        [HttpPost]
        [Route("isexistnamecategorywhenedit")]
        public bool IsExistNameCategoryWhenEdit([FromBody]Category c)
        {
            if (CategoryDAO.Instance.IsExistNameCategoryWhenEdit(c.Name, c.ID))
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [Route("editType")]
        public bool EditTypes([FromBody]Types t)
        {
            if (TypesDAO.Instance.UpdateTypes(t))
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        [Route("editCategory")]
        public bool EditCategory([FromBody]Category c)
        {
            if (CategoryDAO.Instance.UpdateCategory(c))
            {
                return true;
            }
            return false;
        }

        [HttpDelete]
        [Route("deletetypes/{id}")]
        public bool DeleteTypes(int id)
        {
            try
            {
                if (TypesDAO.Instance.DeleteTypes(id))
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        [HttpDelete]
        [Route("deletecategory/{id}")]
        public bool DeleteCategory(int id)
        {
            try
            {
                if (CategoryDAO.Instance.DeleteCategory(id))
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        [HttpGet]
        [Route("highestproductscore")]
        public DataTable ProductHighestScore()
        {
            return ProductDAO.Instance.GetProductHighestScore();
        }

        [HttpGet]
        [Route("suggestBook/{idPro}")]
        //Lấy những sách bán chạy nhất trong tháng
        public DataTable  SuggestBook(int idPro)
        {
            return ProductDAO.Instance.GetSuggestBook(idPro);
        }
    }
}