﻿using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.DAO
{
    public class StaffDAO
    {
        private static StaffDAO instance;
        public static StaffDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new StaffDAO();
                return instance;
            }
            private set { instance = value; }
        }

        private StaffDAO()
        {

        }

        public Staff GetStaffByIDAccount(int id)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Staff WHERE idAccount = @id", new object[] { id });
            if (data.Rows.Count > 0)
                return new Staff(data.Rows[0]);
            return null;
        }

        public Staff GetStaffByID(int id)
        {

            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Staff WHERE id = @id", new object[] { id });
            if (data.Rows.Count > 0)
                return new Staff(data.Rows[0]);
            return null;
        }

        public DataTable GetFullInfoStaffByID(int id)
        {

            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetFullInfoStaffByID @id", new object[] { id });
            return data;
        }

        public bool UpdateInfoStaffById(int id, string name, Image avatar, DateTime birthDay, string gender,
            string address, string phone, string email, string permission)
        {
            MemoryStream imgStr = new MemoryStream();
            avatar.Save(imgStr, avatar.RawFormat);
            string query = "USP_UpdateInfoStaffByID @id , @name , @avatar , @birthDay , @gender , @address , @phone , @email , @permission";
            object[] objs = new object[] { id, name, imgStr.ToArray(), birthDay, gender, address, phone, email, permission };
            int result = DataProvider.Instance.ExecuteNonQuery(query, objs);
            return result > 0;
        }

        public string ConvertImageToBase64String(Image img)
        {
            MemoryStream imgStr = new MemoryStream();
            img.Save(imgStr, img.RawFormat);
            string base64String = Convert.ToBase64String(imgStr.ToArray());
            return base64String;
        }

        public Image ConvertFromBase64StringToImage(string base64String)
        {
            byte[] data = Convert.FromBase64String(base64String);
            using (var stream = new MemoryStream(data, 0, data.Length))
            {
                Image image = Image.FromStream(stream);
                return image;
            }
        }

        public bool AddStaff(Staff staff, Account account)
        {
            string userName = account.UserName;
            string passWord = account.PassWord;
            int idPermission = account.IDPermission;
            string name = staff.Name;
            Image avatar = staff.Avatar;
            MemoryStream imgStr = new MemoryStream();
            avatar.Save(imgStr, avatar.RawFormat);
            DateTime birthDay = staff.BirthDay;
            string gender = staff.Gender;
            string address = staff.Address;
            string phone = staff.Phone;
            string email = staff.Email;
            int idMan = VarDAO.Instance.staff.IDMan;
            string query = "EXEC USP_AddStaff @userName , @passWord , @idPermission ," +
                " @name , @avatar , @birthDay , @gender , @address , @phone , @email , @idMan";
            object[] obj = new object[] { userName, passWord, idPermission, name, imgStr.ToArray(), birthDay, gender, address, phone, email, idMan };
            int result = DataProvider.Instance.ExecuteNonQuery(query, obj);
            return result > 0;
        }

    }
}