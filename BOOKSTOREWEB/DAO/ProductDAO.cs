﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using BOOKSTOREWEB.Models;
using System.Data.SqlClient;
using System.Collections;

namespace BOOKSTOREWEB.DAO
{
    public class ProductDAO
    {

        private static ProductDAO instance;
        public static ProductDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new ProductDAO();
                return instance;
            }
            private set { instance = value; }
        }

        public ProductDAO()
        {
        }


        /// <summary>
        /// Lấy ra tất cả Sản phẩm và thông tin sản phẩm. 
        /// </summary>
        /// <returns>List<Account>Danh sách Accounts</returns>
        public List<Product> GetListProducts()
        {
            List<Product> products = new List<Product>();
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Product");
            foreach (DataRow row in data.Rows)
            {
                Product a = new Product(row);
                products.Add(a);
            }
            return products;
        }

        public DataTable GetProducts()
        {
           DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Product");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }
        /// <summary>
        /// Lấy sách theo tên loại sách
        /// </summary>
        /// <param name="nameType"></param>
        /// <returns></returns>
        public DataTable GetProductWithType (string nameType)
        {
            Types type = new Types();
            
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Types WHERE name = @nameType", new object[] {nameType});
            if (data.Rows.Count > 0)
              type =new Types (data.Rows[0]);

            int idType = type.ID;

            DataTable t = DataProvider.Instance.ExecuteQuery("SELECT * FROM Product where idType = @idType", new object[] { idType} );
            if (t.Rows.Count > 0)
                return t;
            return null;


        }

        public DataTable GetProductWithType(int idType)
        {

           // List<Product> list = new List<Product>();
            DataProvider a = new DataProvider();
            DataTable data = a.ExecuteQuery("USP_GetProductWithType @id_type ", new object[] { idType });
            //List<Product> products = new List<Product>();
            //foreach (DataRow row in data.Rows)
            //{
            //    Product pro = new Product(row);
            //    products.Add(pro);
            //}
            //return products;
            if (data.Rows.Count > 0)
                return data;
            return null;


        }
        /// <summary>
        /// Lấy danh sách thể loại sách
        /// </summary>
        /// <returns></returns>
        public DataTable GetListTypeProduct()
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Types");
            DataTable datafilter = new DataTable();
            if (data.Rows.Count >= 5)
            {
                for (int i = 0; i < 5; i++)
                {
                    datafilter.Rows.Add(data.Rows[i]);
                }
                return datafilter;
            }
            if(data.Rows.Count>0)
                return data;
            return null;
        }
        /// <summary>
        /// Lấy danh sách tất cả sản phẩm có giảm giá
        /// </summary>
        /// <returns></returns>
        public DataTable GetListHotdealProduct()
        {
            DataTable data;
            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet ("USP_GetListHotdeadProduct", CommandType.StoredProcedure);
               
            }
            //List<Product> products = new List<Product>();

            //foreach (DataRow row in data.Rows)
            //{
            //    Product pro = new Product(row);
            //    products.Add(pro);
            //}
            //return products;

            return data;
        }

        public DataTable GetThebestSaler(int top)
        {
            DataProvider a = new DataProvider();
            DataTable data = a.ExecuteQuery("USP_GetBestSaler @top ", new object[] {top });
            //List<Product> products = new List<Product>();
            //foreach (DataRow row in data.Rows)
            //{
            //    Product pro = new Product(row);
            //    products.Add(pro);
            //}
            //return products;
            return data;
        }

        //LẤY SẢN PHẨM BÁN CHẠY NHẤT THEO LOẠI

        public List<Product> GetThebestSalerWithCato(int top, int idtype)
        {
            DataProvider a = new DataProvider();
            DataTable data = a.ExecuteQuery("USP_GetBestSalerwithCato @top , @idtype ", new object[] { top, idtype});
            List<Product> products = new List<Product>();
            foreach (DataRow row in data.Rows)
            {
                Product pro = new Product(row);
                products.Add(pro);
            }
            return products;
        }

        public DataTable GetNewestProduct (int top)
        {
            DataProvider a = new DataProvider();
            DataTable data = a.ExecuteQuery("USP_GetNewestBook @top ", new object[] { top });
            //List<Product> products = new List<Product>();
            //foreach (DataRow row in data.Rows)
            //{
            //    Product pro = new Product(row);
            //    products.Add(pro);
            //}
            //return products;
            return data;
        }
        public DataTable GetSuggestBook(int idProduct)
        {
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_GetSuggestProduct", CommandType.StoredProcedure, new List<SqlParameter>() { new SqlParameter("@idProduct", idProduct) });
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        //Thao procdure
        public DataTable GetListProduct()
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetListProduct");
            DataTable data;

           // using (UI_My_DB mydb = new UI_My_DB())
            {
                DataProvider a = new DataProvider();
                data = a.ExecuteQuery ("USP_GetListProduct ");
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }
        public DataTable GetProductByIdCus(int idCus)
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetListProductByIDCus @idCus", new object[] { idCus });
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_GetListProductByIDCus", CommandType.StoredProcedure, new List<SqlParameter>() { new SqlParameter("@idCus", idCus) });
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }
        public DataTable GetDetailProduct(int idProduct)
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetProductById @idProduct", new object[] { idProduct });
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_GetProductById", CommandType.StoredProcedure, new List<SqlParameter>() { new SqlParameter("@idProduct", idProduct) });
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }
        //Thao
        public int GetQuantityInCart(int idCus)
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetProductById @idProduct", new object[] { idProduct });
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_QuantityInCart", CommandType.StoredProcedure, new List<SqlParameter>() { new SqlParameter("@idCus", idCus) });
            }
            return ((data.Rows.Count > 0) ? Convert.ToInt32(data.Rows[0][0]) : 0);
        }
        public DataTable GetCommentOfProduct(int idProduct)
        {
            //DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetCommenOfProduct @idProduct", new object[] { idProduct });
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_GetCommenOfProduct", CommandType.StoredProcedure, new List<SqlParameter>() { new SqlParameter("@idProduct", idProduct) });
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public bool SaveComment(Comment comt)
        {
            // int data = DataProvider.Instance.ExecuteNonQuery("USP_commentInProduct @idCus , @idPro , @idShop , @content , @rating ",
            //  new object[] { comt.IDCustomer, comt.IDProduct, comt.IDShop, comt.Content, comt.Rating });
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                List<SqlParameter> para = mydb.turntoListParam(new ArrayList() { comt.IDCustomer, comt.IDProduct, comt.IDShop, comt.Content, comt.Rating }, new string[] { "@idCus", "@idPro", "@idShop", "@content", "@rating" });
                data = mydb.ExecuteQueryDataSet("USP_commentInProduct", CommandType.StoredProcedure, para);
            }
            return true;
        }

        public bool SaveInCart(int idCus, int idPro, int quantity)
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetProductById @idProduct", new object[] { idProduct });
            bool data;
            //Thao
            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.MyExecuteNonQuery("USP_AddProductToCart", CommandType.StoredProcedure,
                    new List<SqlParameter>() { new SqlParameter("@idCus", idCus),
                        new SqlParameter("@idPro", idPro), new SqlParameter ("@quantity",quantity)});

            }
            return data;
        }
        //Get Image of picture
        public DataTable GetImageOfProduct(int idProduct)
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetProductById @idProduct", new object[] { idProduct });
            DataTable data;
            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_GetImgProduct", CommandType.StoredProcedure, new List<SqlParameter>() { new SqlParameter("@idProduct", idProduct) });
            }
            var tmp = data.Rows[0]["photo"];
            if (data.Rows.Count > 0)

                return data;
            return null;
        }
        public  DataTable GetPhotoInDetail(int idProduct)
        {
            DataTable data;
            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_GetImgProduct", CommandType.StoredProcedure, new List<SqlParameter>() { new SqlParameter("@idProduct", idProduct) });
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        #region GIANG

        /// <summary>
        /// Thêm sản phẩm
        /// </summary>
        /// <param name="p"></param>
        /// <returns>True/False</returns>
        public bool AddProduct(Product p)
        {
            string query = "EXEC USP_AddProduct @idshop , @idtype , @name , @price , @quantity , @author , @description , @publisher ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { p.IDShop, p.IDType, p.Name, p.Price, p.Quantity, p.Author, p.Description, p.Publisher }) > 0;
        }

        public bool IsExistNameProduct(string name)
        {
            string query = "SELECT * FROM dbo.Product WHERE name = @name AND quantity <> -1";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { name });
            return data.Rows.Count > 0;
        }

        public bool IsExistNameProductWhenEdit(string name, int id)
        {
            string query = "SELECT * FROM dbo.Product WHERE name = @name AND id <> @id AND quantity <> -1";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { name, id });
            return data.Rows.Count > 0;
        }

        public DataTable GetListTypePro()
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT Types.id, Types.name, Category.name as name_cate FROM Types, Category WHERE Types.idCategory = Category.id");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetListCategory()
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Category");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetProductForDataTable()
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT dbo.Product.id, dbo.Types.name as name_types, dbo.Product.name, price, quantity, quantitySold,score FROM Product, Types WHERE Product.idType = Types.id AND quantity <> -1 ORDER BY dbo.Product.id");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetProductForDataTableFilter(int select)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT dbo.Product.id, dbo.Types.name as name_types, dbo.Product.name, price, quantity, quantitySold,score FROM Product, Types WHERE Product.idType = Types.id AND quantity <> -1 AND idType = @select ORDER BY dbo.Product.id", new object[] { select });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable SearchProduct(string search)
        {
            string query = "EXEC USP_SearchProduct @search ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { search });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable SearchProductFilter(string search, int select)
        {
            string query = "EXEC USP_SearchProductFilter @search , @select ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { search, select });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        /// <summary>
        /// Thêm ảnh sản phẩm
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public bool AddPhotoProduct(PhotoProduct p)
        {
            string query = "EXEC USP_AddPhotoProduct @idproduct , @photo ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { p.IDProduct, p.Photo }) > 0;
        }

        /// <summary>
        /// Xóa sản phẩm
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true: Delete thành công - false: delete thất bại</returns>
        public bool DeleteProduct(int id)
        {
            string query = "UPDATE Product SET quantity = -1 WHERE id = @id";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { id });
            return result > 0;
        }

        public bool DeleteProductError(int id)
        {
            string query = "DELETE Product WHERE id = @id";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { id });
            return result > 0;
        }

        public DataTable GetInfoOneProduct(int id)
        {
            string query = "EXEC USP_GetInfoOneProduct @id ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { id });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public bool EditProduct(Product p)
        {
            string query = "EXEC USP_UpdateProduct @id , @idshop , @idtype , @name , @price , @quantity , @author , @description , @publisher ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { p.ID, p.IDShop, p.IDType, p.Name, p.Price, p.Quantity, p.Author, p.Description, p.Publisher }) > 0;
        }
        #endregion

        public DataTable GetProductHighestScore ()
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetProductHighestScore");
            //List<Product> products = new List<Product>();
            //if(data !=null && data.Rows.Count >0)
            //foreach (DataRow row in data.Rows)
            //{
            //    Product pro = new Product(row);
            //    products.Add(pro);
            //}
            //return products;
            return data;
        }

        #region An
        public int GetMaxIDProduct()
        {
            string query = "SELECT MAX(id) FROM Product";
            try
            {
                int result = (int)DataProvider.Instance.ExecuteScalar(query);
                return result;
            }
            catch
            {
                return 1;
            }
        }

        #endregion
    }
}
