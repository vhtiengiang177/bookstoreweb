﻿using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.DAO
{
    public class DeliveryDAO
    {

        private static DeliveryDAO instance;
        public static DeliveryDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new DeliveryDAO();
                return instance;
            }
            private set { instance = value; }
        }

        private DeliveryDAO()
        {

        }

        //Thảo Nguyễn
        //Lấy danh sách đơn vị vận chuyển
        public DataTable GetListDelivery()
        {

            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Delivery where feeShip > 0");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        //Tìm đơn vị vận chuyển theo tên hoặc phí
        public DataTable SearchDelivery(string str)
        {
            string query = "SELECT*FROM  FN_SearchDelivery( @str )";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { str });
        }

        //Thêm đơn vị vận chuyển
        public bool AddDelivery(Delivery d)
        {
            string query = "EXEC USP_AddDelivery  @name , @feeShip ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { d.Name,d.FeeShip }) > 0;
        }

        //kiểm tra tồn tại tên đơn vị
        public bool IsExistNameDelivery(string name)
        {
            string query = "SELECT * FROM dbo.Delivery WHERE name = @name";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { name });
            return data.Rows.Count > 0;
        }

        //cập nhật đơn vị vận chuyển
        public bool UpdateDelivery(Delivery d)
        {
            string query = "EXEC USP_UpdateDelivery @id , @name , @feeShip ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { d.ID, d.Name, d.FeeShip}) > 0;
        }


        //cập nhật phí đơn vị vận chuyển =-1
        public bool DeleteDelivery(Delivery d)
        {
            string query = "EXEC USP_UpdateFeeShipDelivery @id  ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { d.ID}) > 0;
        }

        //lấy thông tin voucher theo id
        public DataTable GetInfoDeliveryByID(int id)
        {

            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetInfoDeliveryByID @id", new object[] { id });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }
    }
}