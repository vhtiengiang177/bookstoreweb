﻿using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace BOOKSTOREWEB.DAO
{
    public class CustomerDAO
    {
        private static CustomerDAO instance;
        public static CustomerDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new CustomerDAO();
                return instance;
            }
            private set { instance = value; }
        }

        private CustomerDAO()
        {

        }

        public bool AddCustomer(Customer customer, Account account)
        {
            string userName = account.UserName;
            string passWord = account.PassWord;
            string name = customer.Name;
            DateTime birthDay = customer.BirthDay;
            string gender = customer.Gender;
            string address = customer.Address;
            string phone = customer.Phone;
            string email = customer.Email;
            string query = "EXEC USP_AddCustomer @userName , @passWord , @name , @birthDay , @gender , @address , @phone , @email";
            object[] obj = new object[] { userName, passWord, name, birthDay, gender, address, phone, email };
            int result = DataProvider.Instance.ExecuteNonQuery(query, obj);
            return result > 0;
        }

        public bool AddNewCustomer(Customer customer, Account account)
        {
            string userName = account.UserName;
            string passWord = account.PassWord;
            string name = customer.Name;
            DateTime birthDay = customer.BirthDay;
            string gender = customer.Gender;
            string address = customer.Address;
            string phone = customer.Phone;
            string email = customer.Email;
            string query = "EXEC USP_AddNewCustomer @userName , @passWord , @name , @birthDay , @gender , @address , @phone , @email";
            object[] obj = new object[] { userName, passWord, name, birthDay, gender, address, phone, email };
            int result = DataProvider.Instance.ExecuteNonQuery(query, obj);
            return result > 0;
        }


        public bool IsExistEmail(string email)
        {
            string query = "USP_IsExistEmail @email";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { email });
            return data.Rows.Count > 0;
        }

        public string SendEmail(string email)
        {
            string senderID = "antran2509@gmail.com";
            string senderPassword = "01692889894";
            string result = "Email Sent Successfully";
            string code = AccountDAO.Instance.CreateCode();
            string body = " " + email + " có một email từ " + senderID;
            body += ", Mã xác nhận của bạn là " + code;
            try
            {
                MailMessage mail = new MailMessage();
                mail.To.Add(email);
                mail.From = new MailAddress(senderID);
                mail.Subject = "Quên mật khẩu!";
                mail.Body = body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com"; //Or Your SMTP Server Address

                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential(senderID, senderPassword);
                smtp.Port = 587;
                smtp.EnableSsl = true;
                //return "123456";
                smtp.Send(mail);
                result = code;
            }
            catch
            {
                result = "problem occurred";
                //Response.Write("Exception in sendEmail:" + ex.Message);
            }
            return result;
        }

        public Customer GetCustomer(int id)
        {
            using (UI_My_DB mydb = new UI_My_DB())
            {
                string query = "select * from dbo.Customer where id ='" + id.ToString() + "'";
                DataTable data = mydb.ExecuteQueryDataSet(query, CommandType.Text);
                if (data.Rows.Count > 0)
                    return new Customer(data.Rows[0]);
                else
                    return null;
            }

        }

        public int GetIDCustomer(string userName)
        {
            string query = "select Customer.id from dbo.Customer, dbo.Account where userName = @userName and Customer.idAccount=Account.id ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { userName });
            if (data.Rows.Count > 0)
                return (int)data.Rows[0][0];
            else
                return 0;
        }

        public Customer UpdateCustomer(int idCus, Customer customer)
        {
            string query = "EXEC USP_UpdateCustomer @id , @name , @birthDay , @gender , @address , @phone , @email ";
            object[] obj = new object[] {idCus, customer.Name, customer.BirthDay, customer.Gender, customer.Address, customer.Phone, customer.Email };
            int result = DataProvider.Instance.ExecuteNonQuery(query, obj);
            if (result > 0) return GetCustomer(idCus);
            else return null;
        }

        #region Giang
        public DataTable GetListCustomer()
        {
            string query = "SELECT Customer.id, Customer.name, Account.userName as username, email, phone, address FROM Customer, Account WHERE Customer.idAccount = Account.id ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable SearchCus(string search)
        {
            string query = "EXEC USP_SearchCustomer @search ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { search });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetInfoBillOfCusByDate(int idCus, DateTime date)
        {
            string query = "EXEC USP_GetInfoBillOfCusByDate @idCus , @date ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { idCus, date });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetInfoBillOfCus(int idCus)
        {
            string query = "EXEC USP_GetInfoBillOfCus @idCus ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { idCus });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetBillDetail(int id)
        {
            string query = "EXEC USP_GetInfoBillDetail @id ";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { id });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }
        #endregion
    }
}