﻿using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.DAO
{
    public class VoucherDAO
    {
        private static VoucherDAO instance;
        public static VoucherDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new VoucherDAO();
                return instance;
            }
            private set { instance = value; }
        }

        private VoucherDAO()
        {

        }
        //Thảo Nguyễn
      
        //Lấy danh sách khuyến mãi
        public DataTable GetListVoucher()
        {
            
            DataTable data = DataProvider.Instance.ExecuteQuery("SELECT * FROM Voucher");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        //lấy thông tin voucher theo id
        public DataTable GetInfoVoucherByID(int id)
        {

            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetInfoVoucherByID @id", new object[] { id });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        //Lấy mô tả voucher theo id
        public DataTable GetDetailVoucherByID(int id)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetDetailVoucherByID @id", new object[] { id });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        //Thêm voucher
        public bool AddVoucher(Voucher v)
        {
            string query = "EXEC USP_AddVoucher  @name , @start , @end , @detail , @value ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] {  v.Name, v.StartDate,v.EndDate,v.Detail,v.Discount }) > 0;
        }

       

        //thêm mã khuyến mãi cho sản phẩm
        public bool UpdateProductVoucher(int id,int idvoucher)
        {
            string query = "EXEC USP_UpdateProductVoucher @id , @idvoucher ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { id,idvoucher }) > 0;
        }


        //xóa mã khuyesn mãi của sản phẩm
        public bool DeleteProductVoucher(int id)
        {
            string query = "EXEC USP_DeleteProductVoucher @id ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { id}) > 0;
        }

        //xóa mã khuyesn mãi của sản phẩm khi truyền idvoucher
        public bool DeleteProductVoucher2(int idvoucher)
        {
            string query = "EXEC USP_DeleteProductVoucher2 @idvoucher ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { idvoucher }) > 0;
        }

        //cập nhật khuyến mãi
        public bool UpdateVoucher(Voucher v)
        {
            string query = "EXEC USP_UpdateVoucher @id , @name , @start , @end , @detail , @discount ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { v.ID,v.Name,v.StartDate,v.EndDate,v.Detail, v.Discount }) > 0;
        }
        //Math.Round((Convert.ToDouble(v.Discount)),2)

        //Xóa voucher
        public bool DeleteVoucher(int id)
        {
            string query = "EXEC USP_DeleteVoucher @id  ";
            return DataProvider.Instance.ExecuteNonQuery(query, new object[] { id }) > 0;
        }


        //Tìm kiếm khuyến mãi theo tên và giá trị khuyesn mãi
        public DataTable SearchVoucher(string str)
        {
            string query = "SELECT*FROM  FN_SearchVoucher( @str )";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { str });
        }

        //kiểm tra tồn tại tên khuyến mãi
        public bool IsExistNameVoucher(string name)
        {
            string query = "SELECT * FROM dbo.Voucher WHERE name = @name";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { name });
            return data.Rows.Count > 0;
        }


        public DataTable getListProByType(int idtype)
        {
            string query = "SELECT*FROM  dbo.FN_getListProByType( @idtype )";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { idtype });
        }

        public DataTable getListProByTypeByVoucher(int idtype,int idvoucher)
        {
            string query = "SELECT*FROM  dbo.FN_getListProByTypeByVoucher( @idtype , @idvoucher )";
            return DataProvider.Instance.ExecuteQuery(query, new object[] { idtype, idvoucher });
        }

    }
}