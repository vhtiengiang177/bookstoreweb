﻿using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.DAO
{
    public class PhotoProductDAO
    {
        private static PhotoProductDAO instance;
        public static PhotoProductDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new PhotoProductDAO();
                return instance;
            }
            private set { instance = value; }
        }

        private PhotoProductDAO()
        {

        }

        public bool AddPhotoProduct(PhotoProduct photoProduct)
        {
            MemoryStream imgStr = new MemoryStream();
            photoProduct.Photo.Save(imgStr, photoProduct.Photo.RawFormat);
            string query = "INSERT INTO dbo.PhotoProduct (idProduct, photo) VALUES ( @idProduct , @photo )";
            int result = DataProvider.Instance.ExecuteNonQuery(query, new object[] { photoProduct.IDProduct, imgStr.ToArray() });
            return result > 0;
        }

    }
}