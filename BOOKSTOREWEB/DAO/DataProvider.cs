﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.DAO
{
    public class DataProvider
    {
        private static DataProvider instance;

        public static DataProvider Instance
        {
            get
            {
                if (instance == null)
                    instance = new DataProvider();
                return instance;
            }
            private set { instance = value; }
        }

        public DataProvider() { }

        /// <summary>
        /// Execute query trả về table. Dùng cho DataSoure của DataGridView.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public DataTable ExecuteQuery(string query, object[] parameter = null)
        {
            DataTable data = new DataTable();
            //My_DB a = new My_DB();
            SqlConnection sqlConnection = My_DB.Instance.createConnection();
            sqlConnection.Open();

            using (SqlCommand command = new SqlCommand(query,sqlConnection))
            {
                //My_DB.Instance.openConnection();
                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(data);
                //My_DB.Instance.closeConnection();
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return data;
        }

        /// <summary>
        /// Trả về số hàng, CHỈ DÙNG CHO INSERT UPDATE DELETE, không dùng đc cho SELECT
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string query, object[] parameter = null)
        {
            int data = 0;
            SqlConnection sqlConnection = My_DB.Instance.createConnection();
            sqlConnection.Open();
            using (SqlCommand command = new SqlCommand(query,sqlConnection))
            {
                command.CommandTimeout = 0;
               //My_DB.Instance.openConnection();
               
                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }
                data = command.ExecuteNonQuery();
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return data;
        }

        /// <summary>
        /// Trả về ô đầu tiên của hàng đầu tiên --> phù hợp cho câu truy vấn select count()
        /// </summary>
        /// <param name="query"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        public object ExecuteScalar(string query, object[] parameter = null)
        {
            object data = 0;
            //Hoang edit
            SqlConnection sqlConnection = My_DB.Instance.createConnection();
            sqlConnection.Open();

            using (SqlCommand command = new SqlCommand(query, sqlConnection))
            {
               
                if (parameter != null)
                {
                    string[] listPara = query.Split(' ');
                    int i = 0;
                    foreach (string item in listPara)
                    {
                        if (item.Contains('@'))
                        {
                            command.Parameters.AddWithValue(item, parameter[i]);
                            i++;
                        }
                    }
                }
                data = command.ExecuteScalar();
                //My_DB.Instance.closeConnection();
                //Hoang edit
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            return data;
        }
    }
}