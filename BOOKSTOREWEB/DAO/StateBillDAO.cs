﻿using BOOKSTOREWEB.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace BOOKSTOREWEB.DAO
{
    public class StateBillDAO
    {
        private static StateBillDAO instance;
        public static StateBillDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new StateBillDAO();
                return instance;
            }
            private set { instance = value; }
        }

        private StateBillDAO()
        {

        }

        public StateBill getStateBillByID(int id)
        {
            string query = "SELECT * FROM StateBill WHERE id = @id";
            DataTable data = DataProvider.Instance.ExecuteQuery(query, new object[] { id });
            if (data.Rows.Count > 0)
                return new StateBill(data.Rows[0]);
            return null;
        }

        public List<StateBill> getListStateBill()
        {
            List<StateBill> stateBills = new List<StateBill>();
            string query = "SELECT * FROM StateBill";
            DataTable data = DataProvider.Instance.ExecuteQuery(query);
            foreach(DataRow item in data.Rows)
            {
                StateBill state = new StateBill(item);
                stateBills.Add(state);
            }
            return stateBills;
        }
    }
}