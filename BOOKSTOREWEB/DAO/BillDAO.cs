﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using BOOKSTOREWEB.Models;

namespace BOOKSTOREWEB.DAO
{
    public class BillDAO
    {
        private static BillDAO instance;
        public static BillDAO Instance
        {
            get
            {
                if (instance == null)
                    instance = new BillDAO();
                return instance;
            }
            private set { instance = value; }
        }

        private BillDAO()
        {

        }

        public DataTable GetListTransport()
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetListTransport");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetListTransportByState(string state)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetListTransportByState @state", new object[] { state });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetOrderByIDBill(int idBill)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetOrderByIDBill @idBill", new object[] { idBill });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public bool UpdateOrderStateByIdBill(int idBill, string state)
        {
            int result = DataProvider.Instance.ExecuteNonQuery("USP_UpdateOrderStateByIdBill @idBill , @state", new object[] { idBill, state });
            return result > 0;
        }

        public DataTable GetTransportDetailByIDBill(int idBill)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetTransportDetailByIDBill @idBill", new object[] { idBill });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetTransportByIDBillAndState(int idBill, string state)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetTransportByIDBillAndState @idBill , @state", new object[] { idBill, state });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetOrderFullState()
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetOrderFullState");
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        public DataTable GetCustomerByIDBill(int idBill)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetInfoCustomerByIDBill @idBill", new object[] { idBill });
            if (data.Rows.Count > 0)
                return data;
            return null;    
        }

        public DataTable GetInfoBillByIDBill(int idBill)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetInfoBillByIDBill @idBill", new object[] { idBill });
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        //Pro_GUI_Thao: Lấy các đơn vị vận chuyển 
        public DataTable GetDelivery()
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetDelivery ");
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_GetDelivery ", CommandType.StoredProcedure);
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }
        
        //Pro_GUI_Thao: Lấy các Khuyến mãi áp dụng -- New Maybe Conflict
        public DataTable GetPromotion()
        {
            // DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetDelivery ");
            DataTable data;

            using (UI_My_DB mydb = new UI_My_DB())
            {
                data = mydb.ExecuteQueryDataSet("USP_getVoucher ", CommandType.StoredProcedure);
            }
            if (data.Rows.Count > 0)
                return data;
            return null;
        }

        //Pro_GUI_Thao: Cập nhật đơn hàng thanh toán
        public bool CreateNewBill(int idCus, int idDel, int idPay, int idVou,
                                    string add, string phone, double feeShip, double totalCost)
        {

            using (UI_My_DB mydb = new UI_My_DB())
            {
                List<SqlParameter> para = mydb.turntoListParam(new ArrayList() { idCus, idDel, idPay, idVou, add, phone, feeShip, totalCost },
                    new string[] { "@idCus", "@idDelivery", "@idPayment", "@idVoucher", "@address", "@phone", "@feeShip", "@totalCost" });
                return mydb.MyExecuteNonQuery("USP_create_Bill", CommandType.StoredProcedure, para);    
            }
            //  return result>0;
        }
        //Pro_GUI_Thao: Cập nhật đơn hàng thanh toán__ NewUpdate_ 3/1
        public bool MatchInBillDetail(int idCustomer, int idProduct )
        {

            using (UI_My_DB mydb = new UI_My_DB())
            {
                List<SqlParameter> para = mydb.turntoListParam(new ArrayList() {idCustomer, idProduct  },
                    new string[] { "@idCustomer","@IdProduct" });
                return mydb.MyExecuteNonQuery("USP_MatchInBillDetail", CommandType.StoredProcedure, para);
            }
            //  return result>0;
        }
        


        public DataTable GetRevenueReportOfWeek(DateTime date)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetRevenueReportOfWeek @date", new object[] { date });
            return data;
        }

        public DataTable GetRevenueReportOfMonth(DateTime date)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetRevenueReportOfMonth @date", new object[] { date });
            return data;
        }

        public DataTable GetSpendingCustomersOfWeek(DateTime date)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetCustomersByMostOfWeek @date", new object[] { date });
            return data;
        }

        public DataTable GetSpendingCustomersOfMonth(DateTime date)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetCustomersByMostOfMonth @date", new object[] { date });
            return data;
        }

        /// <summary>
        /// Chuyển chuỗi có dạng dd-mm-yyyy thành datetime
        /// </summary>
        /// <param name="sDate"></param>
        /// <returns></returns>
        public DateTime ConvertStringToDateTime(string sDate)
        {
            string[] str = sDate.Trim().Split('-');
            return new DateTime(Convert.ToInt32(str[0]), Convert.ToInt32(str[1]), Convert.ToInt32(str[2]));
        }

        public int GetOrderNumberOfWeek(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetOrderNumberOfWeek @date", new object[] { date });
            return data;
        }

        public int GetOrderNumberOfMonth(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetOrderNumberOfMonth @date", new object[] { date });
            return data;
        }

        public int GetCustomerNumberOfWeek(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetCustomerNumberOfWeek @date", new object[] { date });
            return data;
        }
        public int GetCustomerNumberOfMonth(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetCustomerNumberOfMonth @date", new object[] { date });
            return data;
        }

        public int GetCompleteNumberOfWeek(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetCompleteNumberOfWeek @date", new object[] { date });
            return data;
        }

        public int GetCompleteNumberOfMonth(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetCompleteNumberOfMonth @date", new object[] { date });
            return data;
        }

        public int GetReturnNumberOfWeek(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetReturnNumberOfWeek @date", new object[] { date });
            return data;
        }

        public int GetReturnNumberOfMonth(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetReturnNumberOfMonth @date", new object[] { date });
            return data;
        }

        public int GetCancelledNumberOfWeek(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetCancelledNumberOfWeek @date", new object[] { date });
            return data;
        }
        public int GetCancelledNumberOfMonth(DateTime date)
        {
            int data = (int)DataProvider.Instance.ExecuteScalar("USP_GetCancelledNumberOfMonth @date", new object[] { date });
            return data;
        }


        public int GetTotalRevenueReportOfWeek(DateTime date)
        {
           DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetTotalRevenueReportOfWeek @date", new object[] { date });
            if (data.Rows.Count > 0)
            {
                if (data.Rows[0]["totalRevenue"].ToString() == "")
                    return 0;
                return Convert.ToInt32(data.Rows[0]["totalRevenue"]);
            }
            return 0;
        }

        public int GetTotalRevenueReportOfMonth(DateTime date)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetTotalRevenueReportOfMonth @date", new object[] { date });
            if (data.Rows.Count > 0)
            {
                if (data.Rows[0]["totalRevenue"].ToString() == "")
                    return 0;
                return Convert.ToInt32(data.Rows[0]["totalRevenue"]);
            }
            return 0;
        }


        public DataTable GetSellingProductsOfWeek(DateTime date)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetSellingProductsOfWeek @date", new object[] { date });
            return data;
        }

        public DataTable GetSellingProductsOfMonth(DateTime date)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_GetSellingProductsOfMonth @date", new object[] { date });
            return data;
        }

        //Hoàng thêm
        //lấy danh sách bill của một khách hàng
        public List<Bill> GetListBillofCus(int idCus, string state)
        {
            DataTable data  = DataProvider.Instance.ExecuteQuery("USP_GetListBillOfAcus @idCus , @state ", new object[] { idCus,state });
            List<Bill> bills = new List<Bill>();
            foreach(DataRow row in data.Rows)
            {
                Bill bill = new Bill(row);
                List<DetailProduct> list = new List<DetailProduct>();
                DataTable temp = new DataTable();
               
                temp = DataProvider.Instance.ExecuteQuery("USP_GetAllProductOfBill @idBill", new object[] { bill.ID });
               
                foreach (DataRow r in temp.Rows)
                {
                    DetailProduct detail = new DetailProduct(r);
                    list.Add(detail);
                }
                bill.ListProduct = list;
                bills.Add(bill);
            }

            return bills;
        }

        //Tìm kiếm đơn hàng

        public List<Bill> searchBill(int idCus, string state, int condition, string key)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_searchBill @idCus , @state , @condition , @key ", new object[] { idCus, state, condition, key });
            List<Bill> bills = new List<Bill>();
            foreach (DataRow row in data.Rows)
            {
                Bill bill = new Bill(row);
                List<DetailProduct> list = new List<DetailProduct>();
                DataTable temp = new DataTable();

                temp = DataProvider.Instance.ExecuteQuery("USP_GetAllProductOfBill @idBill", new object[] { bill.ID });

                foreach (DataRow r in temp.Rows)
                {
                    DetailProduct detail = new DetailProduct(r);
                    list.Add(detail);
                }
                bill.ListProduct = list;
                bills.Add(bill);
            }

            return bills;
        }

        public Bill getDetailBill(int idBill)
        {
            DataTable data = DataProvider.Instance.ExecuteQuery("USP_getDetailBill @idBill ", new object[] { idBill});
            Bill bill = new Bill();
            if (data != null && data.Rows.Count > 0)
            {
                bill = new Bill(data.Rows[0]);
                bill.Voucher =float.Parse(data.Rows[0]["voucher"].ToString()) ;
                bill.Vouchername = data.Rows[0]["vouchername"].ToString();
                bill.Payment = data.Rows[0]["payment"].ToString();
                bill.Delivery = data.Rows[0]["delivery"].ToString();

                DataTable temp = new DataTable();

                temp = DataProvider.Instance.ExecuteQuery("USP_GetAllProductOfBill @idBill", new object[] { bill.ID });
                if (temp != null && temp.Rows.Count > 0)
                {
                    List<DetailProduct> list = new List<DetailProduct>();
                    foreach (DataRow r in temp.Rows)
                    {
                        DetailProduct detail = new DetailProduct(r);
                        list.Add(detail);
                    }
                    bill.ListProduct = list;
                }
            }
            return bill;
        }
    }
    
}