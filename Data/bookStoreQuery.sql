﻿CREATE DATABASE BOOKSTOREWEB1
GO

USE BOOKSTOREWEB1
GO



-- * ghi chú của Ân.

-----------1. Permission -- *done
CREATE TABLE Permissions(
	id INT IDENTITY PRIMARY KEY,
	permission  NVARCHAR(30))
	GO
-------------------------------------------------------------- insert
INSERT INTO Permissions VALUES ('admin'), ('staff'), ('shipper'), ('user')
Go


-----------2. Account -- *done
CREATE TABLE Account(
	id INT IDENTITY PRIMARY KEY,
	userName NVARCHAR(100) NOT NULL UNIQUE,
	passWord NVARCHAR(100) NOT NULL DEFAULT '00000000',
	idPermission INT NOT NULL DEFAULT 0,   
	FOREIGN KEY (idPermission) REFERENCES Permissions(id))
	GO
------------------------------------------------------------- insert
INSERT INTO Account VALUES ('admin001', 'admin001',1), ('staff001', 'staff001',2), ('shipper001', 'shipper001',3), ('user001','user002',4),
							('user003', 'user003',4)
							GO


------3. Category -- *done
CREATE TABLE Category(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100) NOT NULL DEFAULT 'Regular')
	GO 
---------------------------------------------------------------------
INSERT INTO Category VALUES ('English Book'), ('Comic Book'), ('Computer Books')
GO


------- 7. Shop --*dogin -- done model. -- Ân Trần chuyển từ số 7 lên trên này.
CREATE TABLE Shop(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100),
	address NVARCHAR(100),
	phone NVARCHAR(15),
	email NVARCHAR(30),
	avatar IMAGE,
	info NVARCHAR(MAX))
	GO
-----------------------------------
INSERT INTO Shop VALUES ('GHAT2', '23- ChuSoDo Shigadama', '02342342342', 'ghat2@gmail.com','','Make you found on me')
GO


-----4. Type -- *done
CREATE TABLE Types(
	id INT IDENTITY NOT NULL PRIMARY KEY,
	idCategory INT,
	name NVARCHAR(100) NOT NULL DEFAULT 'Regular',
	FOREIGN KEY (idCategory) REFERENCES Category(id))
GO
---------------------------------------------------------------------------
INSERT INTO Types VALUES (1, 'Listening book'), (1, 'Speaking book'), (1,'Bilingual book'), (1,'Dictionary'),
						(2, 'Anime'), (2, 'Thriller Book'), (2,'Fiction'), (2,'Short story'),
						(3, 'Programming for Beginners'), (3,'Scratch code'),  (3,'Android programming')
GO
------- 9. Voucher --*doing --done model
CREATE TABLE Voucher(
	id INT IDENTITY (1,1) PRIMARY KEY,
	name NVARCHAR(100) NOT NULL DEFAULT 'Annual promotions',
	startDate DATETIME NOT NULL ,
	endDate DATETIME NOT NULL,
	detail NVARCHAR(MAX),
	discount decimal(3,2) NOT NULL DEFAULT 0)
GO

------5. Product --*doing --done Model.
CREATE TABLE Product(
	id INT IDENTITY PRIMARY KEY,
	idShop INT,					--???c thêm b?i Ân Tr?n (26/10/2020) -- ?ã s?a insert.
	idType INT,
	name NVARCHAR(100) NOT NULL,
	price FLOAT NOT NULL,
	quantity INT NOT NULL DEFAULT 0,
	author NVARCHAR(100) NOT NULL DEFAULT 'Collection',
	description NVARCHAR(MAX),
	quantitySold INT NOT NULL DEFAULT 0,
	publisher NVARCHAR(100),
	rating float NOT NULL DEFAULT 3.0,
	score INT NOT NULL DEFAULT 0,
	idVoucher INT NOT NULL DEFAULT 0,				-------Thêm ph?n discount cho m?i s?n ph?m ---???c thêm b?i Hoàng Nguy?n (24/11/2020) 
	FOREIGN KEY (idShop) REFERENCES dbo.Shop(id),
	FOREIGN KEY (idType) REFERENCES Types(id)
	
)
GO

INSERT INTO Product VALUES (1, 1, 'Listening band 3.5 to 4.5',79000, 20,'Hyrichy Adobe', '	Our unique self-paced approach will help you build competence and 
											confidence in your programming skills. And Python is the best language ever for learning how to 
											program because of its simplicity and breadth...two features that are hard to 
											find in a single language',4, 'KMTC Publisher',4.6, 300,0),
						(1, 1, 'Listening band 7.0 to 9.0',79000,21,'Hyrichy Adobe', 'Our unique self-paced approach will help you build competence and 
											confidence in your programming skills. And Python is the best language ever for learning how to 
											program because of its simplicity and breadth...two features that are hard to 
											find in a single language',5, 'Starz Entertainment',3.6, 2550,0),
						(1, 2, 'Main meaning for Speaking Ielts test',79000,36,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',75, 'KMTC Publisher',4.9, 536,0),
						(1, 2, 'Speaking band 4.5 to 6.0',79000,122,'Romy Hausmann','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',50, 'Starz Entertainment',4.7, 366,0), 
						(1, 2, 'Suggesting in Speaking Ielts test',35000,35,'Romy Hausmann','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',13, 'KMTC Publisher',4.0, 364,0),
						(1, 3, 'The Foot boook',25000,1,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',55, 'KMTC Publisher',3.9,155,1),
						(1, 3,'Collins easy learning Italia words',155000,86,'Catherine Gildiner','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',44, 'KMTC Publisher',3.6, 233,2),
						(1, 4,'Oxford Dictionary 10000 word',86000,74,'Romy Hausmann','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',2, 'KMTC Publisher',1.5, 100,1),
						(1, 4,'Oxford Dictionary essential word',126000,66,'Romy Hausmann','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',5, 'KMTC Publisher',3.0,20,0),
						(1, 5,'Your Name',195000,22,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',88, 'KMTC Publisher',4.3, 400,0),
						(1, 5,'Sheep or ship',255000,58,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',56, 'KMTC Publisher',4.6, 299,0),
						(1, 5,'How the  sheep turn to moon',155000,2,'Catherine Gildiner','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',120, 'Starz Entertainment',4.8, 355,0),
						(1, 6, 'Edogawa Conan Detective 1',169000,3,'Catherine Gildiner','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',22, 'KMTC Publisher',4.2, 315,0), 
						(1, 6, 'Edogawa Conan Detective 2',99000,8,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',89, 'KMTC Publisher',4.5, 465,1),
						(1, 7, 'Flower on the OldMan',109000,7,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',42, 'Starz Entertainment',4.5, 306,1),
						(1, 7, 'Secret make Secret Woman',126000,2,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',2, 'Kindle Edition',4.2, 300,0), 
						(1, 8, 'Adventure of Jack and his tiny friend',79000,9,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',6, 'KMTC Publisher',4.9, 335,2),
						(1, 8, 'Poenic of twenty zodiacs',39000,3,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',9, 'Sling TV LLC',4.8, 265,0),
						(1, 8, 'Helalulu in giant land 2',89000,2,'Sigrid Nunez','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',5, 'KMTC Publisher',4.6, 342,2), 
						(1, 9, 'How I start my ethusiastic with coding!',99000, 7,'Sigrid Nunez','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',2, 'Kindle Edition',5, 105,2), 
						(1, 9, 'Your first coding programming',129000,2,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',23, 'Sling TV LLC',4.2, 300,0), 
						(1, 10, 'What is scatch? How your children like this?',139000,3,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',88, 'Kindle Edition',4.2,262,0),
						(1, 10, 'Scratching version Math- a close friend',89000,9,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',55, 'Sling TV LLC',4.8, 368,0),
						(1, 10, 'Scatch and your life- efficient impact, get better in every day',55000,2,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',66, 'Starz Entertainment',4.2, 359,0), 
						(1, 11, 'Work with Java, first step to become Android Development',68000,9,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',15, 'Kindle Edition',4.8, 325,0), 
						(1, 11, 'Android with Android Studio',136000,65,'Hyrichy Adobe','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',64, 'Kindle Edition',4.6, 234,0), 
						(1, 11, 'UX design- better for your application',13000,64,'Sigrid Nunez','Christie Tates...writing displays a wonderful combination
											of clear and simple with sparkle and intelligence...[Group is] a compelling narrative
											that empowers readers to better understand their own lives.
											Booklist (starred review)',5, 'Kindle Edition',4.6, 364,0)
						
Go

--Create by Ân Trần, date: 26/10/2020 ===================================
--6.01. PhotoProduct. -- Ảnh của sản phẩm. --*doing --done Model.
CREATE TABLE PhotoProduct(
	id INT IDENTITY(1, 1) PRIMARY KEY,
	idProduct INT,
	photo IMAGE
    CONSTRAINT productPhoto FOREIGN KEY (idProduct) REFERENCES dbo.Product(id)
)
GO
			
--6. Thông tin Staff
CREATE TABLE Staff(
	id INT IDENTITY (1,1) PRIMARY KEY,
	idAccount INT UNIQUE NOT NULL,
	name NVARCHAR(100),
	avatar IMAGE,
	birthDay DATE NOT NULL DEFAULT '01-01-1990',
	gender NVARCHAR(30),
	address NVARCHAR(100),
	phone NVARCHAR(15),
	email NVARCHAR(30),
	idMan INT
)
GO
ALTER TABLE dbo.Staff ADD CONSTRAINT PK_Staff_Staff FOREIGN KEY (idMan) REFERENCES dbo.Staff(id)	
INSERT INTO dbo.Staff
        (idAccount, name, avatar, birthDay, gender, address, phone, email, idMan)
VALUES (1,'Jack Handy', '', '2-2-2000', 'Male', '123- Andrew, Himston', '0123456789', 'jackcm@gmail.com', 1),
							(2,'Mark Zuckerberg', '','1-13-2002', 'Male', '63- Wales, Royal Navy', '0333875398', 'markzkb@gmail.com', 1)
GO	
-------7. Customer --*doing -- done Model
CREATE TABLE Customer(
	id INT IDENTITY (1,1) PRIMARY KEY,
	idAccount INT UNIQUE NOT NULL,
	name NVARCHAR(100),
	avatar IMAGE,
	birthDay DATE NOT NULL DEFAULT '1-1-1990',
	gender NVARCHAR(30),
	address NVARCHAR(100),
	phone NVARCHAR(15),
	email NVARCHAR(30),
	CONSTRAINT accCus FOREIGN KEY (idAccount) REFERENCES Account(id))
	GO
-------------------------------------------------------------------------------------
							 

-------8. Cart --*doing -- done model.

CREATE TABLE Cart(
	idCustomer INT  ,
	idProduct INT,
	idShop INT,
	quantity INT,
	CONSTRAINT useCart FOREIGN KEY (idCustomer) REFERENCES Customer(id),
	CONSTRAINT proCart FOREIGN KEY (idProduct) REFERENCES Product(id),
	CONSTRAINT shopping PRIMARY KEY (idCustomer, idProduct)
)

------- 10. Comment --*doing --done model
CREATE TABLE Comment(
	id INT IDENTITY PRIMARY KEY,
	idProduct INT NOT NULL,
	idCustomer INT NOT NULL,
	idShop INT NOT NULL,
	datePosting DATETIME  NOT NULL DEFAULT GETDATE(),
	content NVARCHAR(MAX), --=====================Dữ liệu này chuyển thành Max - nếu muốn giới hạn kí tự có thể bổ sung bên JS. Cập nhật thêm photo nếu cần thiết.
	rating INT --================================Bổ sung bởi Ân Trần ngày: 26/10/2020 -- Rate từ 1 đến 5
	CONSTRAINT proComment FOREIGN KEY (idProduct) REFERENCES Product(id),
	CONSTRAINT cusComment FOREIGN KEY (idCustomer) REFERENCES Customer(id),
	CONSTRAINT shopComment FOREIGN KEY (idShop) REFERENCES Shop(id))

	GO
    
---------- 11.	Delivery: Các phương thức chuyển hàng. -- *doing --done Model.
CREATE TABLE Delivery(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100) NOT NULL,
	feeShip FLOAT NOT NULL)
	GO
-----------------------------------------------------
INSERT INTO Delivery VALUES ('DHL Express', 170000), ('Tobo Delivery', 20000), ('GHN fast delivery',22000)
GO

------- 12. Payment --*doing --done model
CREATE TABLE Payment(
	id INT IDENTITY PRIMARY KEY,
	name NVARCHAR(100) NOT NULL)

	GO
-------------------------------------------------------
INSERT INTO Payment VALUES ('cash'), ('VISA CARD'), ('Momo mobile money')
GO

------ 13. Bill -- *doing -- done model.
	CREATE TABLE Bill(
		id INT IDENTITY PRIMARY KEY,
		idCustomer INT NOT NULL,
		idDelivery INT NOT NULL,
		idPayment INT NOT NULL,
		idVoucher INT DEFAULT 0,
		addressReceive NVARCHAR(100) NOT NULL,
		phone NVARCHAR(15),
		dateConfirm DATETIME NOT NULL DEFAULT GETDATE(),
		dateReceive DATE NOT NULL DEFAULT GETDATE() + 4, --Thay đổi 2 thành 4. (Số ngày này có thể do Code thiết lập để phù hợp với khoảng cách).
		feeShip float NOT NULL DEFAULT 20000,
		totalCost FLOAT NOT NULL DEFAULT 20000
		CONSTRAINT cusBill FOREIGN KEY (idCustomer) REFERENCES dbo.Customer(id),
		CONSTRAINT delBill FOREIGN KEY (idDelivery) REFERENCES  Delivery(id),
		CONSTRAINT payBill FOREIGN KEY (idPayment) REFERENCES Payment(id)
	 )

----------------------------------------
GO
--14.1 -- StateBill
CREATE TABLE StateBill
(
	id INT IDENTITY PRIMARY KEY,
	state NVARCHAR(50)
)
GO
INSERT INTO dbo.StateBill
        ( state )
VALUES  ( N'unapproved'), (N'approved'), (N'cancelled'), (N'shipping'), (N'complete'), (N'return')
GO 


-------- 14. BillDetail --*doing
CREATE TABLE BillDetail(
		id INT IDENTITY PRIMARY KEY,
		idBill INT NOT NULL,
		idProduct INT NOT NULL,
		idStateBill INT REFERENCES dbo.StateBill(id),
		timeState DATETIME DEFAULT GETDATE(),
		prices FLOAT NOT NULL,
		quantity INT NOT NULL DEFAULT 1,
		CONSTRAINT bilDetail FOREIGN KEY (idBill) REFERENCES Bill(id),
		CONSTRAINT proDetail FOREIGN KEY (idProduct) REFERENCES Product(id))
GO


--======================================================================================================================================= PROCEDURE.
--Bỏ dấu tiếng việt
CREATE FUNCTION [dbo].[non_unicode_convert](@inputVar NVARCHAR(MAX) )
RETURNS NVARCHAR(MAX)
AS
BEGIN    
    IF (@inputVar IS NULL OR @inputVar = '')  RETURN ''
   
    DECLARE @RT NVARCHAR(MAX)
    DECLARE @SIGN_CHARS NCHAR(256)
    DECLARE @UNSIGN_CHARS NCHAR (256)
 
    SET @SIGN_CHARS = N'ăâđêôơưàảãạáằẳẵặắầẩẫậấèẻẽẹéềểễệếìỉĩịíòỏõọóồổỗộốờởỡợớùủũụúừửữựứỳỷỹỵýĂÂĐÊÔƠƯÀẢÃẠÁẰẲẴẶẮẦẨẪẬẤÈẺẼẸÉỀỂỄỆẾÌỈĨỊÍÒỎÕỌÓỒỔỖỘỐỜỞỠỢỚÙỦŨỤÚỪỬỮỰỨỲỶỸỴÝ' + NCHAR(272) + NCHAR(208)
    SET @UNSIGN_CHARS = N'aadeoouaaaaaaaaaaaaaaaeeeeeeeeeeiiiiiooooooooooooooouuuuuuuuuuyyyyyAADEOOUAAAAAAAAAAAAAAAEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOUUUUUUUUUUYYYYYDD'
 
    DECLARE @COUNTER int
    DECLARE @COUNTER1 int
   
    SET @COUNTER = 1
    WHILE (@COUNTER <= LEN(@inputVar))
    BEGIN  
        SET @COUNTER1 = 1
        WHILE (@COUNTER1 <= LEN(@SIGN_CHARS) + 1)
        BEGIN
            IF UNICODE(SUBSTRING(@SIGN_CHARS, @COUNTER1,1)) = UNICODE(SUBSTRING(@inputVar,@COUNTER ,1))
            BEGIN          
                IF @COUNTER = 1
                    SET @inputVar = SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) + SUBSTRING(@inputVar, @COUNTER+1,LEN(@inputVar)-1)      
                ELSE
                    SET @inputVar = SUBSTRING(@inputVar, 1, @COUNTER-1) +SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1) + SUBSTRING(@inputVar, @COUNTER+1,LEN(@inputVar)- @COUNTER)
                BREAK
            END
            SET @COUNTER1 = @COUNTER1 +1
        END
        SET @COUNTER = @COUNTER +1
    END
    -- SET @inputVar = replace(@inputVar,' ','-')
    RETURN @inputVar
END
GO
-- Tạo procedure cho tìm kiếm tài khoản thông qua username. (Thay vì hiển thị idPremission thì sẽ hiển thị tên của Permission trong bảng).
-- Create by Ân Trần - 26/10/2020
-- Lấy thông tin đăng nhập
CREATE PROC USP_GetInfoLogin
@username NVARCHAR(30), @password NVARCHAR(30)
AS
BEGIN
    SELECT sta.id, sta.idAccount, sta.name, sta.birthDay, sta.gender, sta.address, sta.phone, sta.email, acc.idPermission, per.permission
	FROM dbo.Staff AS sta, dbo.Account AS acc, dbo.Permissions AS per
	WHERE sta.idAccount = acc.id AND acc.idPermission = per.id
	AND acc.userName = @username AND acc.passWord = @password
	UNION
	SELECT cus.id, cus.idAccount, cus.name, cus.birthDay, cus.gender, cus.address, cus.phone, cus.email, acc.idPermission, per.permission
	FROM dbo.Customer AS cus, dbo.Account AS acc, dbo.Permissions AS per
	WHERE cus.idAccount = acc.id AND acc.idPermission = per.id
	AND acc.userName = @username AND acc.passWord = @password
END
GO

-- Lấy thông tin tài khoản theo usernam
CREATE PROC USP_SearchAccountByUserName
@userName NVARCHAR(100)
AS
BEGIN
    SELECT Account.id, userName, passWord, permission
	FROM dbo.Account INNER JOIN dbo.Permissions ON Permissions.id = Account.idPermission
	WHERE userName = @userName
END
GO

CREATE PROC USP_GetAccountsByIDPermission
@idPermission INT
AS
BEGIN
    SELECT Account.id, userName, passWord, permission
	FROM dbo.Account INNER JOIN dbo.Permissions ON Permissions.id = Account.idPermission
	WHERE idPermission = @idPermission
	ORDER BY id
END
GO

-- Thêm khách hàng mới
CREATE PROC USP_AddCustomer
@userName NVARCHAR(30), @passWord NVARCHAR(30), @name NVARCHAR(30), @birthDay DATE, @gender NVARCHAR(30), @address NVARCHAR(MAX), @phone NVARCHAR(30), @email NVARCHAR(30)
AS
BEGIN
    DECLARE @maxIDAccount INT
	INSERT INTO dbo.Account ( userName, password, idPermission ) VALUES  (@userName, @passWord, 4)
	SELECT @maxIDAccount = MAX(id) FROM dbo.Account
	INSERT INTO dbo.Customer (idAccount, name, birthDay, gender, address, phone, email)
	VALUES (@maxIDAccount, @name, @birthDay, @gender, @address, @phone, @email)
END
GO


--Thêm nhân viên mới
CREATE PROC USP_AddStaff
@userName NVARCHAR(30), @passWord NVARCHAR(30), @idPermission int,  @name NVARCHAR(30), @avatar IMAGE,
 @birthDay DATE, @gender NVARCHAR(30), @address NVARCHAR(MAX), @phone NVARCHAR(30), @email NVARCHAR(30), @idMan INT
AS
BEGIN
    DECLARE @maxIDAccount INT
	INSERT INTO dbo.Account ( userName, password, idPermission ) VALUES  (@userName, @passWord, @idPermission)
	SELECT @maxIDAccount = MAX(id) FROM dbo.Account
	INSERT INTO dbo.Staff(idAccount, name, avatar, birthDay, gender, address, phone, email, idMan)
	VALUES (@maxIDAccount, @name, @avatar, @birthDay, @gender, @address, @phone, @email, @idMan)
END
GO

--Cập nhật tài khoản bằng email
CREATE PROC USP_UpdatePassWordByEmail
@password NVARCHAR(30), @email NVARCHAR(30)
AS
BEGIN
	UPDATE dbo.Account SET password = @password WHERE id IN (SELECT idAccount FROM dbo.Customer WHERE email = @email) 
END
GO

-- Lấy danh sách các đơn hàng (shipper dùng để kiểm tra và giao hàng)
CREATE PROC USP_GetListTransport
AS
BEGIN
    SELECT bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
	FROM dbo.Bill AS bi, dbo.Customer AS cus, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.idCustomer = cus.id AND bi.id = bid.idBill AND bid.idStateBill = sb.id AND  sb.state != N'unapproved' AND sb.state != N'cancelled' 
	GROUP BY bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
END
GO

-- Lấy danh sách các đơn hàng đã giao (shipper dùng để kiểm tra và giao hàng)
CREATE PROC USP_GetListTransportByState
@state NVARCHAR(30)
AS
BEGIN
    SELECT bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
	FROM dbo.Bill AS bi, dbo.Customer AS cus, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.idCustomer = cus.id AND bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = @state
	GROUP BY bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
END
GO
--Cập nhật trạng thái đơn hàng qua idBill
CREATE PROC USP_UpdateOrderStateByIdBill
@idBill INT, @state NVARCHAR(30)
AS
BEGIN
	DECLARE @idState INT
	SELECT @idState = id FROM StateBill WHERE state = @state
    UPDATE dbo.BillDetail SET idStateBill = @idState WHERE idBill = @idBill
END
GO

--Lấy chi tiết đơn hàng vận chuyển
CREATE PROC USP_GetTransportDetailByIDBill
@idBill INT
AS
BEGIN
    SELECT bid.idProduct, pro.name, bid.prices, bid.quantity, bid.idStateBill
	FROM dbo.BillDetail AS bid, dbo.Product AS pro
	WHERE bid.idProduct = pro.id AND bid.idBill = @idBill
END
GO

--Lấy dữ liệu đơn hàng vận chuyển theo mã đơn hàng và trạng thái đơn hàng
CREATE PROC USP_GetTransportByIDBillAndState
@idBill INT, @state NVARCHAR(30)
AS
BEGIN
	IF (@state != N'all')
	BEGIN
	    SELECT bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
		FROM dbo.Bill AS bi, dbo.Customer AS cus, dbo.BillDetail AS bid, dbo.StateBill AS sb
		WHERE bi.idCustomer = cus.id AND bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = @state AND bi.id = @idBill
		GROUP BY bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
	END
    ELSE
	BEGIN
	     SELECT bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
		FROM dbo.Bill AS bi, dbo.Customer AS cus, dbo.BillDetail AS bid, dbo.StateBill AS sb
		WHERE bi.idCustomer = cus.id AND bi.id = bid.idBill AND bi.id = @idBill AND bid.idStateBill = sb.id AND sb.state != N'unapproved' AND sb.state != N'cancelled' 
		GROUP BY bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
	END
END
GO


--Lấy dữ liệu full state cho order
CREATE PROC USP_GetOrderFullState
AS
BEGIN
     SELECT bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
	FROM dbo.Bill AS bi, dbo.Customer AS cus, dbo.BillDetail AS bid
	WHERE bi.idCustomer = cus.id AND bi.id = bid.idBill
	GROUP BY bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
END
GO

--Lấy dữ liệu khách hàng thông qua IDBill
CREATE PROC USP_GetInfoCustomerByIDBill
@idBill INT
AS
BEGIN
    SELECT bi.idCustomer, cus.name, bi.phone, cus.email, bi.addressReceive
	FROM dbo.Bill AS bi, dbo.Customer AS cus
	WHERE bi.idCustomer = cus.id AND bi.id = @idBill
END
GO

--Lấy thông tin Bill bằng idBill
CREATE PROC USP_GetInfoBillByIDBill
@idBill INT
AS
BEGIN
  SELECT idCustomer, idDelivery, idPayment, idVoucher, addressReceive, phone, dateConfirm, dateReceive, feeShip, totalCost FROM dbo.Bill WHERE id = @idBill
END
GO

--Tìm kiếm order qua IDBill
CREATE PROC USP_GetOrderByIDBill
@idBill INT
AS
BEGIN
     SELECT bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
	FROM dbo.Bill AS bi, dbo.Customer AS cus, dbo.BillDetail AS bid
	WHERE bi.idCustomer = cus.id AND bi.id = bid.idBill AND bi.id = @idBill
	GROUP BY bi.id, bi.dateConfirm, cus.name, bi.addressReceive, bi.phone, bi.totalCost, bid.idStateBill
END
GO


--Thống kê
--Function lấy ngày cuối cùng của tháng
CREATE FUNCTION FN_GetMaxDayOfMonth(@date DATE)
RETURNS DATE
AS
BEGIN
	DECLARE @lastDate DATE
	SELECT @lastDate = DATEADD(d,-1, DATEADD(mm, DATEDIFF(mm, 0 ,@date)+1, 0))
	RETURN @lastDate
END
GO

--Kiểm tra ngày có trong tháng đó không
CREATE FUNCTION FN_CheckDateInMonth(@timeState DATETIME, @date DATETIME)
RETURNS BIT
AS
BEGIN
    IF (CONVERT(DATE, @timeState) > CONVERT(DATE, DATEADD(m,-1,DATEADD(mm, DATEDIFF(m,0,@date), 0))) AND CONVERT(DATE, @timeState) <= CONVERT(DATE, dbo.FN_GetMaxDayOfMonth(@date)))
		RETURN 1
	RETURN 0
END
GO

--Thống kê doanh thu theo tuần 
CREATE PROC USP_GetRevenueReportOfWeek
@date DATETIME
AS
BEGIN
	DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	WITH tableRevenue AS(
	SELECT bid.timeState AS [date], AVG(bi.totalCost - bi.feeShip) AS [revenue]
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' 
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY bid.timeState)
	SELECT CONVERT(DATE, tableRevenue.date) AS [date], SUM(tableRevenue.revenue) AS revenue
	FROM tableRevenue
	GROUP BY CONVERT(DATE, tableRevenue.date)
END
GO

--Thống kê doanh thu theo tháng  
CREATE PROC USP_GetRevenueReportOfMonth
@date DATETIME
AS
BEGIN
	WITH tableRevenue AS(
	SELECT bid.timeState AS [date], AVG(bi.totalCost - bi.feeShip) AS [revenue]
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' 
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY bid.timeState)
	SELECT CONVERT(DATE, tableRevenue.date) AS [date], SUM(tableRevenue.revenue) AS revenue
	FROM tableRevenue
	GROUP BY CONVERT(DATE, tableRevenue.date)
END
GO
 
--Tổng doanh thu trong tuần
CREATE PROC USP_GetTotalRevenueReportOfWeek
@date DATETIME
AS
BEGIN
	DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	WITH tableRevenue AS(
	SELECT bid.timeState AS [date], AVG(bi.totalCost - bi.feeShip) AS [revenue]
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' 
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY bid.timeState)
	SELECT SUM(tableRevenue.revenue) AS totalRevenue
	FROM tableRevenue
END
GO

--Tổng doanh thu trong tháng
CREATE PROC USP_GetTotalRevenueReportOfMonth
@date DATETIME
AS
BEGIN
	WITH tableRevenue AS(
	SELECT bid.timeState AS [date], AVG(bi.totalCost - bi.feeShip) AS [revenue]
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' 
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY bid.timeState)
	SELECT SUM(tableRevenue.revenue) AS totalRevenue
	FROM tableRevenue
END
GO
 
-- Thống kê chi tiết doanh thu

--Số đơn hàng của tuần
CREATE PROC USP_GetOrderNumberOfWeek
@date DATETIME
AS
BEGIN
    DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	WITH tableRevenue AS(
	SELECT bid.timeState AS [date]
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY bid.timeState)
	SELECT COUNT([date]) AS orderNumber 
	FROM tableRevenue
END
GO

--Số đơn hàng của tháng
CREATE PROC USP_GetOrderNumberOfMonth
@date DATETIME
AS
BEGIN
	WITH tableRevenue AS(
	SELECT bid.timeState AS [date]
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY bid.timeState)
	SELECT COUNT([date]) AS orderNumber 
	FROM tableRevenue
END
GO

--Số khách mua hàng của tuần
CREATE PROC USP_GetCustomerNumberOfWeek
@date DATETIME
AS
BEGIN
    DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	WITH tableRevenue AS(
	SELECT bi.idCustomer AS cus
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY  bi.idCustomer)
	SELECT COUNT(cus) AS customerNumber 
	FROM tableRevenue
END
GO

--Số khách mua hàng của tháng
CREATE PROC USP_GetCustomerNumberOfMonth
@date DATETIME
AS
BEGIN
	WITH tableRevenue AS(
	SELECT bi.idCustomer AS cus
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY  bi.idCustomer)
	SELECT COUNT(cus) AS customerNumber 
	FROM tableRevenue
END
GO


--Số đơn hoàn thành theo tuần
CREATE PROC USP_GetCompleteNumberOfWeek
@date DATETIME
AS
BEGIN
    DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	WITH tableRevenue AS(
	SELECT bid.idBill AS bil
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' 
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY  bid.idBill)
	SELECT COUNT(bil) AS completeNumber 
	FROM tableRevenue
END
GO

--Số đơn hoàn thành theo tháng
CREATE PROC USP_GetCompleteNumberOfMonth
@date DATETIME
AS
BEGIN
	WITH tableRevenue AS(
	SELECT bid.idBill AS bil
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' 
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY  bid.idBill)
	SELECT COUNT(bil) AS completeNumber 
	FROM tableRevenue
END
GO

--Số đơn trả theo tuần
CREATE PROC USP_GetReturnNumberOfWeek
@date DATETIME
AS
BEGIN
    DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	WITH tableRevenue AS(
	SELECT bid.idBill AS bil
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'return' 
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY  bid.idBill)
	SELECT COUNT(bil) AS returnNumber 
	FROM tableRevenue
END
GO

--Số đơn trả theo tháng
CREATE PROC USP_GetReturnNumberOfMonth
@date DATETIME
AS
BEGIN
	WITH tableRevenue AS(
	SELECT bid.idBill AS bil
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'return' 
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY  bid.idBill)
	SELECT COUNT(bil) AS returnNumber 
	FROM tableRevenue
END
GO

--Số đơn hủy theo tuần
CREATE PROC USP_GetCancelledNumberOfWeek
@date DATETIME
AS
BEGIN
    DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	WITH tableRevenue AS(
	SELECT bid.idBill AS bil
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'cancelled' 
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY  bid.idBill)
	SELECT COUNT(bil) AS cancelledNumber 
	FROM tableRevenue
END
GO

--Số đơn hủy theo tháng
CREATE PROC USP_GetCancelledNumberOfMonth
@date DATETIME
AS
BEGIN
	WITH tableRevenue AS(
	SELECT bid.idBill AS bil
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'cancelled' 
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY  bid.idBill)
	SELECT COUNT(bil) AS cancelledNumber 
	FROM tableRevenue
END
GO

--Danh sách sản phảm bán chạy theo tuần
CREATE PROC USP_GetSellingProductsOfWeek
@date DATETIME
AS
BEGIN
    DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	SELECT TOP(7) bid.idProduct, pr.name, ca.name AS category, SUM(bid.quantity) AS totalQuantity
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb, dbo.Product AS pr, dbo.Types AS tp, dbo.Category AS ca
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' AND bid.idProduct = pr.id AND pr.idType = tp.id AND tp.idCategory = ca.id
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY  bid.idProduct, pr.name, ca.name
	ORDER BY totalQuantity DESC
END
GO


--Danh sách sản phảm bán chạy theo tháng
CREATE PROC USP_GetSellingProductsOfMonth
@date DATETIME
AS
BEGIN
	SELECT TOP(7) bid.idProduct, pr.name, ca.name AS category, SUM(bid.quantity) AS totalQuantity
	FROM dbo.Bill AS bi, dbo.BillDetail AS bid, dbo.StateBill AS sb, dbo.Product AS pr, dbo.Types AS tp, dbo.Category AS ca
	WHERE bi.id = bid.idBill AND bid.idStateBill = sb.id AND sb.state = 'complete' AND bid.idProduct = pr.id AND pr.idType = tp.id AND tp.idCategory = ca.id
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY  bid.idProduct, pr.name, ca.name
	ORDER BY totalQuantity DESC
END
GO

-- Thống kê khách hàng
CREATE PROC USP_GetCustomersByMostOfWeek	
@date DATETIME
AS
BEGIN
	DECLARE @dayOfWeek INT
	SELECT @dayOfWeek = DATEPART(dw, @date);
	DECLARE @BillDetailTemp TABLE(idBill INT, idStateBill INT, timeState DATETIME)
	INSERT INTO @BillDetailTemp SELECT idBill, idStateBill, timeState FROM dbo.BillDetail GROUP BY idBill, idStateBill, timeState
    SELECT TOP(10) cus.id, cus.name, bi.phone, cus.email, COUNT(*) AS orderNumber, (SUM(bi.totalCost) - SUM(bi.feeShip)) AS spending
	FROM dbo.Bill AS bi, @BillDetailTemp AS bid, dbo.Customer AS cus, dbo.StateBill AS sbi
	WHERE bi.id = bid.idBill AND bi.idCustomer = cus.id AND bid.idStateBill = sbi.id AND sbi.state = 'complete'
	AND CONVERT(DATE, bid.timeState) > CONVERT(DATE, DATEADD(DAY, - @dayOfWeek, @date)) AND CONVERT(DATE, bid.timeState) <= CONVERT(DATE, DATEADD(DAY, 7 - @dayOfWeek, @date))
	GROUP BY cus.id, cus.name, bi.phone, cus.email
	ORDER BY spending DESC
END
GO

-- Thống kê khách hàng
CREATE PROC USP_GetCustomersByMostOfMonth
@date DATETIME
AS
BEGIN
	DECLARE @BillDetailTemp TABLE(idBill INT, idStateBill INT, timeState DATETIME)
	INSERT INTO @BillDetailTemp SELECT idBill, idStateBill, timeState FROM dbo.BillDetail GROUP BY idBill, idStateBill, timeState
    SELECT TOP(10) cus.id, cus.name, bi.phone, cus.email, COUNT(*) AS orderNumber, (SUM(bi.totalCost) - SUM(bi.feeShip)) AS spending
	FROM dbo.Bill AS bi, @BillDetailTemp AS bid, dbo.Customer AS cus, dbo.StateBill AS sbi
	WHERE bi.id = bid.idBill AND bi.idCustomer = cus.id AND bid.idStateBill = sbi.id AND sbi.state = 'complete'
	AND dbo.FN_CheckDateInMonth(bid.timeState, @date) = 1
	GROUP BY cus.id, cus.name, bi.phone, cus.email
	ORDER BY spending DESC
END
GO

--Lấy đẩy đủ thông tin Staff bằng id
CREATE PROC USP_GetFullInfoStaffByID
@id INT
AS
BEGIN
    SELECT st.id, st.idAccount, st.name, st.avatar, st.birthDay, st.gender, st.address, st.phone, st.email, st.idMan, per.permission
	FROM dbo.Staff AS st, dbo.Account AS acc, dbo.Permissions AS per
	WHERE st.idAccount = acc.id AND acc.idPermission = per.id AND st.id = @id
END
GO

CREATE PROC USP_UpdateInfoStaffByID
@id INT, @name NVARCHAR(100), @avatar IMAGE, @birthDay DATE, @gender NVARCHAR(30), @address NVARCHAR(MAX), @phone NVARCHAR(30), @email NVARCHAR(100), @permission NVARCHAR(30)
AS
BEGIN
	BEGIN TRAN
	BEGIN TRY
		DECLARE @idPermission INT
		SELECT @idPermission = id FROM dbo.Permissions WHERE permission = @permission
		DECLARE @idAccount INT
		SELECT @idAccount = idAccount FROM dbo.Staff WHERE id = @id
		UPDATE dbo.Account SET idPermission = @idPermission WHERE id = @idAccount
		UPDATE dbo.Staff SET name = @name, avatar = @avatar, birthDay = @birthDay, gender = @gender, address = @address, phone = @phone,
		email = @email WHERE id = @id
		COMMIT
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN
	END CATCH
END
GO

-- Lấy danh sách tài khoản staff
CREATE PROC USP_GetAccountManagermentOfStaff
AS
BEGIN
	SELECT sta.id, acc.userName, sta.name, sta.avatar, sta.birthDay, sta.gender, sta.address, sta.phone, sta.email, per.permission
	FROM dbo.Permissions AS per, dbo.Account AS acc, dbo.Staff AS sta
	WHERE acc.idPermission = per.id AND acc.id = sta.idAccount
END
GO

-- Tìm kiếm tài khoản theo từ khóa
CREATE PROC USP_GetAccountManagermentOfStaffByKeyword
@keyword NVARCHAR(100)
AS
BEGIN
  SELECT sta.id, acc.userName, sta.name, sta.avatar, sta.birthDay, sta.gender, sta.address, sta.phone, sta.email, per.permission
  FROM dbo.Permissions AS per, dbo.Account AS acc, dbo.Staff AS sta 
  WHERE acc.idPermission = per.id AND acc.id = sta.idAccount
  AND CONCAT(sta.id, acc.userName, dbo.non_unicode_convert(sta.name), sta.phone, sta.email) LIKE N'%' + dbo.non_unicode_convert(@keyword) + '%'
END
GO


-- Lấy quyền bằng userid
CREATE PROC USP_GetPermissionByIDStaff
@idStaff INT
AS
BEGIN
	SELECT per.id, per.permission
	FROM dbo.Staff AS st, dbo.Account AS acc, dbo.Permissions AS per
	WHERE st.idAccount = acc.id AND acc.idPermission = per.id AND st.id = @idStaff
END
GO
--Kiểm tra tồn tại email
CREATE PROC USP_IsExistEmail
@email NVARCHAR(100)
AS
BEGIN
    SELECT email FROM dbo.Staff
	WHERE email = @email
	UNION
	SELECT email FROM dbo.Customer
	WHERE email = @email
END
GO

--======================================================================================================================================= TRIGGER.
--Cập nhật thời gian BillDetail khhi state Billdetail cập nhật
CREATE TRIGGER TG_UpdateTimeState
ON dbo.BillDetail
AFTER UPDATE, INSERT
AS
BEGIN
	DECLARE @idBill INT
	SELECT @idBill = ins.idBill FROM Inserted AS ins, Deleted AS del WHERE ins.id = del.id   
	UPDATE dbo.BillDetail SET timeState = GETDATE() WHERE idBill = @idBill;
END
GO



--===================================================================================
--======================================================================================================================================= PROCEDURE.
-- Create by Tiên Giang 21/12/2020
-- Thêm sản phẩm
CREATE PROC USP_AddProduct
@idshop INT, @idtype INT, @name NVARCHAR(100), @price FLOAT, @quantity INT, @author NVARCHAR(100), @description NVARCHAR(MAX),  @publisher NVARCHAR(100)
AS
BEGIN
	INSERT INTO dbo.Product (idShop, idType, name, price, quantity, author, description, publisher) 
	VALUES (@idshop, @idtype, @name, @price, @quantity, @author, @description, @publisher)
END
GO

-- Tìm kiếm tên sản phẩm và tên loại sản phẩm
CREATE PROC USP_SearchProduct @search NVARCHAR(MAX)
AS
BEGIN 
	SELECT dbo.Product.id, dbo.Types.name as name_types, dbo.Product.name, price, quantity, quantitySold,score 
	FROM Product, Types
	WHERE Product.idType = Types.id AND (Product.name like '%' + @search + '%' OR dbo.Types.name like '%' + @search + '%' ) AND quantity <> -1
END
GO

-- Create 11/01/2021
CREATE PROC USP_GetInfoOneProduct @idproduct INT
AS
BEGIN
	SELECT Product.id, Product.name, idType, Types.name as name_types, price, quantity, author, description, publisher
	FROM Product, Types
	WHERE Product.idType = Types.id AND Product.id = @idproduct
END
GO

-- Sửa thông tin sp
CREATE PROC USP_UpdateProduct
@id INT, @idshop INT, @idtype INT, @name NVARCHAR(100), @price FLOAT, @quantity INT, @author NVARCHAR(100), @description NVARCHAR(MAX),  @publisher NVARCHAR(100)
AS
BEGIN
	UPDATE Product
	SET idShop = @idshop, idType = @idtype, Product.name = @name, price = @price, quantity = @quantity, author = @author, description = @description, publisher = @publisher
	WHERE id = @id
END
GO

CREATE PROC USP_GetInfoOneType @idtype INT
AS
BEGIN
	SELECT Types.id, Types.name, Category.name as name_cate, idCategory 
	FROM Types, Category 
	WHERE Types.idCategory = Category.id AND Types.id = @idtype
END
GO

-- Tìm kiếm tên, username, phone, email khách hàng
CREATE PROC USP_SearchCustomer @search NVARCHAR(MAX)
AS
BEGIN 
	SELECT Customer.id, Customer.name, Account.userName as username, email, phone, address
	FROM Customer, Account 
	WHERE Customer.idAccount = Account.id AND (Customer.name like '%' + @search + '%' OR dbo.Account.username like '%' + @search + '%' OR email like '%' + @search + '%' OR phone like '%' + @search + '%' OR address like '%' + @search + '%')
END
GO

-- Lấy thông tin đơn hàng của khách hàng theo ngày
CREATE PROCEDURE USP_GetInfoBillOfCusByDate @idCus INT, @date DATE
AS
BEGIN
	SELECT bi.id, SUM(bid.quantity) AS totalQuantity, bi.totalCost, bi.dateReceive, bi.dateConfirm
	FROM Bill as bi, BillDetail as bid
	WHERE bi.id = bid.idBill AND bi.idCustomer = @idCus AND bi.dateConfirm = @date
	GROUP BY bi.id, bi.totalCost, bi.dateReceive, bi.dateConfirm
	ORDER BY bi.dateConfirm DESC
END
GO

-- Lấy thông tin đơn theo khách
CREATE PROCEDURE USP_GetInfoBillOfCus @idCus INT 
AS
BEGIN
	SELECT bi.id, SUM(bid.quantity) AS totalQuantity, bi.totalCost, bi.dateReceive, bi.dateConfirm
	FROM Bill as bi, BillDetail as bid
	WHERE bi.id = bid.idBill AND bi.idCustomer = @idCus
	GROUP BY bi.id, bi.totalCost, bi.dateReceive, bi.dateConfirm
	ORDER BY bi.dateConfirm DESC
END
GO

-- Lấy thông tin chi tiết đơn hàng
CREATE PROCEDURE USP_GetInfoBillDetail @idBill INT 
AS
BEGIN
	SELECT bid.id, p.name as nameproduct, s.state as statebill, bid.prices, bid.quantity
	FROM Bill as bi, BillDetail as bid, Product as p, StateBill as s
	WHERE @idBill = bid.idBill AND bid.idProduct = p.id AND bid.idStateBill = s.id
	GROUP BY bid.id, p.name, s.state, bid.prices, bid.quantity
END
GO

--TRIGGER An bo sung
--Xóa ảnh trước khi xóa sản phẩm
CREATE TRIGGER TG_DeleteProduct
ON dbo.Product
FOR DELETE
AS
BEGIN
    DECLARE @idProduct INT
	SELECT @idProduct = Deleted.id FROM Deleted
	DELETE dbo.PhotoProduct WHERE idProduct = @idProduct
END
GO

--====================================================================================================
--Create by Hoàng 29/12/2020
-----Lấy danh sách top sản phẩm bán chạy nhât tính từ 30 ngày trước

CREATE PROC USP_GetBestSaler(@top int)

AS
BEGIN
	select Top(@top) id, idShop, idType, name, price, quantity, 
	author,description, quantitySold, publisher,rating, score, idVoucher  
		 from  (select sum(quantity) as amount ,idProduct
				from BillDetail, Bill, StateBill
				where BillDetail.idStateBill=StateBill.id and state= 'complete' and dateReceive> (GETDATE() -30) and BillDetail.idBill= Bill.id
				group by idProduct) as c, Product
		where Product.id= c.idProduct
		Order by c.amount DESC;
END
GO

-----Lấy danh sách top sản phẩm bán chạy nhât tính từ 30 ngày trước theo thể loại (Catogery)
CREATE PROC USP_GetBestSalerwithCato(@top int, @idCat int)
AS
BEGIN
	select Top(@top) id, idShop, idType, name, price, quantity, 
	author,description, quantitySold, publisher,rating, score, idVoucher  
		 from  (select sum(BillDetail.quantity) as amount ,idProduct
				from BillDetail, Bill, Types, Product, StateBill
				where BillDetail.idStateBill=StateBill.id and state= 'complete' and dateReceive> (GETDATE() -30) and BillDetail.idBill= Bill.id
				and BillDetail.idProduct= Product.id and Product.idType= Types.id and Types.idCategory= @idCat
				group by idProduct) as c, Product
		where Product.id= c.idProduct
		Order by c.amount DESC;
END
GO


----------Lấy thể loại sách được mua nhiều nhất------------ OK
create   PROC USP_GetListHotdeadProduct 
AS
BEGIN	
	begin
		--Using Cursor
		DECLARE @MyCursor CURSOR;
		--value for loop through
		DECLARE @ItemID int;
	    DECLARE @count int
		set @count =0

		DECLARE @id int, @name  nvarchar(100), @photo varbinary(max) , @price float, @quantity int,
				@idShop int, @idType int, @author nvarchar(100),@description varchar(100), 
					@publisher varchar(100),@rating int ,@score varchar(100), @quantitySold int, @idVoucher int,@discount numeric(3,2)

			DECLARE @tblResult table (id int, name  nvarchar(100), photo varbinary(max) , price float, quantity int,
				idShop int, idType int, author nvarchar(100),description varchar(100), 
					publisher varchar(100),rating int ,score varchar(100), quantitySold int, idVoucher int, discount numeric(3,2))
			BEGIN 
					SET @MyCursor = CURSOR FOR
					select  p0.id from Product p0 where p0.idVoucher !=0 
					OPEN @MyCursor
					FETCH NEXT FROM @MyCursor INTO @ItemID
					WHILE @@FETCH_STATUS = 0 and  @count <8
						BEGIN

						if (select p.idVoucher from Product p where p.id= @ItemID)>0
							begin
								(select top 1  @id= p.id, @name =concat(SUBSTRING(p.name,1,20),'...'), @photo=pho.photo , @price =p.price, @quantity= p.quantity ,
										@idShop= p.idShop, @idType =p.idType, @author =p.author,@description =p.description, 
										@publisher= p.publisher,@rating =p.rating ,@score= p.score, 
										@quantitySold =p.quantity, @idVoucher =p.idVoucher, @discount=v.discount
										from Product p, PhotoProduct pho, Voucher v
										where p.id= @itemID and p.id = pho.idProduct  and v.id= p.idVoucher
									)
							end
						else
							begin	
								(select top 1  @id= p.id, @name =concat(SUBSTRING(p.name,1,20),'...'), @photo=pho.photo , @price =p.price, @quantity= p.quantity ,
										@idShop= p.idShop, @idType =p.idType, @author =p.author,@description =p.description, 
										@publisher= p.publisher,@rating =p.rating ,@score= p.score, 
										@quantitySold =p.quantity, @idVoucher =p.idVoucher, @discount=0
										from Product p, PhotoProduct pho
										where p.id= @itemID and p.id = pho.idProduct  
									)
							end

							
						insert into @tblresult   values (@id, @name , @photo  , @price, @quantity ,
										@idShop, @idType, @author ,@description , 
										@publisher ,@rating ,@score , @quantitySold , @idVoucher,@discount)
							set  @count =@count+1
							FETCH NEXT FROM @MyCursor
									INTO @ItemID  
						END; 
					CLOSE @MyCursor ;
					DEALLOCATE @MyCursor;
				END;
				select * from @tblResult
		END
end

GO
-------Lấy phần trăm được giảm của một voucher
CREATE PROCEDURE USP_GetDiscountvalue (@idvoucher int)
AS
BEGIN
	select discount from Voucher where id=@idvoucher
END
GO

CREATE FUNCTION Fn_GetDiscountvalue (@idvoucher int)
returns int
as
begin 
	 return (select discount from Voucher where id=@idvoucher);
end
GO

------------Them vao bang Cart---------------------------
CREATE PROC InsertCart (@idCus int, @idProduct int, @idShop int,@quantity int)
AS
BEGIN
	INSERT INTO Cart VALUES (@idCus,@idProduct, @idShop, @quantity);
END
GO

------------Kiểm tra món hàng có trong giỏ hay chưa
CREATE PROC USP_Checkquantityproduct(@idCus int, @idProduct int)
AS
BEGIN
	if(Exists( SELECT quantity 
	FROM Cart
	WHERE idCustomer= @idCus and idProduct=@idProduct))
	Begin
		SELECT quantity 
		FROM Cart
		WHERE idCustomer= @idCus and idProduct=@idProduct
	end
	else
	select 0
	
END
GO

CREATE  proc USP_GetListProductByIDCus(@idCus int)
as BEGIN
		select  photo.photo, p.name, p.price, c.quantity  
		from Cart c, Product p, PhotoProduct photo 
				WHERE c.idCustomer = @idCus AND c.idProduct= p.id and photo.idProduct = p.id
	END
GO

--Run lại hàm vì có chỉnh sửa  (Hoàng) 22-1-2020

Create  proc USP_GetNewestBook(@top int)   
---------Lấy những sách mới thêm
as BEGIN

	begin
		--Using Cursor
		DECLARE @MyCursor CURSOR;
		--value for loop through
		DECLARE @ItemID int;
	    DECLARE @count int
		set @count =0

		DECLARE @id int, @name  nvarchar(100), @photo varbinary(max) , @price float, @quantity int,
				@idShop int, @idType int, @author nvarchar(100),@description varchar(100), 
					@publisher varchar(100),@rating int ,@score varchar(100), @quantitySold int, @idVoucher int

			DECLARE @tblResult table (id int, name  nvarchar(100), photo varbinary(max) , price float, quantity int,
				idShop int, idType int, author nvarchar(100),description varchar(100), 
					publisher varchar(100),rating int ,score varchar(100), quantitySold int, idVoucher int)
			BEGIN 
					SET @MyCursor = CURSOR FOR
					select  p0.id from Product p0 order by id desc
					OPEN @MyCursor
					FETCH NEXT FROM @MyCursor INTO @ItemID
					WHILE @@FETCH_STATUS = 0 and @count< @top
						BEGIN
							(select top 1  @id= p.id, @name =concat(SUBSTRING(p.name,1,20),'...'), @photo=pho.photo , @price =p.price, @quantity= p.quantity ,
										@idShop= p.idShop, @idType =p.idType, @author =p.author,@description =p.description, 
										@publisher= p.publisher,@rating =p.rating ,@score= p.score, @quantitySold =p.quantity, @idVoucher =p.idVoucher
										from Product p, PhotoProduct pho
										where p.id= @itemID and p.id = pho.idProduct and p.quantity>0
									)
							insert into @tblresult   values (@id, @name , @photo  , @price, @quantity ,
										@idShop, @idType, @author ,@description , 
										@publisher ,@rating ,@score , @quantitySold , @idVoucher)
							set  @count =@count+1
							FETCH NEXT FROM @MyCursor
									INTO @ItemID  
						END; 
					CLOSE @MyCursor ;
					DEALLOCATE @MyCursor;
				END;
				select * from @tblResult
		END
END
GO


-----Mới thêm (22-1-2020) -------------------------------------

create  proc USP_GetProductWithType(@id_type int)
as BEGIN
	begin
		--Using Cursor
		DECLARE @MyCursor CURSOR;
		--value for loop through
		DECLARE @ItemID int;
	    DECLARE @count int
		set @count =0

		DECLARE @id int, @name  nvarchar(100), @photo varbinary(max) , @price float, @quantity int,
				@idShop int, @idType int, @author nvarchar(100),@description varchar(100), 
					@publisher varchar(100),@rating int ,@score varchar(100), @quantitySold int, @idVoucher int

			DECLARE @tblResult table (id int, name  nvarchar(100), photo varbinary(max) , price float, quantity int,
				idShop int, idType int, author nvarchar(100),description varchar(100), 
					publisher varchar(100),rating int ,score varchar(100), quantitySold int, idVoucher int)
			BEGIN 
					SET @MyCursor = CURSOR FOR
					select  p0.id from Product p0 where p0.idType=@id_type
					OPEN @MyCursor
					FETCH NEXT FROM @MyCursor INTO @ItemID
					WHILE @@FETCH_STATUS = 0 
						BEGIN
							(select top 1  @id= p.id, @name =concat(SUBSTRING(p.name,1,27),'...'), @photo=pho.photo , @price =p.price, @quantity= p.quantity ,
										@idShop= p.idShop, @idType =p.idType, @author =p.author,@description =p.description, 
										@publisher= p.publisher,@rating =p.rating ,@score= p.score, @quantitySold =p.quantity, @idVoucher =p.idVoucher
										from Product p, PhotoProduct pho 
										where p.id= @itemID and p.id = pho.idProduct 
									)
							insert into @tblresult   values (@id, @name , @photo  , @price, @quantity ,
										@idShop, @idType, @author ,@description , 
										@publisher ,@rating ,@score , @quantitySold , @idVoucher)
							set  @count =@count+1
							FETCH NEXT FROM @MyCursor
									INTO @ItemID  
						END; 
					CLOSE @MyCursor ;
					DEALLOCATE @MyCursor;
				END;
				select * from @tblResult
		END

END
GO



--------------------------------------------QUERY CỦA THẢO NGUYỄN------------------------------
--------------------------------------------QUERY CỦA THẢO NGUYỄN------------------------------
--Thêm voucher
CREATE PROC USP_AddVoucher
@name nvarchar(100),
@startDate datetime,
@endDate datetime,
@detail nvarchar(max),
@discount FLOAT
AS
BEGIN
	INSERT INTO dbo.Voucher(name,startDate,endDate,detail,discount) VALUES (@name,@startDate,@endDate,@detail,@discount)
END
GO

--Cập nhật voucher
CREATE PROC USP_UpdateVoucher
@id int,
@name nvarchar(100),
@startDate datetime,
@endDate datetime,
@detail nvarchar(max),
@discount FLOAT
AS
BEGIN
	UPDATE dbo.Voucher SET name=@name,startDate=@startDate,endDate=@endDate,detail=@detail,discount=@discount
	WHERE id=@id
END
GO


--Xóa voucher
create PROC USP_DeleteVoucher
@id int
AS
BEGIN
	DELETE FROM dbo.Voucher WHERE id = @id
END
GO

--Tìm khuyến mãi
CREATE FUNCTION FN_SearchVoucher(@string NVARCHAR(50))
RETURNS TABLE
AS
RETURN
	SELECT* FROM dbo.Voucher V
	WHERE V.name LIKE '%'+ @string+'%'
	OR V.startDate LIKE '%'+ @string+'%'
	OR V.endDate LIKE '%'+ @string+'%'
	OR V.detail LIKE '%'+ @string+'%'
	OR V.discount LIKE '%'+ @string+'%'
go

--Lấy mô tả voucher theo id
CREATE PROC USP_GetDetailVoucherByID
@id int
AS
BEGIN
	SELECT name,detail FROM dbo.Voucher
	WHERE id=@id
END
GO

--Lấy thông tinvoucher theo id
CREATE PROC USP_GetInfoVoucherByID
@id int
AS
BEGIN
	SELECT * FROM dbo.Voucher
	WHERE id=@id
END
GO



--Lấy danh sách sản phẩm theo type và chưa có chương trình khuyến mãi
CREATE FUNCTION FN_getListProByType(@idtype int)
RETURNS TABLE
AS
RETURN
	SELECT* FROM dbo.Product P
	WHERE P.idVoucher=0
	AND P.idType=@idtype
	
go


--Lấy danh sách sản phẩm theo type và  có chương trình khuyến mãi
CREATE FUNCTION FN_getListProByTypeByVoucher(@idtype int,@idvoucher int)
RETURNS TABLE
AS
RETURN
	SELECT* FROM dbo.Product P
	WHERE P.idVoucher=@idvoucher
	AND P.idType=@idtype
	
go


----Cập nhật mã khuyến mãi của sản phẩm
CREATE PROC USP_UpdateProductVoucher
@id int,
@idvoucher int
AS
BEGIN
	
		UPDATE dbo.Product  SET idVoucher=@idvoucher
		WHERE id=@id 
END
GO

----xóa mã khuyến mãi của sản phẩm
CREATE PROC USP_DeleteProductVoucher
@id int
AS
BEGIN
	
		UPDATE dbo.Product  SET idVoucher=0
		WHERE id=@id 
END
GO

----xóa mã khuyến mãi của sản phẩm khi truyền id voucher
CREATE PROC USP_DeleteProductVoucher2
@idvoucher int
AS
BEGIN
	
		UPDATE dbo.Product  SET idVoucher=0
		WHERE idVoucher=@idvoucher
END
GO



--trigger khi xóa khuyến mãi thì idvoucher của sản phẩm = 0
create trigger tg_updateProduct on Voucher
after delete
as 
begin
	declare @id int
	select @id=id from deleted
	update Product
	set idVoucher=0
	where idVoucher=@id
end
go

--Tìm đơn vị vận chuyển theo tên hoặc phí
CREATE FUNCTION FN_SearchDelivery(@string NVARCHAR(50))
RETURNS TABLE
AS
RETURN
	SELECT* FROM dbo.Delivery D
	WHERE D.name LIKE '%'+ @string+'%'
	OR D.feeShip LIKE '%'+ @string+'%'
	
go

--Thêm đơn vị vận chuyển
CREATE PROC USP_AddDelivery
@name nvarchar(100),
@feeShip int

AS
BEGIN
	INSERT INTO dbo.Delivery(name,feeShip) VALUES (@name,@feeShip)
END
GO

--Cập nhật delivery
CREATE PROC USP_UpdateDelivery
@id int,
@name nvarchar(100),
@feeShip int
AS
BEGIN
	UPDATE dbo.Delivery SET name=@name, feeShip=@feeShip
	WHERE id=@id
END
go

--Lấy thông tin delivery theo id
CREATE PROC USP_GetInfoDeliveryByID
@id int
AS
BEGIN
	SELECT * FROM dbo.Delivery
	WHERE id=@id
END
GO


---update phí ship =-1 khi xóa đơn vị vận chuyển
CREATE PROC USP_UpdateFeeShipDelivery
@id int
AS
BEGIN
	UPDATE dbo.Delivery SET  feeShip=-1
	WHERE id=@id
END
go

---danh sách khách hàng theo tổng tiền mua giảm dần
CREATE VIEW VIEW_GetDataCustomerByMoney
AS
    SELECT C.id, C.name, Count(B.idCustomer) as totalBill, SUM(B.totalCost) as totalMoney
	FROM dbo.Customer C, dbo.Bill B
	WHERE C.id=B.idCustomer
	GROUP BY C.id,C.name
GO 

	

--------------------------------------------------------------------


--Lấy 38 kí tự đầu của tên sách
CREATE FUNCTION fn_get38characters (@string nvarchar(100))
returns nvarchar(38)
as
begin
   Declare @s nvarchar;
   set @s= SUBSTRING(@string,1,38);
   return @s
end
GO

--Lấy danh sách bill của một khách hàng
CREATE PROC USP_GetListBillOfAcus (@idCus int, @state nvarchar(50))
AS
BEGIN
if(@state='getall')
	Select *
	from Bill 
	where idCustomer=@idCus
else
	Select *
	from Bill as b
	where idCustomer=@idCus  and not exists(select idBill
											from BillDetail as bd, StateBill as s
											where state!=@state and b.id= bd.idBill and bd.idStateBill= s.id)
END
go


--Lấy danh sách sản phẩm của một đơn
CREATE PROC USP_GetAllProductOfBill (@idBill int)
as
BEGIN
	SELECT p.name, bid.quantity, s.state, bid.prices, ph.photo
	FROM BillDetail AS bid, Product as p , StateBill as s, PhotoProduct as ph
	WHERE bid.idBill =@idBill and bid.idProduct= p.id and bid.idStateBill= s.id and ph.idProduct= p.id
END
GO

-----Tìm kiếm sản phẩm
CREATE PROC USP_searchBill (@idCus int, @state nvarchar(50) , @condition int, @key nvarchar(Max))
as
BEGIN
if (@state='getall')
	begin		
		if(@condition=0)
		Select Distinct  *
		from Bill as b 
		where idCustomer=@idCus and exists( select * from BillDetail as t, Product
											where b.id= t.idBill and t.idProduct= Product.id and (select CHARINDEX(@key, Product.name))>0 )
									
		else
		if(@condition=1)
			Select *
			from Bill as b
			where idCustomer=@idCus and  (select CHARINDEX(@key,b.id))>0 
	
	end
else
	begin
		if(@condition=0)
		Select Distinct  *
		from Bill as b 
		where idCustomer=@idCus and exists( select * from BillDetail as t, Product
											where b.id= t.idBill and t.idProduct= Product.id and (select CHARINDEX(@key, Product.name))>0 )
									and not exists(select idBill
												from BillDetail as bd, StateBill as s 
												where state!=@state and b.id= bd.idBill and bd.idStateBill= s.id )
		else
		if(@condition=1)
			Select *
			from Bill as b
			where idCustomer=@idCus and (select CHARINDEX(@key,b.id))>0  and
										not exists(select idBill
													from BillDetail as bd, StateBill as s 
													where state!=@state and b.id= bd.idBill and bd.idStateBill= s.id )
	
	end
 
END
GO

---------------Mới thêm 22-1-2020


CREATE PROC USP_AddNewCustomer
@userName NVARCHAR(30), @passWord NVARCHAR(30), @name NVARCHAR(30), @birthDay DATE, @gender NVARCHAR(30), @address NVARCHAR(MAX), @phone NVARCHAR(30), @email NVARCHAR(30)
AS
BEGIN
    DECLARE @maxIDAccount INT
	INSERT INTO dbo.Account ( userName, password, idPermission ) VALUES  (@userName, @passWord, 4)
	SELECT @maxIDAccount = MAX(id) FROM dbo.Account
	INSERT INTO dbo.Customer (idAccount, name, birthDay, gender, address, phone, email)
	VALUES (@maxIDAccount, @name, @birthDay, @gender, @address, @phone, @email)
END
GO


-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------
---============================ Create by Thao Le_ for UI 
--1. Lấy các thông tin của một Bill
create procedure USP_infoBill(@idBill int)
as
	begin
		select b.id, c.name as userName, d.name as deliveryName, v.name as voucherName,
		p.name as paymentName,b.addressReceive,b.phone, b.dateConfirm, b.dateReceive, b.feeShip, b.totalCost from Bill b, Customer c, Delivery d, Payment p, Voucher v
				where  @idBill =b.id and b.idCustomer= c.id and b.idDelivery= d.id and b.idVoucher=v.id and b.idPayment =p.id
	END
GO

--2. Lấy các sản phẩm trong 1 bill
create PROCEDURE USP_getProductsInBill (@idBill int)
as
BEGIN
	select dt.idProduct, dt.idStateBill, dt.timeState, dt.prices, dt.quantity, p.name from BillDetail dt, Product p where dt.idBill = @idBill and p.id = dt.idProduct
end
GO

--3.  Hủy đơn hàng
create PROCEDURE USP_cancelBill (@idBill INT)
AS
	BEGIN
		update BillDetail  set idStateBill =3 where idBill= @idBill
	END
GO
--------------=========== Trang thanh toán
--4. Lấy voucher đang có
create PROCEDURE USP_getVoucher 
as
	begin
		select * from Voucher 
			where startDate <= getDate() and endDate >= getDate()
	END
GO

--5. Thêm các sản phẩm trong đơn hàng vào bill detail
create procedure USP_MatchInBillDetail  (@idCustomer int, @idProduct int) --- Update 2.1.2021
as
	BEGIN
			declare @price int,@quantity int , @idBill int
				
			Select Top 1  @idBill = b.id from Bill b WHERE b.idCustomer= @idCustomer ORDER by b.id desc
				
			select @quantity = c.quantity from Cart c WHERE c.idCustomer=@idCustomer and c.idProduct= @IdProduct
			select @price = p.price from Product p WHERE p.id=@idProduct
				
			INSERT INTO BillDetail values (@idBill, @IdProduct,1,dateadd(day,0, getdate()), @price,@quantity)
			DELETE from Cart  where idProduct = @idProduct and idCustomer= @idCustomer
				--Do some processing here
	END
GO

--6. Create Bill
--EXEC USP_create_Bill @idCus =2,@idDelivery =1,@idPayment= 1,@idVoucher =1,
							--@address= 'hdjhjsdhsds' , @phone ='02222',
							--@feeShip= 23000, @totalCost= 22000
			---=============
create proc USP_create_Bill(@idCus int,@idDelivery int,@idPayment int,@idVoucher int,
				@address nvarchar(100), @phone nvarchar(12),
				@feeShip float, @totalCost float)
	as
	BEGIN
		insert into Bill VALUES (@idCus,@idDelivery,@idPayment,@idVoucher,@address,
		@phone, GETDATE(),CAST (DATEADD(DAY,3,GETDATE())   AS DATE), @feeShip, @totalCost);
					 
	END
GO

--7. Lấy các sản phẩm khách hàng muốn mua hiện tại.
CREATE PROC USP_getSelectProduct (@idCus int ,@idPro int)
AS
BEGIN
	select pro.id,pro.name, pro.photo,pro.price,c.quantity from Cart c, (select p.id, ph.photo,p.name,p.price,p.rating FROM Product p left join PhotoProduct ph ON p.id= ph.idProduct) as pro
			WHERE c.idProduct=@idPro and c.idCustomer=@idCus and c.idProduct=pro.id 
END
GO
--================== Bổ sung procedureDetail Product
--8. Lấy thông tin sản phẩm bất kì
create PROC USP_GetProductById(@idProduct int)
AS
	BEGIN
		SELECT p.id, p.idType,p.name,p.author,p.price,p.description,p.publisher,p.rating,p.score from Product p WHERE p.id =@idProduct
	END
	GO
--9. Lấy số lượng sản phẩm trong giỏ hàng của một khách hàng hiện tại
Create   PROC USP_QuantityInCart (@idCus int)
as
	BEGIN
		select count(*) as quantity from Cart WHERE idCustomer=@idCus
	END
    GO
--10. Lấy 5 bình luận đầu tiên của một sản phẩm
create  PROC USP_GetCommenOfProduct(@idProduct INT)
as	
	BEGIN
	select TOP 5 cus.name, c.rating, c.datePosting  as datePost, c.content 
		from  Comment c, Customer cus 
		WHERE c.idProduct= @idProduct and cus.id = c.idCustomer
		ORDER BY datePost desc
	END
	GO
--11. Tính lại rating 
create proc USP_CalRating (@idpro int)
as
	begin
			declare @rating float
			select @rating=  avg(cast(c.rating as float))  from Comment c where c.idProduct = 1 and c.rating is not null
			update Product set rating = @rating where id =@idPro
			return 1
	END
    GO
--12. Lưu bình luận, đánh giá của người dùng
create  PROC USP_commentInProduct(@idCus int, @idPro int, @idShop int=1, @content nvarchar(100), @rating int)
	as
		BEGIN
				if (@rating <0) 
					set @rating=0
				if ((SELECT Count(*) from Bill b,BillDetail bd WHERE b.idCustomer=@idCus and b.id = bd.idBill and bd.idProduct =@idPro)<=0 )
						set @rating=null;
				
				insert into Comment VALUES (@idPro, @idCus, @idShop,GETDATE(), @content , @rating)
				exec USP_CalRating @idpro =@idPro
		END
		GO
--13. Thêm sản phẩm vào giỏ hàng, nếu sản phẩm đã có trong giỏ hàng thì tăng thêm
create   proc USP_AddProductToCart (@idCus int, @idPro int, @quantity int, @idShop int =1)
	as
		BEGIN
			if (SELECT COUNT(*) from Cart WHERE idCustomer = @idCus and idProduct = @idPro) >0 
				UPDATE Cart set quantity = quantity + @quantity WHERE @idPro= idProduct and @idCus = idCustomer
			ELSE
				INSERT INTO Cart VALUES (@idCus, @idPro, @idShop, @quantity)
		END
go
--14. Kiểm tra số lượng còn trong kho
create proc USP_GetQuantityInWareHouse (@idPro int)
as
	begin
		select d.quantity from Product d where d.id = @idPro
	end
GO
--16. get 3 image of product 
create proc USP_GetImgProduct (@idProduct int)
as
	begin
		select top 3 * from PhotoProduct where idProduct =@idProduct
	end
go
--17. Tăng số lượng trong giỏ hàng
CREATE   proc USP_PlusAProduct (@idCus int, @idPro INT)
		as		
			BEGIN
				if (select count(*) from Cart WHERE @idCus = idCustomer and @idPro = idProduct)>0
						UPDATE Cart set quantity = quantity+1 WHERE @idCus = idCustomer and @idPro = idProduct
			END
GO
--18. Giảm số lượng trong giỏ hàng
CREATE   proc USP_MinusAProduct (@idCus int, @idPro INT)
	as		
		BEGIN
			if (select count(*) from Cart WHERE @idCus = idCustomer and @idPro = idProduct)>0
				UPDATE Cart set quantity = quantity-1 WHERE @idCus = idCustomer and @idPro = idProduct
		END
GO
--19. Xóa sản phẩm khỏi giỏ hàng
CREATE   PROC USP_DeleteProductCart(@idCus int, @idPro int)
AS
		BEGIN
			DELETE FROM Cart where idCustomer=@idCus AND idProduct=@idPro
		END
GO
--20. Lấy đơn vị vận chuyển (mặc định phí giao hàng là đơn vị vận chuyển đầu tiên trong ds)
create proc USP_GetDelivery 
as
	begin
		select * from Delivery
	end
GO

create or alter  PROC USP_getSelectProduct (@idCus int ,@idPro int)
AS
BEGIN
	select top 1 pro.id,pro.name, pro.photo,pro.price,c.quantity from Cart c, (select p.id, ph.photo,p.name,p.price,p.rating FROM Product p left join PhotoProduct ph ON p.id= ph.idProduct) as pro
			WHERE c.idProduct=@idPro and c.idCustomer=@idCus and c.idProduct=pro.id 
END
GO
--21. Tmp 
create proc USP_GetProductHighestScore  
as 
		begin
		--Using Cursor
		DECLARE @MyCursor CURSOR;
		--value for loop through
		DECLARE @ItemID int;
	    DECLARE @count int
		set @count =0

		DECLARE @id int, @name  nvarchar(100), @photo varbinary(max) , @price float, @quantity int,
				@idShop int, @idType int, @author nvarchar(100),@description varchar(100), 
					@publisher varchar(100),@rating int ,@score varchar(100), @quantitySold int, @idVoucher int

			DECLARE @tblResult table (id int, name  nvarchar(100), photo varbinary(max) , price float, quantity int,
				idShop int, idType int, author nvarchar(100),description varchar(100), 
					publisher varchar(100),rating int ,score varchar(100), quantitySold int, idVoucher int)
			BEGIN 
					SET @MyCursor = CURSOR FOR
					select  p0.id from Product p0 order by p0.score desc
					OPEN @MyCursor
					FETCH NEXT FROM @MyCursor INTO @ItemID
					WHILE @@FETCH_STATUS = 0 and  @count <4
						BEGIN
							(select top 1  @id= p.id, @name =concat(SUBSTRING(p.name,1,36),'...'), @photo=pho.photo , @price =p.price, @quantity= p.quantity ,
										@idShop= p.idShop, @idType =p.idType, @author =p.author,@description =p.description, 
										@publisher= p.publisher,@rating =p.rating ,@score= p.score, @quantitySold =p.quantity, @idVoucher =p.idVoucher
										from Product p, PhotoProduct pho
										where p.id= @itemID and p.id = pho.idProduct 
									)
							insert into @tblresult   values (@id, @name , @photo  , @price, @quantity ,
										@idShop, @idType, @author ,@description , 
										@publisher ,@rating ,@score , @quantitySold , @idVoucher)
							set  @count =@count+1
							FETCH NEXT FROM @MyCursor
									INTO @ItemID  
						END; 
					CLOSE @MyCursor ;
					DEALLOCATE @MyCursor;
				END;
				select * from @tblResult
		end
GO
--22. Lấy các sản phẩm cùng Type- gợi ý trong detail Page
create proc USP_GetSuggestProduct (@idProduct int)
as
	begin
		
		--Using Cursor
		DECLARE @myIdType int
		select @myIdType= p.idType from Product p where p.id = @idProduct
			DECLARE @MyCursor CURSOR;
		--value for loop through
		DECLARE @ItemID int;
	    DECLARE @count int
		set @count =0

		DECLARE @id int, @name  nvarchar(100), @photo varbinary(max) , @price float, @quantity int,
				@idShop int, @idType int, @author nvarchar(100),@description varchar(100), 
					@publisher varchar(100),@rating int ,@score varchar(100), @quantitySold int, @idVoucher int

			DECLARE @tblResult table (id int, name  nvarchar(100), photo varbinary(max) , price float, quantity int,
				idShop int, idType int, author nvarchar(100),description varchar(100), 
					publisher varchar(100),rating int ,score varchar(100), quantitySold int, idVoucher int)
			BEGIN 
					SET @MyCursor = CURSOR FOR
					select  p0.id from Product p0 where p0.idType =@myIdType and p0.id != @idProduct
					OPEN @MyCursor
					FETCH NEXT FROM @MyCursor INTO @ItemID
					WHILE @@FETCH_STATUS = 0 and  @count <6
						BEGIN
							(select top 1 @id= p.id, @name =concat(SUBSTRING(p.name,1,20),'...'), @photo=pho.photo , @price =p.price, @quantity= p.quantity ,
										@idShop= p.idShop, @idType =p.idType, @author =p.author,@description =p.description, 
										@publisher= p.publisher,@rating =p.rating ,@score= p.score, @quantitySold =p.quantity, @idVoucher =p.idVoucher
										from Product p, PhotoProduct pho
										where p.id=@ItemID and p.id = pho.idProduct)
							insert into @tblresult   values (@id, @name , @photo  , @price, @quantity ,
										@idShop, @idType, @author ,@description , 
										@publisher ,@rating ,@score , @quantitySold , @idVoucher)
							set  @count =@count+1
							FETCH NEXT FROM @MyCursor
									INTO @ItemID  
						END; 
					CLOSE @MyCursor ;
					DEALLOCATE @MyCursor;
				END;
				select * from @tblResult
	end
GO
--- Bill Page
--Lấy chi tiết đơn hàng
Create   PROC USP_getDetailBill (@idBill int)
as
BEGIN
	
	select Bill.* , v.name as vouchername, v.discount as voucher, d.name as delivery, p.name as payment
	from Bill, Voucher as v, Delivery as d, Payment as p
	where Bill.id=@idBill and Bill.idDelivery= d.id and Bill.idPayment= p.id and Bill.idVoucher= v.id
END
GO

--23. 
create proc USP_AddProductToCart (@idCus int, @idPro int, @quantity int, @idShop int =1)
	as
		BEGIN
			if (SELECT COUNT(*) from Cart WHERE idCustomer = @idCus and idProduct = @idPro) >0 
				UPDATE Cart set quantity = quantity + @quantity WHERE @idPro= idProduct and @idCus = idCustomer
			ELSE
				INSERT INTO Cart VALUES (@idCus, @idPro, @idShop, @quantity)
		END
		GO
--24.bodung
create or alter procedure USP_GetListProductByIDCus(@idCus int)
as
	begin
		--Using Cursor
			DECLARE @MyCursor CURSOR;
		--value for loop through
		DECLARE @ItemID int;
		DECLARE @ItemQuantity int;

		DECLARE @idProduct int, @name  nvarchar(100), @photo varbinary(max) , @price float, @quantity int, @idVoucher int, @discount float

		DECLARE @tblResult table (id int, name nvarchar(100), photo varbinary(max) , price float, quantity int, idVoucher int, discount float)
				BEGIN 
					SET @MyCursor = CURSOR FOR
					select  c.idProduct, c.quantity from Cart c where c.idCustomer =@idCus
				OPEN @MyCursor
				FETCH NEXT FROM @MyCursor INTO @ItemID, @ItemQuantity
				WHILE @@FETCH_STATUS = 0
					BEGIN
					if (select p.idVoucher from Product p where p.id= @ItemID)>0
						begin
								(select top 1 @idPRoduct= p.id, @name=p.name, @photo= pho.photo,@price= p.price, 
								@quantity= p.quantity , @idVoucher= p.idVoucher, @discount= v.discount
								from Product p, PhotoProduct pho, Voucher v where p.id=@ItemID and p.id = pho.idProduct and v.id= p.idVoucher)
						end
					else
						begin
								(select top 1 @idPRoduct= p.id, @name=p.name, @photo= pho.photo,@price= p.price, 
								@quantity= p.quantity , @idVoucher= p.idVoucher, @discount= 0
								from Product p, PhotoProduct pho where p.id=@ItemID and p.id = pho.idProduct )
						end
						
						
								insert into @tblresult (id ,name, photo, price, quantity,idVoucher, discount )  values (@idProduct, @name, @photo, @price, @quantity, @idVoucher, @discount)
									update @tblResult set quantity =@ItemQuantity where id= @ItemID
				FETCH NEXT FROM @MyCursor
								INTO @ItemID, @ItemQuantity      
					END; 
				CLOSE @MyCursor ;
				DEALLOCATE @MyCursor;
				END;
		select * from @tblResult
end
GO
--24.

create procedure USP_GetInfoCustomer(@idCus int)
as
	begin 
	select * from Customer c where c.id=@idCus
	end
	go
--25-- Profile
create PROC USP_UpdateCustomer(@id int, @name nvarchar(100), @birthDay date, @gender nvarchar(30), @address nvarchar(100), @phone nvarchar(15), @email nvarchar(30))
as
BEGIN

  Update Customer set name=@name , 
					  birthDay=@birthDay, 
					  gender=@gender, 
					  address=@address, 
					  phone=@phone , 
					  email=@email
		 where id=@id

END

------------------------------ end Thao LE procedure
