# BOOK STORE WEB

Description: Book Shop eCommerce Websites.

Subjects: eCommerce.

Instructors: Mr. Nguyễn Trường Hải.

# Group information: 
- Trần Văn Ân - 18110249
- Nguyễn Thị Thảo - 18110367
- Võ Hồng Tiên Giang - 18110276
- Nguyễn Thị Minh Hoàng - 18110285
- Lê Thị Phương Thảo- 18110366

# Project information:
## Technologies:
- Database: SQL Server.
- Frontend:
    + JQuery
    + Bootstrap
- Backend:
    + ASP.NET API
    + MVC Model
  
## Installing
- Clone project.
- Go to [Data](https://bitbucket.org/vhtiengiang177/bookstoreweb/src/master/Data/) folder.
- Run file **bookStoreQuery.sql** on SQL Server Management Studio.
- Then go back.
- Run file BOOKSTOREWEB.sln

## Usage

Login to 

- Admin:
    + Username: admin001
    + Password: admin001
- Staff:
    + Username: staff001
    + Password: staff001
- Shipper:
    + Username: shipper001
    + Password: shipper001
- User:
    + Username: user003
    + Password: user003
  
Or you can register new account.
